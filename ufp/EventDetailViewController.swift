//
//  EventDetailViewController.swift
//  Unity for People
//
//  Created by Devclan on 03/12/2015.
//  Copyright (c) 2015 TwinEngineLabs. All rights reserved.
//

import UIKit
import MapKit
import SDWebImage
import WSProgressHUD
import AFNetworking
import apptentive_ios

public class EventDetailViewController: UIViewController,UIWebViewDelegate {
    var dateReader:NSDateFormatter!
    var dateWriter:NSDateFormatter!
    @IBOutlet weak var nameLbl: UILabel!
    @IBOutlet weak var fromToLbl: UILabel!
    @IBOutlet weak var interestsLbl: UILabel!
    @IBOutlet weak var location: UILabel!
    @IBOutlet weak var img: UIImageView!
    
    //------hide mapview---------//
//    @IBOutlet weak var mapVu: MKMapView!
    //------hide mapview---------//
    
    @IBOutlet weak var btnFav: UIButton!
    public var event:Event2!
    @IBOutlet weak var webVu: UIWebView!
    @IBOutlet weak var address: UILabel!
    var eventId:Int?

    @IBOutlet weak var back: UIButton!
    @IBOutlet weak var cancel: UIButton!
    override public func viewDidLoad() {
        super.viewDidLoad()
        ATConnect.sharedConnection().engage("viewed_event_details", fromViewController: self)
        
        let tapRecognizer = UITapGestureRecognizer(target: self, action: Selector("imageTapped:"))
        //Add the recognizer to your view.
        address.userInteractionEnabled=true
        address.addGestureRecognizer(tapRecognizer)
        
        
        dateReader = NSDateFormatter()
        dateWriter = NSDateFormatter()
        //dateReader.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"//"2015-12-19T00:00:00.000-06:00"
        
        let enUSPosixLocale = NSLocale(localeIdentifier: "en_US_POSIX")
        dateReader.locale = enUSPosixLocale
        dateReader.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZZZZZ"
        
        
        dateReader.timeZone=NSTimeZone(abbreviation: "UTC")
//        dateWriter.dateFormat = "MMMM dd"
        dateWriter.dateFormat = "MMM dd | hh:mm a"

        if(self.event == nil && eventId != nil){
            
            self.back.hidden=true
            self.cancel.hidden=false
            fetchEventById(eventId)
        }
        else if (self.event != nil){
            self.back.hidden=false
            self.cancel.hidden=true
            self.populateEventData()
        }
        // Do any additional setup after loading the view.
        
        UIApplication.sharedApplication().statusBarStyle = .LightContent
        
    
    }
    @IBAction func dismissVC(){
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    func fetchEventById(eventId:Int!){
        var p = WSProgressHUD(view: self.view)
        self.view.addSubview(p)
        p.show()
        let manager = AFHTTPRequestOperationManager()
        manager.GET("\(Env.ApiUrl)/api/events/\(eventId!)",
            parameters:nil,
            success:{ operation, responseObject in
                AppManager.sharedInstance.needsReloadEventsFav=true
                AppManager.sharedInstance.needsReloadEvents=true
                let dic:NSDictionary! = responseObject as! NSDictionary
                
                self.event = Event2(dict: dic.objectForKey("event") as! NSDictionary, sect: EventsSection(title: "", type: "", span: "", events: NSArray()))
                self.populateEventData()
                p.dismiss()
            },
            failure: { operation, error in
                p.dismiss()
        })
    }
    
    
    public func webView(webView: UIWebView, shouldStartLoadWithRequest request: NSURLRequest, navigationType: UIWebViewNavigationType) -> Bool {
//        if ( inType == UIWebViewNavigationTypeLinkClicked ) {
//            [[UIApplication sharedApplication] openURL:[inRequest URL]];
//            return NO;
//        }
//        
//        return YES;
        if navigationType == UIWebViewNavigationType.LinkClicked{
            UIApplication.sharedApplication().openURL(request.URL!)
            return false
        }
        return true
    }
    func imageTapped(gestureRecognizer: UITapGestureRecognizer) {
        let label = gestureRecognizer.view as! UILabel
        if count(label.text!) > 0 {
            var add:String! = label.text?.stringByAddingPercentEscapesUsingEncoding(NSUTF8StringEncoding)
            if count(add)>0 {
                var url:String! = "http://maps.apple.com/?q=\(add)"
                if UIApplication.sharedApplication().canOpenURL(NSURL(string: url)!)==true {
                    UIApplication.sharedApplication().openURL(NSURL(string: url)!)
                }
            }
        }
    }
    
    func populateEventData(){
        nameLbl.text = event.name
        
        var date = dateReader.dateFromString(event.startsAt)
        var startDateStr = dateWriter.stringFromDate(date!)
        
//        date = dateReader.dateFromString(event.endsAt)
//        var endDateStr = dateWriter.stringFromDate(date!)

//        fromToLbl.text = startDateStr+" - "+endDateStr
        
        fromToLbl.text = startDateStr
//        content.text = event.content
        webVu.loadHTMLString(event.content, baseURL: nil)
        location.text = event.locationName
        
//        address.attributedText = NSAttributedString(string: event.locationAddress, attributes: [NSFontAttributeName:[UIFont.systemFontOfSize(16, weight: UIFontWeightBold)],
//            
//            NSUnderlineStyleAttributeName:NSUnderlineStyle.StyleSingle.rawValue,
//            NSForegroundColorAttributeName:interestsLbl.textColor
//            
//            ])
        
        
        address.attributedText = NSAttributedString(string: event.locationAddress, attributes: [NSFontAttributeName:UIFont.boldSystemFontOfSize(14),NSUnderlineStyleAttributeName:NSUnderlineStyle.StyleSingle.rawValue])
        
        
        
//        let underlineAttribute = [NSUnderlineStyleAttributeName: NSUnderlineStyle.StyleSingle.rawValue]
//        let underlineAttributedString = NSAttributedString(string: "StringWithUnderLine", attributes: underlineAttribute)
//        myLabel.attributedText = underlineAttributedString
        
        img.sd_setImageWithURL(NSURL(string: event.picUrl), placeholderImage: UIImage(named: "AppIcon"))
        btnFav.selected=event.isFavorite
        
        var location2 = CLLocationCoordinate2D()
        location2.latitude = (event.lat as NSString).doubleValue
        location2.longitude = (event.lng as NSString).doubleValue
        var region = MKCoordinateRegion()
        region.center = location2
        region.span.latitudeDelta = 5
        region.span.longitudeDelta = 5
        
        
        //------hide mapview---------//
//        mapVu.setRegion(region, animated: true)
        //------hide mapview---------//
        
        interestsLbl.text = self.event.intrestsString() as String

//        mapVu.setCenterCoordinate(location2, animated: true)
        
    
    }
    @IBAction public func btnBack(sender: AnyObject) {
        
        self.navigationController?.popViewControllerAnimated(true)
        
    }
    
    @IBAction public func btnFav(sender: AnyObject) {
        btnFav.selected = !btnFav.selected
        self.event.isFavorite = self.btnFav.selected
        var p = WSProgressHUD(view: self.view)
        self.view.addSubview(p)
        p.show()
        self.btnFav.enabled = false
        let manager = AFHTTPRequestOperationManager()
        manager.POST("\(Env.ApiUrl)/api/mark_as_favorite",
            parameters:[
                "user_id":NSNumber(integer: currentUserID),
                "mark":NSNumber(bool:  btnFav.selected),
                "event_id":NSNumber(integer: event.objId)
            ],
            success:{ operation, responseObject in
                p.dismiss()
                self.btnFav.enabled = true
                if self.btnFav.selected==true{
                    ATConnect.sharedConnection().engage("event_marked_favorite", fromViewController: self)
                    
                }else{
                    ATConnect.sharedConnection().engage("event_unmarked_favorite", fromViewController: self)
                }
                AppManager.sharedInstance.needsReloadEventsFav=true
                AppManager.sharedInstance.needsReloadEvents=true
            },
            failure: { operation, error in
                p.dismiss()
                self.btnFav.enabled = true
                self.btnFav.selected = !self.btnFav.selected
                self.event.isFavorite = self.btnFav.selected
        })
    }

    override public func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}
