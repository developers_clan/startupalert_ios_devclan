//
//  DiscussionResponse.swift
//  Unity for People
//
//  Created by Ron De Benedetti on 8/9/15.
//  Copyright (c) 2015 TwinEngineLabs. All rights reserved.
//

class DiscussionResponse : RLMObject {
    dynamic var id = 0
    dynamic var content = ""
    dynamic var user_picture_url = ""
    dynamic var posted_at = ""
    dynamic var user_full_name = ""
    
    override class func primaryKey() -> String? {
        return "id"
    }
    
    class var remap : Dictionary<String, String> {
        get {
            return [
                "id" : "id",
                "content" : "content",
                "user_picture_url" : "user_picture_url",
                "posted_at" : "posted_at",
                "user_full_name" : "user_full_name",
                "discussion_id" : "discussion_id"
            ]
        }
    }
    
    class func createOrUpdateFromArray(array : NSArray) -> Array<DiscussionResponse> {
        var discussionResponses = Array<DiscussionResponse>()
        for dict in array {
            discussionResponses.append(self.createOrUpdateFromDictionary(dict as! NSDictionary) as DiscussionResponse)
        }
        return discussionResponses
    }
    
    class func createOrUpdateFromDictionary(dict : NSDictionary) -> DiscussionResponse {
        var discussionResponse = self.createOrUpdateInDefaultRealmWithValue(Formatter.remapJsonKeys(dict, remapDict: remap))
        return discussionResponse as DiscussionResponse
    }
    
    class func newInstance(content: String, user_picture_url: String, posted_at: String, user_full_name: String) -> DiscussionResponse{
        var discussionResponse = DiscussionResponse.new()
        discussionResponse.content = content
        discussionResponse.user_picture_url = user_picture_url
        discussionResponse.posted_at = posted_at
        discussionResponse.user_full_name = user_full_name
        return discussionResponse
    }
    
}
