//
//  CoworkingSpacesShowViewController.swift
//  Unity for People
//
//  Created by Ron De Benedetti on 7/27/15.
//  Copyright (c) 2015 TwinEngineLabs. All rights reserved.
//

import PureLayout
import TZStackView
import KILabel

class ResourceShowViewController: UIViewController  {
    
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var myImage: UIImageView!
    @IBOutlet weak var myDescription: UITextView!
    
    var stackView: TZStackView!
    
    var contactInfo: UIView!
        var contactSectionLabel: UILabel!
        var contactPhoneLabel: UILabel!
        var contactUrlLabel: KILabel!
        var contactEmailLabel: UILabel!
    
    var investorInfo: UIView!
        var investorSectionLabel: UILabel!
        var investorVerticalsLabel: UILabel!
        var investorFundingRoundsLabel: UILabel!
        var investorAddressLabel: UILabel!
    
    var showContactInfo = false
    var showInvestorInfo = false
    
    var imageURL = ""
    var content = ""
    var location = ""
    var name = ""
    
    var phone = ""
    var url = ""
    var email = ""
    
    var verticals = ""
    var fundingRounds = ""
    var address = ""
    
    var coworkingSpacesMarqueeViewController: CoworkingSpacesMarqueeViewController!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        myImage.sd_setImageWithURL(NSURL(string: imageURL))
        coworkingSpacesMarqueeViewController.myName.text = name
        coworkingSpacesMarqueeViewController.myLocation.text = location
        myDescription.text = content
        
        myDescription.textContainerInset = UIEdgeInsetsMake(10, 6, 10, 6)
        
        stackView = TZStackView(arrangedSubviews: createViews())
        stackView.setTranslatesAutoresizingMaskIntoConstraints(false)
        stackView.axis = .Vertical
        stackView.distribution = .EqualSpacing
        stackView.alignment = .Center
        stackView.spacing = 15
        scrollView.addSubview(stackView)
        
        stackView.autoPinEdge(ALEdge.Top, toEdge: ALEdge.Bottom, ofView: myDescription)
        stackView.autoPinEdge(ALEdge.Left, toEdge: ALEdge.Left, ofView: scrollView)
        stackView.autoPinEdge(ALEdge.Right, toEdge: ALEdge.Right, ofView: scrollView)
        
        contactInfo?.autoPinEdge(ALEdge.Left, toEdge: ALEdge.Left, ofView: stackView)
        contactInfo?.autoPinEdge(ALEdge.Right, toEdge: ALEdge.Right, ofView: stackView)
        investorInfo?.autoPinEdge(ALEdge.Left, toEdge: ALEdge.Left, ofView: stackView)
        investorInfo?.autoPinEdge(ALEdge.Right, toEdge: ALEdge.Right, ofView: stackView)
    }
    
    func setupResourceShow(imageURL: String, content: String, phone: String, url: String, email: String, location: String, name: String) {
        self.imageURL = imageURL
        self.content = content
        self.phone = phone
        self.url = url
        self.email = email
        self.location = location
        self.name = name
    }
    
    func setupInvestorInfo(verticals: String, fundingRounds: String, address: String) {
        showInvestorInfo = true
        
        self.verticals = verticals
        self.fundingRounds = fundingRounds
        self.address = address
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        let navigationBarHeight = navigationController?.navigationBar.bounds.height
        let imageHeight = myImage.bounds.size.height
        let descriptionHeight = myDescription.frame.size.height
        var stackHeight = stackView.bounds.height
        
        //XXX:FIXME:HACK: The below voodoo is necessary because heights are improperly calculated
        // somewhere else and there's clipping. And, it's too late and the work's too free to hunt down the real bug right now.
        // It may not have anything to do with stackView.
        if stackView.subviews.count > 1 {
            stackHeight += stackView.subviews[1].bounds.height
        } else {
            stackHeight += 100
        }
        
        scrollView.contentSize.height = (navigationBarHeight ?? 0) +
            imageHeight +
            descriptionHeight +
            stackHeight
    }
    
    override func viewWillAppear(animated: Bool) {
        
        let maxHeight : CGFloat = 10000
        let maxWidth : CGFloat = myDescription.bounds.size.width
        let rect = myDescription.attributedText?.boundingRectWithSize(CGSizeMake(maxWidth, maxHeight), options: .UsesLineFragmentOrigin, context: nil)
        var frame = myDescription.frame
        var descriptionHeight = rect!.size.height
        frame.size.height = descriptionHeight
        myDescription.frame = frame
        
        self.myImage.clipsToBounds = true
        
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if let vc = segue.destinationViewController as? CoworkingSpacesMarqueeViewController
            where segue.identifier == "ResourceMarqueeSegue" {
                self.coworkingSpacesMarqueeViewController = vc
        }
    }
    
    private func createViews() -> [UIView] {
        var views = [UIView]()
        
        if showInvestorInfo {
            investorInfo = UIView(forAutoLayout: ())
            
            investorSectionLabel = UILabel(forAutoLayout: ())
            investorSectionLabel.font = UIFont.boldSystemFontOfSize(20)
            investorSectionLabel.textColor = UIColor(red: 0.49, green: 0.60, blue: 0.71, alpha: 1)
            investorSectionLabel.text = "Investor Info"
            investorVerticalsLabel = UILabel(forAutoLayout: ())
            investorVerticalsLabel.numberOfLines = 0
            investorVerticalsLabel.text = "Verticals: \(verticals)"
            investorFundingRoundsLabel = UILabel(forAutoLayout: ())
            investorFundingRoundsLabel.numberOfLines = 0
            investorFundingRoundsLabel.text = "Funding Rounds: \(fundingRounds)"
            investorAddressLabel = UILabel(forAutoLayout: ())
            investorAddressLabel.text = address
            
            investorInfo.addSubview(investorSectionLabel)
            investorInfo.addSubview(investorVerticalsLabel)
            investorInfo.addSubview(investorFundingRoundsLabel)
            investorInfo.addSubview(investorAddressLabel)
            
            investorSectionLabel.autoPinEdgesToSuperviewEdgesWithInsets(UIEdgeInsetsMake(0, 10, 0, 10), excludingEdge: .Bottom)
            investorVerticalsLabel.autoPinEdge(.Top, toEdge: .Bottom, ofView: investorSectionLabel, withOffset: 10)
            investorFundingRoundsLabel.autoPinEdge(.Top, toEdge: .Bottom, ofView: investorVerticalsLabel, withOffset: 10)
            investorAddressLabel.autoPinEdge(.Top, toEdge: .Bottom, ofView: investorFundingRoundsLabel, withOffset: 10)
            investorAddressLabel.autoPinEdge(.Bottom, toEdge: .Bottom, ofView: investorInfo)
            let investorSubviews = [investorSectionLabel, investorVerticalsLabel, investorFundingRoundsLabel, investorAddressLabel] as NSArray
            investorSubviews.autoAlignViewsToEdge(.Left)
            investorSubviews.autoAlignViewsToEdge(.Right)
            
            views.append(investorInfo)
        }
        
        if showContactInfo {
            contactInfo = UIView(forAutoLayout: ())
            
            contactSectionLabel = UILabel(forAutoLayout: ())
            contactSectionLabel.font = UIFont.boldSystemFontOfSize(20)
            contactSectionLabel.textColor = UIColor(red: 0.49, green: 0.60, blue: 0.71, alpha: 1)
            contactSectionLabel.text = "Contact Info"
            contactPhoneLabel = UILabel(forAutoLayout: ())
            contactPhoneLabel.text = phone
            contactUrlLabel = KILabel(forAutoLayout: ())
            contactUrlLabel.text = url
            contactUrlLabel.urlLinkTapHandler = { label, url, range in
                var urlWithProtocol = ""
                
                if let textRange = url.rangeOfString("http") {
                    urlWithProtocol = url
                } else {
                    urlWithProtocol = "http://\(url)"
                }
                
                UIApplication.sharedApplication().openURL(NSURL(string: urlWithProtocol)!)
            }
            contactEmailLabel = UILabel(forAutoLayout: ())
            contactEmailLabel.text = email
            
            contactInfo.addSubview(contactSectionLabel)
            contactInfo.addSubview(contactPhoneLabel)
            contactInfo.addSubview(contactUrlLabel)
            contactInfo.addSubview(contactEmailLabel)
            
            contactSectionLabel.autoPinEdgesToSuperviewEdgesWithInsets(UIEdgeInsetsMake(0, 10, 0, 10), excludingEdge: .Bottom)
            contactPhoneLabel.autoPinEdge(.Top, toEdge: .Bottom, ofView: contactSectionLabel, withOffset: 10)
            contactUrlLabel.autoPinEdge(.Top, toEdge: .Bottom, ofView: contactPhoneLabel, withOffset: 5)
            contactEmailLabel.autoPinEdge(.Top, toEdge: .Bottom, ofView: contactUrlLabel, withOffset: 5)
            contactEmailLabel.autoPinEdge(.Bottom, toEdge: .Bottom, ofView: contactInfo)
            let contactSubviews = [contactSectionLabel, contactPhoneLabel, contactUrlLabel, contactEmailLabel] as NSArray
            contactSubviews.autoAlignViewsToEdge(.Left)
            contactSubviews.autoAlignViewsToEdge(.Right)
            
            views.append(contactInfo)
        }
        
        return views
    }
}
