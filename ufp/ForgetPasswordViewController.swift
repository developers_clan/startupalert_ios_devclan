//
//  ForgetPasswordViewController.swift
//  Unity for People
//
//  Created by Devclan on 16/11/2015.
//  Copyright (c) 2015 TwinEngineLabs. All rights reserved.
//

import UIKit
import WSProgressHUD

class ForgetPasswordViewController: UIViewController {
    
    @IBOutlet weak var txtEmail: TextField!
//    public var callbackDelegate:LandingViewConrollerCallbackDelegate!

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func btnReset_Tapd(sender: AnyObject) {
        
        if count(self.txtEmail.text)>3{
            WSProgressHUD.show()
            RequestManager.resetPass(self.txtEmail.text){ (response, error) in
                if error != nil {
                    WSProgressHUD.dismiss()
//                    AppManager.sharedInstance.showMessage(self, title: "Error", msg: "Server not reachable", buttonTitle: "Cancel")
                } else {
                    NSOperationQueue.mainQueue().addOperationWithBlock({ () -> Void in
                        WSProgressHUD.dismiss()
                    })
                    
                    let res = response as! NSDictionary
                    //    var res  = response["message"] as! String
                    let message: String? = res["error"] as? String
                    AppManager.sharedInstance.showMessage(self, title: "", msg: message, buttonTitle: "Ok")
                }
                
                return nil
            }

        }else{
            AppManager.sharedInstance.showMessage(self, title: "Error!", msg: "Please enter email", buttonTitle: "Ok")
        }
        
        
        
    }

    @IBAction func btnCancel_Tapd(sender: AnyObject) {
        
//        if self.callbackDelegate != nil{
//            self.callbackDelegate.cancelPressed()
//        }
        self.navigationController?.popViewControllerAnimated(true)
//        self.dismissViewControllerAnimated(true, completion: nil)
        
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
