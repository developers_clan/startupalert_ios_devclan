//
//  ResourceTextCell.swift
//  Unity for People
//
//  Created by Ron De Benedetti on 8/5/15.
//  Copyright (c) 2015 TwinEngineLabs. All rights reserved.
//

import UIKit

class ResourceTextCell: UITableViewCell {
    
    @IBOutlet var myText: UITextView!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
