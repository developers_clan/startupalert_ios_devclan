//
//  SkillsetCell.swift
//  ufp
//
//  Created by Ron De Benedetti on 6/29/15.
//  Copyright (c) 2015 TwinEngineLabs. All rights reserved.
//

import UIKit

class SkillsetCell: UITableViewCell {
    
    @IBOutlet var myLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        myLabel.highlightedTextColor = UIColor(red: 0.0, green: 0.23, blue: 0.46, alpha:1)
        selectedBackgroundView = UIView()
        selectedBackgroundView.backgroundColor = UIColor(red: 0.0, green: 0.23, blue: 0.46, alpha:0.05)
        self.selectedBackgroundView = selectedBackgroundView
    }
    
}
