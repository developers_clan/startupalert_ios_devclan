//
//  ConnectionsCell.swift
//  Unity for People
//
//  Created by Ron De Benedetti on 8/7/15.
//  Copyright (c) 2015 TwinEngineLabs. All rights reserved.
//

import UIKit

class ConnectionsCell: UITableViewCell {
    
    @IBOutlet var myImage: UIImageView!
    @IBOutlet var myName: UILabel!
    @IBOutlet var myLocation: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
