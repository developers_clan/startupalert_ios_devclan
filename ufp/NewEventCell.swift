//
//  NewEventCell.swift
//  Unity for People
//
//  Created by Devclan on 03/12/2015.
//  Copyright (c) 2015 TwinEngineLabs. All rights reserved.
//

import UIKit
import WSProgressHUD
import AFNetworking
import apptentive_ios
public protocol FavoriteEventCompletionCallBackDelegate{
    func completed(event:Event2!)
}
class NewEventCell: UITableViewCell {
    
    @IBOutlet weak var img: UIImageView!
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var from: UILabel!
    @IBOutlet weak var intrst1: UILabel!
    @IBOutlet weak var intrst2: UILabel!
    var event:Event2!
    var delegate:FavoriteEventCompletionCallBackDelegate!
    @IBOutlet weak var favBtn: UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    @IBAction func favClicked(sender: AnyObject) {
        favBtn.selected = !favBtn.selected
        event.isFavorite=favBtn.selected
        
        
        var p = WSProgressHUD(view: self)
        self.addSubview(p)
        p.show()
        self.favBtn.enabled = false
        let manager = AFHTTPRequestOperationManager()
        manager.POST("\(Env.ApiUrl)/api/mark_as_favorite",
            parameters:[
                "user_id":NSNumber(integer: currentUserID),
                "mark":NSNumber(bool:  favBtn.selected),
                "event_id":NSNumber(integer: event.objId)
            ],
            success:{ operation, responseObject in
                p.dismiss()
                self.favBtn.enabled = true
                self.delegate.completed(self.event)
                
            },
            failure: { operation, error in
                p.dismiss()
                self.favBtn.enabled = true
                self.favBtn.selected = !self.favBtn.selected
                self.event.isFavorite = self.favBtn.selected
        })
        
    }
}
