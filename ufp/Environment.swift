//
//  Environment.swift
//  Unity for People
//
//  Created by Nicholas Phillips on 8/29/15.
//  Copyright (c) 2015 TwinEngineLabs. All rights reserved.
//

struct Env {
    
#if DEVELOPMENT && (arch(i386) || arch(x86_64)) && os(iOS) // Simulator
    
//    static let name = "development"
//    static let ApiUrl = "http://localhost:3000"
//    static let ApptentiveApiKey = "ca05e7a26ef14aa4f8b9a48c69d13d19488ccb8bd2903b7d058762837c99e2d0"
//    static let HeapAppId = "2981905913"
    
    
    static let name = "production"
    static let ApiUrl = "https://production-up.herokuapp.com"
    static let ApptentiveApiKey = "ca05e7a26ef14aa4f8b9a48c69d13d19488ccb8bd2903b7d058762837c99e2d0"
    static let HeapAppId = "1785709440"
    
#elseif DEVELOPMENT // Device
    
//    static let name = "development"
//    static let ApiUrl = "https://staging-up.herokuapp.com" //https://ufp-tel.fwd.wf
//    static let ApptentiveApiKey = "ca05e7a26ef14aa4f8b9a48c69d13d19488ccb8bd2903b7d058762837c99e2d0"
//    static let HeapAppId = "2981905913"
    
    static let name = "production"
    static let ApiUrl = "https://production-up.herokuapp.com"
    static let ApptentiveApiKey = "ca05e7a26ef14aa4f8b9a48c69d13d19488ccb8bd2903b7d058762837c99e2d0"
    static let HeapAppId = "1785709440"
    
#elseif STAGING
    
//    static let name = "staging"
//    static let ApiUrl = "https://staging-up.herokuapp.com"
//    static let ApptentiveApiKey = "ca05e7a26ef14aa4f8b9a48c69d13d19488ccb8bd2903b7d058762837c99e2d0"
//    static let HeapAppId = "2981905913"
    static let name = "production"
    static let ApiUrl = "https://production-up.herokuapp.com"
    static let ApptentiveApiKey = "ca05e7a26ef14aa4f8b9a48c69d13d19488ccb8bd2903b7d058762837c99e2d0"
    static let HeapAppId = "1785709440"
#elseif PRODUCTION
    
    static let name = "production"
    static let ApiUrl = "https://production-up.herokuapp.com"
    static let ApptentiveApiKey = "ca05e7a26ef14aa4f8b9a48c69d13d19488ccb8bd2903b7d058762837c99e2d0"
    static let HeapAppId = "1785709440"
    
#endif
    
}
