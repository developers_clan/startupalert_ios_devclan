//
//  InterestsViewController.swift
//  ufp
//
//  Created by Ron De Benedetti on 6/29/15.
//  Copyright (c) 2015 TwinEngineLabs. All rights reserved.
//

import UIKit
import WSProgressHUD

class InterestsViewController: UIViewController,UICollectionViewDataSource,UICollectionViewDelegate {
    
    var mode: String!
    
    var interests:RLMResults! //= Interest.allObjects().sortedResultsUsingProperty("name", ascending: true)
    
    var titleString: String!
    var profileSetupString: String!
    var interestsString: String!
    var buttonTitle: String!
    var callbackDelegate:LandingViewConrollerCallbackDelegate!
    
    @IBOutlet weak var lblInterests: UILabel!
    
    @IBOutlet var collectionVu: UICollectionView?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationController?.navigationBar.hidden = true
        
        if mode == "signup" {
            
           
            RequestManager.getInterests() { (response, error) in
//                dispatch_async(dispatch_get_main_queue()){
                if error == nil {
                    self.interests = Interest.allObjects().sortedResultsUsingProperty("name", ascending: true)
                    
                    self.collectionVu?.reloadData()
                }
                return nil
//                }
            }
            
        }
        
        
        
        if (mode == "settings"){
//            self.lblTitle.text = titleString
//            self.lblProfileSetup.text = profileSetupString
            self.lblInterests.text = interestsString
            self.interests = Interest.allObjects().sortedResultsUsingProperty("name", ascending: true)
            
//            self.navigationItem.rightBarButtonItem=UIBarButtonItem(title: buttonTitle, style: UIBarButtonItemStyle.Plain, target: self, action:"didTapNextStep")
        }
        
        self.collectionVu?.allowsMultipleSelection = true;
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        if mode == "settings" {
            self.navigationItem.leftBarButtonItem=nil
            for indexPath in preselectedIndexPaths() {
                self.collectionVu?.selectItemAtIndexPath(indexPath as? NSIndexPath, animated: false, scrollPosition: UICollectionViewScrollPosition.None)
            }
        }else{
            self.view.layoutIfNeeded()
//            self.navigationItem.rightBarButtonItem=UIBarButtonItem(title: "Next", style: UIBarButtonItemStyle.Plain, target: self, action:"didTapNextStep")
//            self.navigationItem.leftBarButtonItem=UIBarButtonItem(title: "Cancel", style: UIBarButtonItemStyle.Plain, target: self, action:"cancelPressed")
        }
    }
    func cancelPressed() {
        if self.callbackDelegate != nil{
        self.callbackDelegate.cancelPressed()
        }
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func preselectedIndexPaths() -> NSArray {
        let user = User(forPrimaryKey: currentUserID)
        let preselectedItems = NSMutableSet.new()
        
        var row = 0
        for interest in interests.toArray(Interest.self) {
            if contains(user!.interests.toArray(), interest) {
                preselectedItems.addObject(NSIndexPath(forRow: row, inSection: 0))
            }
            row++
        }
        
        return preselectedItems.allObjects
    }
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        let cell = collectionVu?.dequeueReusableCellWithReuseIdentifier("InterestCollectionCell", forIndexPath: indexPath) as! InterestCollectionCell
        let interest = interests[UInt(indexPath.row)] as! Interest
        cell.lbl.text = interest.name
        return cell
    }
    func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath) {
//        collectionView.reloadItemsAtIndexPaths([indexPath])
//        let cell = collectionVu?.cellForItemAtIndexPath(indexPath) as! InterestCollectionCell
//        cell.lbl.textColor=UIColor.whiteColor()
    }
    func collectionView(collectionView: UICollectionView, didDeselectItemAtIndexPath indexPath: NSIndexPath) {
//        collectionView.reloadItemsAtIndexPaths([indexPath])
//        let cell = collectionVu?.cellForItemAtIndexPath(indexPath) as! InterestCollectionCell
//        cell.lbl.textColor=UIColor.darkGrayColor()
    }
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if interests == nil{
            return 0
        }
        else
        {
            return Int(interests.count)
        }
    }
//    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
//        return Int(interests.count)
//    }
//    
//    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell{
//        let cell = tableView.dequeueReusableCellWithIdentifier("SkillsetCellIdentifier") as! SkillsetCell
//        let interest = interests[UInt(indexPath.row)] as! Interest
//        cell.myLabel.text = interest.name
//        return cell
//    }
    @IBAction func didTapNextStep(){
        AppManager.sharedInstance.needsReloadEvents=true
        AppManager.sharedInstance.needsReloadEventsFav=true
        var selectedIndexPaths = self.collectionVu?.indexPathsForSelectedItems()
        var interests_string = ""
        for (var i = 0; i < selectedIndexPaths?.count; i++) {
            let selected_interest_id = UInt(selectedIndexPaths![i].row)
            let interest = interests[selected_interest_id] as! Interest
            interests_string += "\(interest.id),"
        }
        RequestManager.step3(authToken, interests: interests_string){ (response, error) in
            dispatch_async(dispatch_get_main_queue()){
                self.showNextStep()
            }
        }
    }
    
    @IBAction func btnCancel(sender: AnyObject) {
        
        self.cancelPressed()
        
    }
    func showNextStep(){
        if (mode == "signup"){
//            let vc = self.storyboard?.instantiateViewControllerWithIdentifier("CurrentProject") as! DescribeCurrentProjectViewController
//            vc.mode = "signup"
//            self.navigationController?.pushViewController(vc, animated: true)
//            
            
//            let storyboard = UIStoryboard(name: "CollaborationCorner", bundle: nil)
//            let vc = storyboard.instantiateViewControllerWithIdentifier("CollaborationCornerNavigationController") as! UINavigationController
//            self.presentViewController(vc, animated: true) { () -> Void in
//                let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
//                appDelegate.refreshCurrentUsersLocation()
//            }
            
            self.showNewHome()
            
            
        } else {
//           self.navigationController?.popViewControllerAnimated(true)
            self.dismissViewControllerAnimated(true, completion: nil)
        }
    }
    
    func showNewHome(){
        
        
        
        let mainStoryboard = UIStoryboard(name: "CollaborationCorner2", bundle: nil)
        let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
        
        var centerViewController = mainStoryboard.instantiateViewControllerWithIdentifier("MainViewController") as! MainViewController
        
        var leftViewController = mainStoryboard.instantiateViewControllerWithIdentifier("SideBarViewController") as! SideBarViewController
        
        var leftSideNav = UINavigationController(rootViewController: leftViewController)
        var centerNav = UINavigationController(rootViewController: centerViewController)
        
        appDelegate.centerContainer = MMDrawerController(centerViewController: centerNav, leftDrawerViewController: leftSideNav,rightDrawerViewController:nil)
        
        appDelegate.centerContainer!.openDrawerGestureModeMask = MMOpenDrawerGestureMode.PanningCenterView;
        appDelegate.centerContainer!.closeDrawerGestureModeMask = MMCloseDrawerGestureMode.PanningCenterView;
        
        self.presentViewController(appDelegate.centerContainer!, animated: true) { () -> Void in
        
                let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
//                appDelegate.refreshCurrentUsersLocation()
            
            WSProgressHUD.dismiss()
            
            //        appDelegate.window?.rootViewController = centerContainer
            //        appDelegate.window?.makeKeyAndVisible()
            
            
            }
        
    }
    
}
