//
//  Online.swift
//  ufp
//
//  Created by Ron De Benedetti on 7/13/15.
//  Copyright (c) 2015 TwinEngineLabs. All rights reserved.
//

class Online : RLMObject {
    dynamic var id = 0
    dynamic var title = ""
    dynamic var url = ""
    dynamic var content = ""
    dynamic var picture_url = ""
    
    override class func primaryKey() -> String? {
        return "id"
    }
    
    class var remap : Dictionary<String, String> {
        get {
            return [
                "id" : "id",
                "title" : "title",
                "url" : "url",
                "content" : "content",
                "picture_url" : "picture_url"
            ]
        }
    }
    
    class func createOrUpdateFromArray(array : NSArray) -> Array<Online> {
        var onlines = Array<Online>()
        for dict in array {
            onlines.append(self.createOrUpdateFromDictionary(dict as! NSDictionary) as Online)
        }
        return onlines
    }
    
    class func createOrUpdateFromDictionary(dict : NSDictionary) -> Online {
        var online = self.createOrUpdateInDefaultRealmWithValue(Formatter.remapJsonKeys(dict, remapDict: remap))
        return online as Online
    }
}
