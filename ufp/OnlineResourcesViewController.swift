//
//  OnlineResourcesViewController.swift
//  ufp
//
//  Created by Ron De Benedetti on 7/13/15.
//  Copyright (c) 2015 TwinEngineLabs. All rights reserved.
//

import UIKit

class OnlineResourcesViewController: UIViewController, UITableViewDataSource {
    
    var onlines = Online.allObjects()
    
    @IBOutlet var tableView: UITableView?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        RequestManager.getOnlineResources() { (response, error) in
            dispatch_async(dispatch_get_main_queue()){
                self.tableView?.reloadData()
            }
        }
        
        var nib = UINib(nibName: "CoworkingSpacesCell", bundle: nil)
        tableView?.registerNib(nib, forCellReuseIdentifier: "CoworkingSpacesCellIdentifier")
        
        tableView?.tableFooterView = UIView(frame: CGRect.zeroRect)
        
        tableView?.rowHeight = 100
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationItem.title = "Online Resources"
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return Int(onlines.count)
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell{
        let cell = tableView.dequeueReusableCellWithIdentifier("CoworkingSpacesCellIdentifier") as! CoworkingSpacesCell
        let online = onlines[UInt(indexPath.row)] as! Online
        
        cell.myImage.sd_setImageWithURL(NSURL(string: online.picture_url))
        cell.myTitle.text = online.title
        cell.myLocation.text = ""
        
        return cell
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath){
        let selectedOnline = UInt(indexPath.row)
        let online = onlines[selectedOnline] as! Online
        
        if online.invalidated { return }
        
        RequestManager.showOnline(online) { (response, error) in
            dispatch_async(dispatch_get_main_queue()){
                self.showOnline(online)
            }
        }
    }
    
    func showOnline(online: Online){
        let vc = self.storyboard?.instantiateViewControllerWithIdentifier("ResourceShow") as! ResourceShowViewController
        vc.setupResourceShow(online.picture_url, content: online.content, phone: "", url: "", email: "", location: online.url, name: online.title)
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
}
