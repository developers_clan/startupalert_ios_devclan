//
//  CollaborationCornerViewController.swift
//  ufp
//
//  Created by Ron De Benedetti on 6/30/15.
//  Copyright (c) 2015 TwinEngineLabs. All rights reserved.
//

import UIKit
import apptentive_ios

class CollaborationCornerViewController: UIViewController {
    
    @IBOutlet var headerImageView: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        UIApplication.sharedApplication().statusBarStyle = .LightContent
        
        setupNavigationBar()
    }
    
    override func viewWillAppear(animated: Bool) {
        if !RequestManager.tokenIsValid() {
            promptForSignIn()
        } else {
            super.viewWillAppear(animated)

            if User(forPrimaryKey: currentUserID)?.skills.objectsWhere("name = 'Bossier Chamber of Commerce'").count > 0 {
                self.headerImageView.image = UIImage(named: "bossier_chamber_of_commerce")
            } else {
                self.headerImageView.image = UIImage(named: "header")
            }
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func setupNavigationBar(){
        self.navigationController?.navigationBar.barTintColor = UIColor(red: 0, green: 84/256, blue: 168/256, alpha: 1.0)
        self.navigationController?.navigationBar.tintColor = UIColor.whiteColor()
        self.navigationController?.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName : UIColor.whiteColor()]
        
        let settingsButton = UIBarButtonItem(image: UIImage(named: "SettingsIcon"), style: .Plain, target: self, action: "didTapSettings")
        let connectionsButton = UIBarButtonItem(image: UIImage(named: "ConnectionsIcon"), style: .Plain, target: self, action: "didTapConnections")
        
        self.navigationItem.leftBarButtonItem = settingsButton
        self.navigationItem.rightBarButtonItem = connectionsButton
    }
    
    func didTapSettings(){
        
        if !RequestManager.tokenIsValid() {
            promptForSignIn()
        } else {
            let vc = self.storyboard?.instantiateViewControllerWithIdentifier("Settings") as! SettingsViewController
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    func didTapConnections(){
        if !RequestManager.tokenIsValid() {
            promptForSignIn()
        } else {
            let vc = self.storyboard?.instantiateViewControllerWithIdentifier("Connections") as! ConnectionsViewController
            let predicate = NSPredicate(format: "id != \(currentUserID)")
            vc.users = User.objectsWithPredicate(predicate)
            self.navigationController?.pushViewController(vc, animated: true)
            ATConnect.sharedConnection().engage("Connections Tapped", fromViewController: vc)
        }
    }
    
    func promptForSignIn() {
        var signUpStoryboard = UIStoryboard(name: "SignUp", bundle: nil)
        let vc = signUpStoryboard.instantiateViewControllerWithIdentifier("SignUp") as! SignUpViewController
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
}
