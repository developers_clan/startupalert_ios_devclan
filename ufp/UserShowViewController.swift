//
//  UserShowViewController.swift
//  Unity for People
//
//  Created by Ron De Benedetti on 8/11/15.
//  Copyright (c) 2015 TwinEngineLabs. All rights reserved.
//

class UserShowViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    var user: User!
    
    @IBOutlet var skillsTableView: UITableView!
    @IBOutlet var interestsTableView: UITableView!
    
    @IBOutlet var interestedInLabel: UILabel!
    
    @IBOutlet var textView: UITextView!
    
    @IBOutlet var myImage: UIImageView!
    
    @IBOutlet var scrollView: UIScrollView!
    
    @IBOutlet var skillsTableViewHeight: NSLayoutConstraint!
    @IBOutlet var interestsTableViewHeight: NSLayoutConstraint!
    @IBOutlet var scrollViewHeight: NSLayoutConstraint!
    
    var connectionsMarqueeViewController: ConnectionsMarqueeViewController!
    
    var calculated = false
    
    override func viewDidLoad(){
        super.viewDidLoad()
        
        myImage.sd_setImageWithURL(NSURL(string: user.avatarURL))
        
        textView.text = user.currentProject
        
        connectionsMarqueeViewController.myName.text = user.name()
        connectionsMarqueeViewController.myInformation.text = "\(user.vertical.name) - \(user.fundingRound.name)"
        
        var skillNib = UINib(nibName: "ConnectionsSkillCell", bundle: nil)
        skillsTableView?.registerNib(skillNib, forCellReuseIdentifier: "ConnectionsSkillCellIdentifier")
        
        var interestNib = UINib(nibName: "ConnectionsInterestCell", bundle: nil)
        interestsTableView?.registerNib(interestNib, forCellReuseIdentifier: "ConnectionsInterestCellIdentifier")
        
    }
    
    override func viewWillAppear(animated: Bool) {
        let maxHeight: CGFloat = 10000
        let maxWidth: CGFloat = textView.bounds.size.width
        let rect = user.currentProject.boundingRectWithSize(CGSize(width: maxWidth, height: maxHeight),
            options: NSStringDrawingOptions.UsesLineFragmentOrigin,
            attributes: [NSFontAttributeName: UIFont.systemFontOfSize(17.0)],
            context: nil)
        var frame = textView.frame
        var projectHeight = rect.size.height
        frame.size.height = projectHeight
        textView.frame = frame
        
        self.myImage.clipsToBounds = true
        
        var skillsHeight = CGFloat(44 * user.skills.count)
        var interestsHeight = CGFloat(44 * user.interests.count)
        
        skillsTableViewHeight.constant = CGFloat(44 * user.skills.count)
        interestsTableViewHeight.constant = CGFloat(44 * user.interests.count)
        
        skillsTableView.reloadData()
        interestsTableView.reloadData()
    }
    
    override func viewDidLayoutSubviews() {
        
        let skillsHeight = skillsTableView.frame.size.height
        let interestsHeight = interestsTableView.frame.size.height
        
        let projectHeight = textView.frame.size.height
        
        var totalHeight = projectHeight + skillsHeight + interestsHeight + 350
        
        scrollView.contentSize.height = totalHeight
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if (tableView == skillsTableView){
            var number = Int(user.skills.count)
            return number
        }
        else{
            return Int(user.interests.count)
        }
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell{
        
        if (tableView == self.skillsTableView){
            let cell = tableView.dequeueReusableCellWithIdentifier("ConnectionsSkillCellIdentifier", forIndexPath:indexPath) as! ConnectionsSkillCell
            cell.myName.text = user.skills[UInt(indexPath.row)].name
            cell.backgroundColor = UIColor(red: 78/255, green: 118/255, blue: 156/255, alpha: 1.0)
            return cell
        }
        else{
            let cell = tableView.dequeueReusableCellWithIdentifier("ConnectionsInterestCellIdentifier", forIndexPath:indexPath) as! ConnectionsInterestCell
            cell.myName.text = user.interests[UInt(indexPath.row)].name
            cell.backgroundColor = UIColor(red: 101/255, green: 98/255, blue: 98/255, alpha: 1.0)
            return cell
        }
        
    }
    
    
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?){
        if let vc = segue.destinationViewController as? ConnectionsMarqueeViewController
            where segue.identifier == "ConnectionsMarqueeSegue" {
                self.connectionsMarqueeViewController = vc
        }
    }
    
}
