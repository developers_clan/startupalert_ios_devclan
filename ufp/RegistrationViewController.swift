//
//  RegistrationViewController.swift
//  ufp
//
//  Created by Ron De Benedetti on 7/1/15.
//  Copyright (c) 2015 TwinEngineLabs. All rights reserved.
//

import UIKit
import FBSDKCoreKit
import FBSDKLoginKit
import WSProgressHUD
import apptentive_ios

class RegistrationViewController: UIViewController, UITextFieldDelegate {
    
    var mode: String?
    var user: User!
    var firstNameFieldText: String!
    var lastNameFieldText: String!
    var emailFieldText: String!
    var registerButtonTitle: String!
    var callbackDelegate:LandingViewConrollerCallbackDelegate!
    
    @IBOutlet weak var btnNext: UIButton!
    @IBOutlet weak var fbBtn: UIButton!
    @IBOutlet var firstNameField :UITextField!
    @IBOutlet var lastNameField :UITextField!
    @IBOutlet var emailField :UITextField!
    @IBOutlet var passwordField :UITextField!
    @IBOutlet var passwordConfirmationField :UITextField!
    
    @IBOutlet weak var lblOr: UILabel!
    @IBOutlet weak var lblHeader: UILabel!
    @IBOutlet var bottomConstraint: NSLayoutConstraint!
    @IBOutlet var topConstraint: NSLayoutConstraint!
    
    
    var kbHeight: CGFloat!
    
    @IBAction func cancelPressed(sender: AnyObject) {
//        self.callbackDelegate.cancelPressed()
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationController?.navigationBar.hidden=true
        
//        UIApplication.sharedApplication().statusBarStyle = .LightContent
        
        NSNotificationCenter.defaultCenter().addObserver(self, selector: Selector("keyboardWillShow:"), name: UIKeyboardWillShowNotification, object: nil)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: Selector("keyboardWillHide:"), name: UIKeyboardWillHideNotification, object: nil)
        
        self.firstNameField.addTarget(self, action:"textFieldWasChanged:", forControlEvents:UIControlEvents.EditingChanged)
        self.lastNameField.addTarget(self, action:"textFieldWasChanged:", forControlEvents:UIControlEvents.EditingChanged)
        self.emailField.addTarget(self, action:"textFieldWasChanged:", forControlEvents:UIControlEvents.EditingChanged)
        self.passwordField.addTarget(self, action:"textFieldWasChanged:", forControlEvents:UIControlEvents.EditingChanged)
        self.passwordConfirmationField.addTarget(self, action:"textFieldWasChanged:", forControlEvents:UIControlEvents.EditingChanged)
        
        
        self.setLeftPadding(firstNameField)
        self.setLeftPadding(lastNameField)
        self.setLeftPadding(emailField)
        self.setLeftPadding(passwordField)
        self.setLeftPadding(passwordConfirmationField)
        
        if self.mode == "settings" {
            setTextFields()
            self.fbBtn.hidden = true
            self.lblOr.hidden = true
            self.lblHeader.text = "Edit Profile"
            
        } else if Env.name == "development" {
            useTestUser()
        }
        if self.mode==nil {
            self.mode="signup"
            self.fbBtn.hidden = false
            self.lblOr.hidden = false
            self.lblHeader.text = "SIGNUP"
        }
    }
    @IBAction func loginButtonClicked(){
        if FBSDKAccessToken.currentAccessToken() == nil{
            var login = FBSDKLoginManager()
            login.logInWithReadPermissions(["public_profile","email"], fromViewController: self) {
                (result:FBSDKLoginManagerLoginResult!, error:NSError!) -> Void in
                if error != nil{
                    print("Error: fb login")
                    var alert = UIAlertController(title: "Error!", message: error.localizedDescription , preferredStyle: UIAlertControllerStyle.Alert)
                    alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.Default, handler: nil))
                    self.presentViewController(alert, animated: true, completion: nil)
                }else if result.isCancelled{
                    
                }else{
                    WSProgressHUD.show()
                   // self.populateFieldsFromFb()
                    var token = result.token.tokenString
                    println(token)
                    let delegate = UIApplication.sharedApplication().delegate as! AppDelegate
                    var dToken = delegate.deviceTokenString
                    RequestManager.sendToken(token,deviceToken: dToken!){ (response, error) in
                        if error != nil {
                            WSProgressHUD.dismiss()
                        } else {
                          
//                            self.showCollaborationCorner();
//                            self.showNewHome();
                            self.showNextStep();
                            WSProgressHUD.dismiss()
                        }
                        
                        return nil
                    }
                    
                }
            }
        }else{
           // self.populateFieldsFromFb()
            
            
            var token = FBSDKAccessToken.currentAccessToken().tokenString
            println(token)
            
            WSProgressHUD.show()
            let delegate = UIApplication.sharedApplication().delegate as! AppDelegate
            var dToken = delegate.deviceTokenString//"e678643509dc3ff9a6c7e1966a7fe17fbf40d4027eca8c792b192ffb6d277adf"//
            RequestManager.sendToken(token,deviceToken: dToken!){ (response, error) in
                if error != nil {
                    WSProgressHUD.dismiss()
                } else {
                    
//                    self.showCollaborationCorner();
                    self.showNewHome();
//                    self.showNextStep();
                    WSProgressHUD.dismiss()
                }
                
                return nil
            }
            
        }
        
    }
    
    func showNewHome(){
        let mainStoryboard = UIStoryboard(name: "CollaborationCorner2", bundle: nil)
        let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
        
        var centerViewController = mainStoryboard.instantiateViewControllerWithIdentifier("MainViewController") as! MainViewController
        
        var leftViewController = mainStoryboard.instantiateViewControllerWithIdentifier("SideBarViewController") as! SideBarViewController
        
        var leftSideNav = UINavigationController(rootViewController: leftViewController)
        var centerNav = UINavigationController(rootViewController: centerViewController)
        
        appDelegate.centerContainer = MMDrawerController(centerViewController: centerNav, leftDrawerViewController: leftSideNav,rightDrawerViewController:nil)
//        appDelegate.refreshCurrentUsersLocation()
        appDelegate.centerContainer!.openDrawerGestureModeMask = MMOpenDrawerGestureMode.PanningCenterView;
        appDelegate.centerContainer!.closeDrawerGestureModeMask = MMCloseDrawerGestureMode.PanningCenterView;
        
        self.presentViewController(appDelegate.centerContainer!, animated: true) { () -> Void in
            WSProgressHUD.dismiss()
        }
    }
    
    func showCollaborationCorner(){
        let storyboard = UIStoryboard(name: "CollaborationCorner", bundle: nil)
        let vc = storyboard.instantiateViewControllerWithIdentifier("CollaborationCornerNavigationController") as! UINavigationController
        self.presentViewController(vc, animated: true) { () -> Void in
            WSProgressHUD.dismiss()
            let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
//            appDelegate.refreshCurrentUsersLocation()
            ATConnect.sharedConnection().engage("Login", fromViewController: vc)
        }
    }
    
    /*
    -(void)loginButtonClicked
    {
    FBSDKLoginManager *login = [[FBSDKLoginManager alloc] init];
    [login
    logInWithReadPermissions: @[@"public_profile"]
    fromViewController:self
    handler:^(FBSDKLoginManagerLoginResult *result, NSError *error) {
    if (error) {
    NSLog(@"Process error");
    } else if (result.isCancelled) {
    NSLog(@"Cancelled");
    } else {
    NSLog(@"Logged in");
    }
    }];
    }
*/
    override func viewDidAppear(animated: Bool) {
        
        self.navigationController?.navigationBar.hidden = true
        
        self.navigationController?.navigationBar.barTintColor = UIColor(red: 25/255.0, green: 80/255.0, blue: 144/255.0, alpha: 1.0)
        self.navigationController?.navigationBar.tintColor = UIColor.whiteColor()
        self.navigationController?.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName : UIColor.whiteColor()]
        if self.mode == "settings"{
            self.navigationItem.leftBarButtonItem=nil
        }
//        firstNameField?.becomeFirstResponder()
//        if FBSDKAccessToken.currentAccessToken() != nil {
//            self.fbBtn.setTitle("Load your information from facebook", forState: UIControlState.Normal)
//        }else{
//            self.fbBtn.setTitle("Log in with facebook", forState: UIControlState.Normal)
//        }
        
    }

//    NSMutableDictionary* parameters = [NSMutableDictionary dictionary];
//    [parameters setValue:@"id,name,email" forKey:@"fields"];
//    
//    [[[FBSDKGraphRequest alloc] initWithGraphPath:@"me" parameters:parameters]
//    startWithCompletionHandler:^(FBSDKGraphRequestConnection *connection,
//    id result, NSError *error) {
//    aHandler(result, error);
//    }];
    
    
    func populateFieldsFromFb(){
        var fb = FBSDKGraphRequest(graphPath: "me", parameters: ["fields": "email,last_name,first_name,name"])
        fb.startWithCompletionHandler { (conn:FBSDKGraphRequestConnection!, result:AnyObject!, error:NSError!) -> Void in
            if error == nil{
                var d=result as? NSDictionary
                if d != nil {
                    if (d?.valueForKey("last_name") != nil){
                        var nm:String! = d?.valueForKey("last_name") as! String
                        self.lastNameField.text = nm
                    }
                    if (d?.valueForKey("first_name") != nil){
                        var nm:String! = d?.valueForKey("first_name") as! String
                        self.firstNameField.text = nm
                    }
                    if (d?.valueForKey("name") != nil){
                        var nm:String! = d?.valueForKey("name") as! String
                        var txt:NSString = self.firstNameField.text
                        if txt.length<1 {
                            self.firstNameField.text=nm;
                        }
                    }
                    if (d?.valueForKey("email") != nil){
                        var nm:String! = d?.valueForKey("email") as! String
                        self.emailField.text = nm
                    }
                }
            }else{
                var alert = UIAlertController(title: "Error!", message: error.localizedDescription , preferredStyle: UIAlertControllerStyle.Alert)
                alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.Default, handler: nil))
                self.presentViewController(alert, animated: true, completion: nil)
            }
        }
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func textFieldShouldReturn(textField: UITextField) -> Bool
    {
        if (textField == self.firstNameField){
            self.firstNameField?.resignFirstResponder()
            self.lastNameField?.becomeFirstResponder()
        }
        else if (textField == self.lastNameField){
            self.lastNameField?.resignFirstResponder()
            self.emailField?.becomeFirstResponder()
        }
        else if (textField == self.emailField){
            self.emailField?.resignFirstResponder()
            self.passwordField?.becomeFirstResponder()
        }
        else if(textField == self.passwordField){
            self.passwordField.resignFirstResponder()
            self.passwordConfirmationField.becomeFirstResponder()
        }
        else if (textField == self.passwordConfirmationField){
            if(EverythingIsKosher()){
                self.passwordConfirmationField.resignFirstResponder()
                self.didTapNextStep()
            }
        }
        return true
    }
    
    override func touchesBegan(touches: Set<NSObject>, withEvent event: UIEvent) {
        self.view.endEditing(true)
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.navigationBar.hidden = true
        
        //UIColor(red: 5/255, green: 60/255, blue: 115/255, alpha: 1.0)
        self.navigationController?.navigationBar.barTintColor = UIColor(red: 25/255, green: 80/255, blue: 144/255, alpha: 1.0)
        self.navigationController?.navigationBar.tintColor = UIColor.whiteColor()
        self.navigationController?.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName : UIColor.whiteColor()]
        
        
        self.updateRegisterLoginState()

    }
    func setLeftPadding(fld:UITextField){
        let paddingForFirst = UIView(frame: CGRectMake(0, 0, 15, fld.frame.size.height))
        fld.leftView = paddingForFirst
        fld.leftViewMode = UITextFieldViewMode.Always
    }
    func keyboardWillShow(notification: NSNotification) {
        var info = notification.userInfo!
        var keyboardFrame: CGRect = (info[UIKeyboardFrameEndUserInfoKey] as! NSValue).CGRectValue()
        
        var deltaY = keyboardFrame.size.height
        
//        self.bottomConstraint.constant = self.bottomConstraint.constant + deltaY
//
//        self.topConstraint.constant = self.topConstraint.constant - deltaY
        
        UIView.animateWithDuration(0.1, animations: { () -> Void in
            self.view.layoutIfNeeded()
        })
        
    }
    
    func keyboardWillHide(notification: NSNotification) {
//        self.topConstraint.constant = 15
//        self.bottomConstraint.constant = 30
        UIView.animateWithDuration(0.1, animations: { () -> Void in
            self.view.layoutIfNeeded()
        })
    }
    
    @IBAction func didTapNextStep(){
        
        if self.mode == "settings" {
            btnNext.enabled = false;
            WSProgressHUD.show()
            RequestManager.updateCurrentUser(self.firstNameField.text,last_name: self.lastNameField.text,email: self.emailField.text,password: self.passwordField.text) {(response, error) in
            dispatch_async(dispatch_get_main_queue()){
                if error == nil
                {
                    var dic = response as! NSDictionary
                    self.btnNext.enabled = true;
                    WSProgressHUD.dismiss()
                    if dic.objectForKey("error") == nil{
                        self.navigationController?.popViewControllerAnimated(true)
                    }else{
                        AppManager.sharedInstance.showMessage(self, title: "Error", msg: dic.objectForKey("error") as! String, buttonTitle: "Ok")
                    }
                    
                }
                else
                {
                    WSProgressHUD.dismiss()
                    AppManager.sharedInstance.showMessage(self, title: "Error", msg: error?.localizedDescription, buttonTitle: "Cancel")
                    self.btnNext.enabled = true;
                }
            
                }
            }
        } else {
           registerAndShowNextStep()
        }
    }
    
    func useTestUser(){
        firstNameField?.text = "Test#\(arc4random() % 50)"
        lastNameField?.text = "User"
        emailField?.text = "test\(arc4random() % 5000)@gmail.com"
        passwordField?.text = "asdfasdf"
        passwordConfirmationField?.text = "asdfasdf"
    }
    
    func setTextFields() {
        
        
        
        let userDefaults = NSUserDefaults.standardUserDefaults()
        //        let encodedData = NSKeyedArchiver.archivedDataWithRootObject(profile)
        var userData = userDefaults.objectForKey("user") as! NSDictionary // userDefaults.setObject(userDict as NSDictionary, forKey: "user")
        
        firstNameField?.text = userData["first_name"] as! String //self.firstNameFieldText
        lastNameField?.text = userData["last_name"] as! String //self.lastNameFieldText
        emailField?.text = userData["email"] as! String //self.emailFieldText
        self.navigationItem.rightBarButtonItem=UIBarButtonItem(title: "Next", style: UIBarButtonItemStyle.Plain, target: self, action:"didTapNextStep")
    }
    
    func registerAndShowNextStep(){
        btnNext.enabled = false;
        WSProgressHUD.show()
        let delegate = UIApplication.sharedApplication().delegate as! AppDelegate
        var dToken = delegate.deviceTokenString; //"e678643509dc3ff9a6c7e1966a7fe17fbf40d4027eca8c792b192ffb6d277adf"
        println(dToken);
        RequestManager.register(firstNameField.text!, last_name: lastNameField.text!, email: emailField.text!, password: passwordField.text!, deviceToken: dToken!) { (response, error) in
            dispatch_async(dispatch_get_main_queue()){
                if error == nil{
                    WSProgressHUD.dismiss()
                    self.btnNext.enabled = true;
                    var dic:NSDictionary! = response as! NSDictionary
                    if dic.objectForKey("error") != nil{
                        AppManager.sharedInstance.showMessage(self, title: "Error!", msg: dic.objectForKey("error") as! String, buttonTitle: "Cancel")
                    }else{
                       
                        self.showNextStep()
                    }
                    
                }else{
                     WSProgressHUD.dismiss()
                    self.btnNext.enabled = true;
                    AppManager.sharedInstance.showMessage(self, title: "Error!", msg: error?.localizedDescription, buttonTitle: "Cancel")
                }

            }
        }
    }
    
    func showNextStep(){
//        let vc = self.storyboard?.instantiateViewControllerWithIdentifier("ProfileSetup") as! ProfileSetupViewController
//        vc.mode = "signup"
//        let navController = UINavigationController(rootViewController: vc)
//        self.presentViewController(navController, animated: true, completion: nil)
        
        let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
//        appDelegate.refreshCurrentUsersLocation()
        NSOperationQueue.mainQueue().addOperationWithBlock { () -> Void in
            if (self.mode == "signup"){
                let vc = self.storyboard?.instantiateViewControllerWithIdentifier("Interests") as! InterestsViewController
                vc.mode = "signup"
                
                vc.callbackDelegate=self.callbackDelegate
                self.presentViewController(vc, animated: true, completion: nil)
            } else {
                self.navigationController?.popViewControllerAnimated(true)
            }
        }
        
        
    }
    
    func emailIsValid() -> Bool {
        let emailTest = NSPredicate(format:"SELF MATCHES %@", "[A-Z0-9a-z\\._%+-]+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2,4}")
        return emailTest.evaluateWithObject(self.emailField.text)
    }
    
    func matchingPasswords() -> Bool{
        return (self.passwordField.text == self.passwordConfirmationField.text)
    }
    
    func password8CharactersLong() -> Bool{
        return (count(self.passwordField.text) >= 8)
    }
    
    func EverythingIsKosher() -> Bool{
        return (self.emailIsValid() && !self.passwordField.text.isEmpty &&
                !self.firstNameField.text.isEmpty && !self.lastNameField.text.isEmpty
                && !self.passwordConfirmationField.text.isEmpty && self.matchingPasswords()
                && self.password8CharactersLong())
    }
    
    func updateRegisterLoginState(){
        if(EverythingIsKosher()){
            self.enableRegisterButton()
        }else{
            self.disableRegisterButton()
        }
    }
    
    func setupRegisterButton(){
        updateRegisterLoginState()
    }
    
    func enableRegisterButton(){
//        self.navigationItem.rightBarButtonItem=UIBarButtonItem(title: "Next", style: UIBarButtonItemStyle.Plain, target: self, action:"didTapNextStep")
        
        self.btnNext.hidden = false
        
//        self.registerButton.backgroundColor = UIColor(red: 5/255, green: 60/255, blue: 115/255, alpha: 1.0)
    }
    
    func disableRegisterButton(){
//        self.navigationItem.rightBarButtonItem=nil
        self.btnNext.hidden = true
//        self.registerButton.backgroundColor = UIColor(red: 101/255, green: 98/255, blue: 98/255, alpha: 1.0)
    }
    
    func textFieldWasChanged(textField: UITextField){
        self.updateRegisterLoginState()
        if (textField == self.firstNameField){
            if( !self.firstNameField.text.isEmpty ){
                self.firstNameField.backgroundColor = UIColor(red: 170/255, green: 213/255, blue: 246/255, alpha: 1.0)
            }else{
                self.firstNameField.backgroundColor = UIColor.whiteColor()
            }
        }
        else if (textField == self.lastNameField){
            if( !self.lastNameField.text.isEmpty ){
                self.lastNameField.backgroundColor = UIColor(red: 170/255, green: 213/255, blue: 246/255, alpha: 1.0)
            }else{
                self.lastNameField.backgroundColor = UIColor.whiteColor()
            }
        }
        else if (textField == self.emailField){
            if( self.emailIsValid() ){
                self.emailField.backgroundColor = UIColor(red: 170/255, green: 213/255, blue: 246/255, alpha: 1.0)
            }else{
                self.emailField.backgroundColor = UIColor.whiteColor()
            }
            
        }
        else if(textField == self.passwordField){
            if( !self.passwordField.text.isEmpty && password8CharactersLong()){
                self.passwordField.backgroundColor = UIColor(red: 170/255, green: 213/255, blue: 246/255, alpha: 1.0)
            }else{
                self.passwordField.backgroundColor = UIColor.whiteColor()
            }
//            var color = UIColor()
//            matchingPasswords() ? (color = UIColor(red: 170/255, green: 213/255, blue: 246/255, alpha: 1.0)) : (color = UIColor(red: 155/255, green: 30/255, blue: 33/255, alpha: 1.0))
            self.passwordConfirmationField.backgroundColor = UIColor.whiteColor()
        }
        else {
            var color = UIColor()
            matchingPasswords() ? (color = UIColor(red: 170/255, green: 213/255, blue: 246/255, alpha: 1.0)) : (color = UIColor(red: 155/255, green: 30/255, blue: 33/255, alpha: 1.0))
            self.passwordConfirmationField.backgroundColor = color
        }
    }
}
