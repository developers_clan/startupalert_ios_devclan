//
//  ChangeCityViewController.swift
//  Unity for People
//
//  Created by Devclan on 11/12/2015.
//  Copyright (c) 2015 TwinEngineLabs. All rights reserved.
//

import UIKit
import WSProgressHUD
import AFNetworking
class ChangeCityViewController: UIViewController,UITableViewDelegate,UITableViewDataSource {

    @IBOutlet weak var tableVu: UITableView!
    var appDelegate:AppDelegate!
    var cityId:Int! = 0
    override func viewDidLoad() {
        super.viewDidLoad()
        appDelegate=UIApplication.sharedApplication().delegate as! AppDelegate
        var ud = NSUserDefaults.standardUserDefaults()
        
        cityId = ud.integerForKey(Constants.kSelectedCityId)
        
        
        if cityId>0{
        }else{
            ud.setInteger(-1, forKey: Constants.kSelectedCityId)
            ud.synchronize()
        }
//        updateUI()
        fetchCities()
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
//    func updateUI(){
//        if appDelegate.cities.count<1{
//            fetchCities()
//        }else{
//            self.setSelection()
//        }

//    }
    func fetchCities(){
        var p = WSProgressHUD(view: self.view)
        self.view.addSubview(p)
        p.show()
        
        let manager = AFHTTPRequestOperationManager()
        manager.GET("\(Env.ApiUrl)/api/all_cities",
            parameters:nil,
            success:{ operation, responseObject in
                p.dismiss()
                var ud = NSUserDefaults.standardUserDefaults()
                var dic = responseObject as! NSDictionary
                ud.setObject(dic, forKey: Constants.kCitiesResponse)
                ud.synchronize()
                self.appDelegate.cities = self.jsonToCities(dic)
                self.tableVu.reloadData()
                self.setSelection()
                
        },
        failure: { operation, error in
            p.dismiss()
        })
    }
    
    
    func jsonToCities(dic:NSDictionary!)->NSMutableArray{
        var arr2 = NSMutableArray()
        var arr = dic.objectForKey("all_cities") as! NSArray
        for a in arr{
            arr2.addObject(City(dict: a as! NSDictionary))
        }
        return arr2
    }
    @IBAction func cancelPressed(sender: UIButton) {
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    func setSelection(){
        
        var ud = NSUserDefaults.standardUserDefaults()
        let cityId:Int = ud.integerForKey(Constants.kSelectedCityId)
        var ind = -1;
        var i:Int
        for i=0;i<self.appDelegate.cities.count;i++ {
            var c:City! = self.appDelegate.cities.objectAtIndex(i) as! City
            if c.objId == cityId{
                ind = i
            }
        }
        self.tableVu.selectRowAtIndexPath(NSIndexPath(forItem: ind+1, inSection: 0), animated: true, scrollPosition: UITableViewScrollPosition.Middle)
    }
    
    @IBAction func apply(sender: UIButton) {
        AppManager.sharedInstance.needsReloadEvents = true
        AppManager.sharedInstance.needsReloadEventsFav=true
        var ud = NSUserDefaults.standardUserDefaults()
        var ind = tableVu.indexPathForSelectedRow()
        if ind != nil && ind?.row > 0{
            var city = appDelegate.cities.objectAtIndex(ind!.row-1) as! City
            ud.setInteger(city.objId, forKey: Constants.kSelectedCityId)
        }else{
            ud.setInteger(-1, forKey: Constants.kSelectedCityId)
        }
        ud.synchronize()
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        var cell = tableView.dequeueReusableCellWithIdentifier("CityCell") as! CityCell
        if indexPath.row>0 {
            var city = appDelegate.cities.objectAtIndex(indexPath.row-1) as! City
            cell.lbl.text = "\(city.name), \(city.state), \(city.country)"
        }else{
            cell.lbl.text = "Current location"
        }
        
        return cell;
    }
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return appDelegate.cities.count+1
    }
}
