//
//  Constants.swift
//  Unity for People
//
//  Created by Devclan on 11/12/2015.
//  Copyright (c) 2015 TwinEngineLabs. All rights reserved.
//

import UIKit

struct Constants {
    static let kSelectedCityId = "selectedCityId"
    static let DISTANCE = 300//miles
    static let PAGE_SIZE = 10
    static let INITIAL_PAGE = -1
    static let kCitiesResponse = "citiesResponse"
    static let kLoggedIn = "loggedIn"
    static let kFBToken = "fbToken"
    static let darkBlue = UIColor(red: 10/255.0, green: 50/255.0, blue: 95/255.0, alpha: 1)
    static let lightBlue = UIColor(red: 25/255.0, green: 80/255.0, blue: 144/255.0, alpha: 1)

}
