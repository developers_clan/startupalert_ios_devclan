//
//  Badge.swift
//  Unity for People
//
//  Created by Ron De Benedetti on 8/11/15.
//  Copyright (c) 2015 TwinEngineLabs. All rights reserved.
//

class Badge : RLMObject {
    dynamic var id = 0
    dynamic var name = ""
    dynamic var users = RLMArray(objectClassName: User.className())
    
    override class func primaryKey() -> String? {
        return "id"
    }
    
    class var remap : Dictionary<String, String> {
        get {
            return [
                "id" : "id",
                "name" : "name"
            ]
        }
    }
    
    class func createOrUpdateFromArray(array : NSArray) -> Array<Badge> {
        var badges = Array<Badge>()
        for dict in array {
            badges.append(self.createOrUpdateFromDictionary(dict as! NSDictionary) as Badge)
        }
        return badges
    }
    
    class func createOrUpdateFromDictionary(dict : NSDictionary) -> Badge {
        var badge = self.createOrUpdateInDefaultRealmWithValue(Formatter.remapJsonKeys(dict, remapDict: remap))
        return badge as Badge
    }
}

