//
//  ResourcePictureCell.swift
//  Unity for People
//
//  Created by Ron De Benedetti on 8/5/15.
//  Copyright (c) 2015 TwinEngineLabs. All rights reserved.
//

import UIKit

class ResourcePictureCell: UITableViewCell {
    
    @IBOutlet var myTitle: UILabel!
    @IBOutlet var myLocation: UILabel!
    @IBOutlet var myImage: UIImageView!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setResources(title: String, location: String, picture_url: String){
        myTitle.text = title
        myLocation.text = location
        if let url = NSURL(string: picture_url){
            if let data = NSData(contentsOfURL: url){
                let myImage = UIImage(data: data)
            }
        }
    }

}
