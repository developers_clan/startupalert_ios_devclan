//
//  LandingViewController.swift
//  Unity for People
//
//  Created by Devclan on 11/11/2015.
//  Copyright (c) 2015 TwinEngineLabs. All rights reserved.
//

import UIKit
protocol LandingViewConrollerCallbackDelegate{
    func cancelPressed()
}
class LandingViewController: UIViewController ,LandingViewConrollerCallbackDelegate{

    @IBOutlet weak var signupBtn: UIButton!
    @IBOutlet weak var loginBtn: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        UIApplication.sharedApplication().statusBarStyle = .LightContent
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
        
        self.navigationController?.navigationBar.barTintColor = UIColor(red: 25/255, green: 80/255, blue: 144/255, alpha: 1.0)
        self.navigationController?.navigationBar.tintColor = UIColor.whiteColor()
        self.navigationController?.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName : UIColor.whiteColor()]
        self.navigationController?.navigationBar.hidden = true
        
    }
    func cancelPressed() {
        
    }
    @IBAction func loginPressed(sender: UIButton) {
        var vc:UINavigationController = (self.storyboard?.instantiateViewControllerWithIdentifier("LoginNavController") as? UINavigationController)!
        var signUpVc = vc.viewControllers.last as? SignUpViewController
        signUpVc!.callbackDelegate=self
        self.presentViewController(vc, animated: true) { () -> Void in
            
        }
    }
    @IBAction func signupAction(sender: UIButton) {
//        var vc:UINavigationController = (self.storyboard?.instantiateViewControllerWithIdentifier("SignupNavController") as? UINavigationController)!
//        var regVc = vc.viewControllers.last as? RegistrationViewController
//        regVc!.callbackDelegate=self
        
        var vc:RegistrationViewController = (self.storyboard?.instantiateViewControllerWithIdentifier("Registration") as? RegistrationViewController)!
//        vc!.callbackDelegate=self

        
        self.presentViewController(vc, animated: true) { () -> Void in
            
        }
    }
}
