//
//  ResourceTable.swift
//  Unity for People
//
//  Created by Ron De Benedetti on 8/5/15.
//  Copyright (c) 2015 TwinEngineLabs. All rights reserved.
//

class ResourceTable: UIViewController, UITableViewDataSource {
    
    var resourceCells = []
    
    @IBOutlet var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return Int(resourceCells.count)
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell{
        return resourceCells[indexPath.row] as! UITableViewCell
    }

}
