//
//  Vertical.swift
//  Unity for People
//
//  Created by Ron De Benedetti on 7/25/15.
//  Copyright (c) 2015 TwinEngineLabs. All rights reserved.
//

class Vertical : RLMObject {
    dynamic var id = 0
    dynamic var name = ""
    
    override class func primaryKey() -> String? {
        return "id"
    }
    
    class var remap : Dictionary<String, String> {
        get {
            return [
                "id" : "id",
                "name" : "name"
            ]
        }
    }
    
    class func createOrUpdateFromArray(array : NSArray) -> Array<Vertical> {
        var verticals = Array<Vertical>()
        for dict in array {
            verticals.append(self.createOrUpdateFromDictionary(dict as! NSDictionary) as Vertical)
        }
        return verticals
    }
    
    class func createOrUpdateFromDictionary(dict : NSDictionary) -> Vertical {
        var vertical = self.createOrUpdateInDefaultRealmWithValue(Formatter.remapJsonKeys(dict, remapDict: remap))
        return vertical as Vertical
    }
}
