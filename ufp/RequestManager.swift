//
//  RequestManager.swift
//  ufp
//
//  Created by Ron De Benedetti on 6/15/15.
//  Copyright (c) 2015 TwinEngineLabs. All rights reserved.
//

import AFNetworking
import apptentive_ios
import FBSDKCoreKit
typealias APICompletionBlock = (response: AnyObject?, error: NSError?) -> Void?

class RequestManager {
    
    // MARK: -
    // MARK: Unauthenticated
    
    class func login(email: String, password: String, deviceToken: String, completion: APICompletionBlock? = nil) {
        let manager = AFHTTPRequestOperationManager()
        
        manager.POST("\(Env.ApiUrl)/users/sign_in.json",
            parameters:[
                "email": email,
                "password": password,
                "device_token":deviceToken
            ],
            success:{operation, responseObject in
                dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0)) {
                    var dic:NSDictionary! = responseObject as! NSDictionary
                    if dic.objectForKey("error") != nil{
                        self.showServerErrorMessage((dic.objectForKey("error") as! String), operation: operation)
                        completion!(response:nil, error:NSError(domain: "Error", code: 345, userInfo: nil))
                    }else{
                        self.storeCredentials(responseObject as! NSDictionary)
                        completion!(response:responseObject, error:nil)
                    }
                    
                }
            },
            failure: { operation, error in
                println(error)
                self.showServerErrorMessage("Invalid email/password combination, or server could not be reached", operation: operation)
                completion!(response:nil, error:error)
            }
        )
    }
    
    class func register(first_name: String, last_name: String, email: String, password: String, deviceToken: String, completion: APICompletionBlock? = nil){
        let manager = AFHTTPRequestOperationManager()
        manager.POST("\(Env.ApiUrl)/api/users.json",
            parameters:[
                "first_name": first_name,
                "last_name": last_name,
                "email": email,
                "password": password,
                "device_token":deviceToken
            ],
            success:{operation, responseObject in
                dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0)) {
                    var dic:NSDictionary! = responseObject as! NSDictionary
                    if(dic.objectForKey("error") == nil){
                        self.storeCredentials(responseObject as! NSDictionary)
                    }
                    
                    completion!(response:responseObject, error:nil)
                }
            },
            failure: { operation, error in
                println(error)
                self.showServerErrorMessage("Email is in use, or server could not be reached", operation: operation)
                completion!(response:nil, error:error)
            }
        )
    }
    
    class func storeCredentials(responseObject: NSDictionary) {
        let userDict = responseObject["user"] as! NSDictionary
        
        var profile:Profile = Profile();
        
        profile.id = userDict["id"] as! Int
        profile.firstName = userDict["first_name"] as! String
        profile.lastName = userDict["last_name"] as! String
        profile.email = userDict["email"] as! String
        profile.avatarURL = userDict["avatar_url"] as! String
        profile.authToken = userDict["authentication_token"] as! String
        profile.currentProject = userDict["current_project"] as! String
        
        let realm = RLMRealm.defaultRealm()
        realm.beginWriteTransaction()
        let user = User.createOrUpdateFromDictionary(userDict)
        realm.commitWriteTransaction()
        
        let userDefaults = NSUserDefaults.standardUserDefaults()
        userDefaults.setInteger(user.id, forKey: "currentUserID")
        userDefaults.setObject(userDict["authentication_token"] as! String, forKey: "authenticationToken")
//        let encodedData = NSKeyedArchiver.archivedDataWithRootObject(profile)
        userDefaults.setObject(userDict as NSDictionary, forKey: "user")
        userDefaults.synchronize()
        
        let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
        appDelegate.initializeCurrentUser()
    }
    
    // MARK: -
    // MARK: Users
    
    class func getUsers(completion: APICompletionBlock? = nil) {
        let manager = AFHTTPRequestOperationManager()
        manager.GET("\(Env.ApiUrl)/api/users.json",
            parameters:[
                "authentication_token": authToken
            ],
            success:{operation, responseObject in
                dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0)) {
                    let responseDict = responseObject as! NSDictionary
                    let usersArray = responseDict["users"] as! NSArray
                    
                    let realm = RLMRealm.defaultRealm()
                    realm.beginWriteTransaction()
                    realm.deleteObjects(User.allObjects())
                    var users = User.createOrUpdateFromArray(usersArray)
                    realm.commitWriteTransaction()
                    
                    dispatch_async(dispatch_get_main_queue()) {
                        if completion != nil {
                            completion!(response: responseObject, error: nil)
                        }
                    }
                }
            },
            failure: { operation, error in
                println(error)
                self.showServerErrorMessage("Could Not Fetch Users", operation: operation)
            }
        )
    }
    class func trySendingDeviceTokenIfNotSent(){
        let userDefaults = NSUserDefaults.standardUserDefaults()
        let delegate = UIApplication.sharedApplication().delegate as! AppDelegate
        if count(delegate.deviceTokenString)<1 || AppManager.sharedInstance.deviceTokenSent==true
        {
            return
        }
        else if userDefaults.objectForKey("UserName") != nil && userDefaults.objectForKey("Password") != nil
        {
            RequestManager.login(userDefaults.objectForKey("UserName") as! String, password: userDefaults.objectForKey("Password") as! String, deviceToken: delegate.deviceTokenString){ (response, error) in
                if error==nil{
                    AppManager.sharedInstance.deviceTokenSent=true
                }
                return nil
            }
        }else if FBSDKAccessToken.currentAccessToken() != nil {
            var token = FBSDKAccessToken.currentAccessToken().tokenString
            var dToken = delegate.deviceTokenString
            RequestManager.sendToken(token,deviceToken: dToken!){ (response, error) in
                if error==nil{
                    AppManager.sharedInstance.deviceTokenSent=true
                }
                return nil
            }
        }
    }
    class func getUsersWithSkill(skill: Skill, completion: APICompletionBlock? = nil) {
        let manager = AFHTTPRequestOperationManager()
        manager.GET("\(Env.ApiUrl)/api/skills/\(skill.id)/users.json",
            parameters:[
                "authentication_token": authToken
            ],
            success:{operation, responseObject in
                dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0)) {
                    let responseDict = responseObject as! NSDictionary
                    let usersArray = responseDict["users"] as! NSArray
                    let skillDict = responseDict["skill"] as! NSDictionary
                    
                    let realm = RLMRealm.defaultRealm()
                    realm.beginWriteTransaction()
                    var users = User.createOrUpdateFromArray(usersArray)
                    var skill = Skill.createOrUpdateFromDictionary(skillDict)
                    skill.users.removeAllObjects()
                    skill.users.addObjects(users)
                    realm.commitWriteTransaction()
                    
                    dispatch_async(dispatch_get_main_queue()) {
                        if completion != nil {
                            completion!(response: responseObject, error: nil)
                        }
                    }
                }
            },
            failure: { operation, error in
                println(error)
                self.showServerErrorMessage("Could Not Fetch Users", operation: operation)
            }
        )
    }
    
//    class func getUser(user: User, completion: APICompletionBlock? = nil) {
//        let manager = AFHTTPRequestOperationManager()
//        manager.GET("\(Env.ApiUrl)/api/users/\(user.id).json",
//            parameters:[
//                "authentication_token": authToken
//            ],
//            success:{operation, responseObject in
//                dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0)) {
//                    let responseDict = responseObject as! NSDictionary
//                    let userDict = responseDict["user"] as! NSDictionary
//                    let realm = RLMRealm.defaultRealm()
//                    realm.beginWriteTransaction()
//                    var user = User.createOrUpdateFromDictionary(userDict)
//                    realm.commitWriteTransaction()
//                    
//                    dispatch_async(dispatch_get_main_queue()) {
//                        if completion != nil {
//                            completion!(response: responseObject, error: nil)
//                        }
//                    }
//                }
//            },
//            failure: { operation, error in
//                println(error)
//                self.showServerErrorMessage("Could Not Fetch User", operation: operation)
//            }
//        )
//    }
    
    class func getUser(user: User, completion: APICompletionBlock? = nil) {
        let manager = AFHTTPRequestOperationManager()
        manager.GET("\(Env.ApiUrl)/api/users/\(user.id).json",
            parameters:[
                "authentication_token": authToken
            ],
            success:{operation, responseObject in
                dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0)) {
                    let responseDict = responseObject as! NSDictionary
                    let userDict = responseDict["user"] as! NSDictionary
                    let realm = RLMRealm.defaultRealm()
                    realm.beginWriteTransaction()
                    var user = User.createOrUpdateFromDictionary(userDict)
                    realm.commitWriteTransaction()
                    
                    dispatch_async(dispatch_get_main_queue()) {
                        if completion != nil {
                            completion!(response: responseObject, error: nil)
                        }
                    }
                }
            },
            failure: { operation, error in
                println(error)
                if completion != nil {
                    completion!(response: nil, error: error)
                }
                self.showServerErrorMessage("Could Not Fetch User", operation: operation)
            }
        )
    }
    
    class func updateCurrentUserImage(image: UIImage, completion: APICompletionBlock? = nil){
            
            let manager = AFHTTPRequestOperationManager()
            
            let request = manager.requestSerializer.multipartFormRequestWithMethod("POST",
                URLString: "\(Env.ApiUrl)/api/users/revise.json",
                parameters: [
                    "authentication_token": authToken
                ],
                constructingBodyWithBlock: { (formData) -> Void in
                    formData.appendPartWithFileData(UIImageJPEGRepresentation(image, 0.8), name: "avatar", fileName: "image", mimeType: "image/png")
                },
                error: nil
            )
            
            let operation = manager.HTTPRequestOperationWithRequest(request,
                success:{operation, responseObject in
                    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0)) {
                        completion!(response:responseObject, error:nil)
                    }
                },
                failure: { operation, error in
                    println(error)
                    self.showServerErrorMessage("Could Not Update Profile Picture", operation: operation)
                }
            )
            
            manager.operationQueue.addOperation(operation)
    }
    
    class func updateCurrentUserLocation(latitude: Double, longitude: Double) {
        let manager = AFHTTPRequestOperationManager()
            
        manager.PUT("\(Env.ApiUrl)/api/users/location.json",
            parameters:[
                "authentication_token": authToken,
                "latitude": "\(latitude)",
                "longitude": "\(longitude)"
            ],
            success: nil,
            failure: nil
        )
    }
    
//    class func updateCurrentUser(user: User, var parameters: [String: String]) {
//        let manager = AFHTTPRequestOperationManager()
//        
//        parameters["authentication_token"] = authToken
//        
//        manager.POST("\(Env.ApiUrl)/api/users/revise.json",
//            parameters: parameters,
//            success: nil,
//            failure: nil
//        )
//        
//    }
    
    class func updateCurrentUser(first_name: String, last_name: String, email: String, password: String, completion: APICompletionBlock? = nil) {
        let manager = AFHTTPRequestOperationManager()
        var parameters:NSMutableDictionary!=NSMutableDictionary()
        parameters["authentication_token"] = authToken
        parameters["first_name"] = first_name
        parameters["last_name"] = last_name
        parameters["email"] = email
        parameters["password"] = password
        
        manager.POST("\(Env.ApiUrl)/api/users/revise.json", parameters: parameters,
            success: { (op:AFHTTPRequestOperation!, responseObject:AnyObject!) -> Void in
            
                dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0)) {
                    var dic:NSDictionary! = responseObject as! NSDictionary
                    if(dic.objectForKey("error") == nil){
                        self.storeCredentials(responseObject as! NSDictionary)
                        completion!(response:responseObject, error:nil)
                    }
                    
                }
            
            }) { (op:AFHTTPRequestOperation!, err:NSError!) -> Void in
            
                completion!(response:nil, error:err)
        }
        
    }
    
    // MARK: -
    // MARK: Wizard

    class func step1(badges: String, completion: APICompletionBlock? = nil){
        let manager = AFHTTPRequestOperationManager()
        manager.POST("\(Env.ApiUrl)/api/users/revise.json",
            parameters:[
                "authentication_token": authToken,
                "badges": badges
            ],
            success:{operation, responseObject in
                dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0)) {
                    let responseDict = responseObject as! NSDictionary
                    let userDict = responseDict["user"] as! NSDictionary
                    let realm = RLMRealm.defaultRealm()
                    realm.beginWriteTransaction()
                    var user = User.createOrUpdateFromDictionary(userDict)
                    realm.commitWriteTransaction()
                    
                    completion!(response:responseObject, error:nil)
                }
            },
            failure: { operation, error in
                println(error)
                self.showServerErrorMessage("Could Not Update Information", operation: operation)
            }
        )
    }
    
    class func tokenIsValid() -> Bool {
        let manager = AFHTTPRequestOperationManager()
        var operation = manager.GET(
            "\(Env.ApiUrl)/api/authentication_tokens/\(authToken)/user.json",
            parameters: nil,
            success: nil,
            failure: nil
        )
        manager.operationQueue.waitUntilAllOperationsAreFinished()
        NSRunLoop.currentRunLoop().runUntilDate(NSDate())
        return (operation.response.statusCode >= 200 && operation.response.statusCode < 300)
    }
    
    class func getSkills(completion: APICompletionBlock? = nil) {
        let manager = AFHTTPRequestOperationManager()
        manager.GET("\(Env.ApiUrl)/api/skills.json",
            parameters:[
                "authentication_token": authToken
            ],
            success:{operation, responseObject in
                dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0)) {
                    let responseDict = responseObject as! NSDictionary
                    let skillsArray = responseDict["skills"] as! NSArray
                    
                    let realm = RLMRealm.defaultRealm()
                    realm.beginWriteTransaction()
                    realm.deleteObjects(Skill.allObjects())
                    var skills = Skill.createOrUpdateFromArray(skillsArray)
                    realm.commitWriteTransaction()
                    
                    dispatch_async(dispatch_get_main_queue()) {
                        if completion != nil {
                            completion!(response: responseObject, error: nil)
                        }
                    }
                }
            },
            failure: { operation, error in
                println(error)
                self.showServerErrorMessage("Could Not Fetch Skills", operation: operation)
            }
        )
    }
    
    class func step2(skills: String, completion: APICompletionBlock? = nil){
        let manager = AFHTTPRequestOperationManager()
        manager.POST("\(Env.ApiUrl)/api/users/revise.json",
            parameters:[
                "authentication_token": authToken,
                "skills": skills
            ],
            success:{operation, responseObject in
                dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0)) {
                    let responseDict = responseObject as! NSDictionary
                    let userDict = responseDict["user"] as! NSDictionary
                    let realm = RLMRealm.defaultRealm()
                    realm.beginWriteTransaction()
                    var user = User.createOrUpdateFromDictionary(userDict)
                    realm.commitWriteTransaction()
                    
                    completion!(response:responseObject, error:nil)
                }
            },
            failure: { operation, error in
                println(error)
                self.showServerErrorMessage("Could Not Update Information", operation: operation)
            }
        )

    }
    
    class func getInterests(completion: APICompletionBlock? = nil) {
        let manager = AFHTTPRequestOperationManager()
        manager.GET("\(Env.ApiUrl)/api/interests.json",
            parameters:[
                "authentication_token": authToken
            ],
            success:{operation, responseObject in
//                dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0)) {
                    let responseDict = responseObject as! NSDictionary
                    let interestsArray = responseDict["interests"] as! NSArray
                    
                    let realm = RLMRealm.defaultRealm()
                    realm.beginWriteTransaction()
                    realm.deleteObjects(Interest.allObjects())
                    var interests = Interest.createOrUpdateFromArray(interestsArray)
                    realm.commitWriteTransaction()
                    
//                    dispatch_async(dispatch_get_main_queue()) {
                        if completion != nil {
                            completion!(response: responseObject, error: nil)
                        }
//                    }
//                }
            },
            failure: { operation, error in
                println(error)
                if completion != nil {
                    completion!(response: nil, error: error)
                }
                self.showServerErrorMessage("Could Not Fetch Interests", operation: operation)
            }
        )
    }
    
    class func step3(authentication_token: String, interests: String, completion: APICompletionBlock? = nil){
        let manager = AFHTTPRequestOperationManager()
        manager.POST("\(Env.ApiUrl)/api/users/revise.json",
            parameters:[
                "authentication_token": authentication_token,
                "interests": interests
            ],
            success:{operation, responseObject in
                dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0)) {
                    let responseDict = responseObject as! NSDictionary
                    let userDict = responseDict["user"] as! NSDictionary
                    let realm = RLMRealm.defaultRealm()
                    realm.beginWriteTransaction()
                    var user = User.createOrUpdateFromDictionary(userDict)
                    realm.commitWriteTransaction()
                    
                    completion!(response:responseObject, error:nil)
                }
            },
            failure: { operation, error in
                println(error)
                self.showServerErrorMessage("Could Not Update Information", operation: operation)
            }
        )
        
    }
    
    class func getVerticals(completion: APICompletionBlock? = nil) {
        let manager = AFHTTPRequestOperationManager()
        manager.GET("\(Env.ApiUrl)/api/verticals.json",
            parameters:[
                "authentication_token": authToken
            ],
            success:{operation, responseObject in
                dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0)) {
                    let responseDict = responseObject as! NSDictionary
                    let verticalsArray = responseDict["verticals"] as! NSArray
                    
                    let realm = RLMRealm.defaultRealm()
                    realm.beginWriteTransaction()
                    realm.deleteObjects(Vertical.allObjects())
                    var verticals = Vertical.createOrUpdateFromArray(verticalsArray)
                    realm.commitWriteTransaction()
                    
                    dispatch_async(dispatch_get_main_queue()) {
                        if completion != nil {
                            completion!(response: responseObject, error: nil)
                        }
                    }
                }
            },
            failure: { operation, error in
                println(error)
                self.showServerErrorMessage("Could Not Fetch Verticals", operation: operation)
            }
        )
    }
    
    class func getFundingRounds(completion: APICompletionBlock? = nil) {
        let manager = AFHTTPRequestOperationManager()
        manager.GET("\(Env.ApiUrl)/api/funding_rounds.json",
            parameters:[
                "authentication_token": authToken
            ],
            success:{operation, responseObject in
                dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0)) {
                    let responseDict = responseObject as! NSDictionary
                    let fundingRoundsArray = responseDict["funding_rounds"] as! NSArray
                    
                    let realm = RLMRealm.defaultRealm()
                    realm.beginWriteTransaction()
                    realm.deleteObjects(FundingRound.allObjects())
                    var interests = FundingRound.createOrUpdateFromArray(fundingRoundsArray)
                    realm.commitWriteTransaction()
                    
                    dispatch_async(dispatch_get_main_queue()) {
                        if completion != nil {
                            completion!(response: responseObject, error: nil)
                        }
                    }
                }
            },
            failure: { operation, error in
                println(error)
                self.showServerErrorMessage("Could Not Fetch Funding Rounds", operation: operation)
            }
        )
    }
    
    class func step4(authentication_token: String, current_project: String, vertical: String, funding_round: String, completion: APICompletionBlock? = nil){
        let manager = AFHTTPRequestOperationManager()
        manager.POST("\(Env.ApiUrl)/api/users/revise.json",
            parameters:[
                "authentication_token": authentication_token,
                "current_project": current_project,
                "vertical": vertical,
                "funding_round": funding_round
            ],
            success:{operation, responseObject in
                dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0)) {
                    let responseDict = responseObject as! NSDictionary
                    let userDict = responseDict["user"] as! NSDictionary
                    let realm = RLMRealm.defaultRealm()
                    realm.beginWriteTransaction()
                    var user = User.createOrUpdateFromDictionary(userDict)
                    realm.commitWriteTransaction()
                    
                    completion!(response:responseObject, error:nil)
                }
            },
            failure: { operation, error in
                println(error)
                self.showServerErrorMessage("Could Not Update Information", operation: operation)
            }
        )
        
    }
    
    // MARK: -
    // MARK: Coworking Spaces
    
    class func getCoworkingSpaces(completion: APICompletionBlock? = nil) {
        let manager = AFHTTPRequestOperationManager()
        manager.GET("\(Env.ApiUrl)/api/coworking_spaces.json",
            parameters:[
                "authentication_token": authToken
            ],
            success:{operation, responseObject in
                dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0)) {
                    let responseDict = responseObject as! NSDictionary
                    let coworkingSpacesArray = responseDict["coworking_spaces"] as! NSArray
                    
                    let realm = RLMRealm.defaultRealm()
                    realm.beginWriteTransaction()
                    realm.deleteObjects(CoworkingSpace.allObjects())
                    var coworkingSpaces = CoworkingSpace.createOrUpdateFromArray(coworkingSpacesArray)
                    realm.commitWriteTransaction()
                    
                    dispatch_async(dispatch_get_main_queue()) {
                        if completion != nil {
                            completion!(response: responseObject, error: nil)
                        }
                    }
                }
            },
            failure: { operation, error in
                println(error)
                self.showServerErrorMessage("Could Not Fetch Coworking Spaces", operation: operation)
            }
        )
    }
    
    class func showCoworkingSpace(coworking_space: CoworkingSpace, completion: APICompletionBlock? = nil) {
        let manager = AFHTTPRequestOperationManager()
        manager.GET("\(Env.ApiUrl)/api/coworking_spaces/\(coworking_space.id).json",
            parameters:[
                "authentication_token": authToken
            ],
            success:{operation, responseObject in
                dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0)) {
                    let responseDict = responseObject as! NSDictionary
                    let coworkingSpacesDict = responseDict["coworking_space"] as! NSDictionary
                    
                    let realm = RLMRealm.defaultRealm()
                    realm.beginWriteTransaction()
                    let coworkingSpace = CoworkingSpace.createOrUpdateFromDictionary(coworkingSpacesDict)
                    realm.commitWriteTransaction()
                    
                    dispatch_async(dispatch_get_main_queue()) {
                        if completion != nil {
                            completion!(response: responseObject, error: nil)
                        }
                    }
                }
            },
            failure: { operation, error in
                println(error)
                self.showServerErrorMessage("Could Not Fetch Coworking Space", operation: operation)
            }
        )
    }
    
    // MARK: -
    // MARK: Communities
    
    class func getCommunities(completion: APICompletionBlock? = nil) {
        let manager = AFHTTPRequestOperationManager()
        manager.GET("\(Env.ApiUrl)/api/communities.json",
            parameters:[
                "authentication_token": authToken
            ],
            success:{operation, responseObject in
                dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0)) {
                    let responseDict = responseObject as! NSDictionary
                    let communitiesArray = responseDict["communities"] as! NSArray
                    
                    let realm = RLMRealm.defaultRealm()
                    realm.beginWriteTransaction()
                    realm.deleteObjects(Community.allObjects())
                    var communities = Community.createOrUpdateFromArray(communitiesArray)
                    realm.commitWriteTransaction()
                    
                    dispatch_async(dispatch_get_main_queue()) {
                        if completion != nil {
                            completion!(response: responseObject, error: nil)
                        }
                    }
                }
            },
            failure: { operation, error in
                println(error)
                self.showServerErrorMessage("Could Not Fetch Communities", operation: operation)
            }
        )
    }
    class func showCommunity(community: Community, completion: APICompletionBlock? = nil) {
        let manager = AFHTTPRequestOperationManager()
        manager.GET("\(Env.ApiUrl)/api/communities/\(community.id).json",
            parameters:[
                "authentication_token": authToken
            ],
            success:{operation, responseObject in
                dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0)) {
                    let responseDict = responseObject as! NSDictionary
                    let communityDict = responseDict["community"] as! NSDictionary
                    
                    let realm = RLMRealm.defaultRealm()
                    realm.beginWriteTransaction()
                    let community = Community.createOrUpdateFromDictionary(communityDict)
                    realm.commitWriteTransaction()
                    
                    dispatch_async(dispatch_get_main_queue()) {
                        if completion != nil {
                            completion!(response: responseObject, error: nil)
                        }
                    }
                }
            },
            failure: { operation, error in
                println(error)
                self.showServerErrorMessage("Could Not Fetch Community", operation: operation)
            }
        )
    }
    
    // MARK: -
    // MARK: Investors
    
    class func getInvestors(completion: APICompletionBlock? = nil) {
        let manager = AFHTTPRequestOperationManager()
        manager.GET("\(Env.ApiUrl)/api/investors.json",
            parameters:[
                "authentication_token": authToken
            ],
            success:{operation, responseObject in
                dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0)) {
                    let responseDict = responseObject as! NSDictionary
                    let investorsArray = responseDict["investors"] as! NSArray
                    
                    let realm = RLMRealm.defaultRealm()
                    realm.beginWriteTransaction()
                    realm.deleteObjects(Investor.allObjects())
                    let investors = Investor.createOrUpdateFromArray(investorsArray)
                    realm.commitWriteTransaction()
                    
                    dispatch_async(dispatch_get_main_queue()) {
                        if completion != nil {
                            completion!(response: responseObject, error: nil)
                        }
                    }
                }
            },
            failure: { operation, error in
                println(error)
                self.showServerErrorMessage("Could Not Fetch Investors", operation: operation)
            }
        )
    }
    
    class func showInvestor(investor: Investor, completion: APICompletionBlock? = nil) {
        let manager = AFHTTPRequestOperationManager()
        manager.GET("\(Env.ApiUrl)/api/investors/\(investor.id).json",
            parameters:[
                "authentication_token": authToken
            ],
            success:{operation, responseObject in
                dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0)) {
                    let responseDict = responseObject as! NSDictionary
                    let investorDict = responseDict["investor"] as! NSDictionary
                    
                    let realm = RLMRealm.defaultRealm()
                    realm.beginWriteTransaction()
                    let investor = Investor.createOrUpdateFromDictionary(investorDict)
                    realm.commitWriteTransaction()
                    
                    dispatch_async(dispatch_get_main_queue()) {
                        if completion != nil {
                            completion!(response: responseObject, error: nil)
                        }
                    }
                }
            },
            failure: { operation, error in
                println(error)
                self.showServerErrorMessage("Could Not Fetch Investor", operation: operation)
            }
        )
    }
    
    // MARK: -
    // MARK: Online Resources
    
    class func getOnlineResources(completion: APICompletionBlock? = nil) {
        let manager = AFHTTPRequestOperationManager()
        manager.GET("\(Env.ApiUrl)/api/onlines.json",
            parameters:[
                "authentication_token": authToken
            ],
            success:{operation, responseObject in
                dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0)) {
                    let responseDict = responseObject as! NSDictionary
                    let onlinesArray = responseDict["onlines"] as! NSArray
                    
                    let realm = RLMRealm.defaultRealm()
                    realm.beginWriteTransaction()
                    realm.deleteObjects(Online.allObjects())
                    var onlines = Online.createOrUpdateFromArray(onlinesArray)
                    realm.commitWriteTransaction()
                    
                    dispatch_async(dispatch_get_main_queue()) {
                        if completion != nil {
                            completion!(response: responseObject, error: nil)
                        }
                    }
                }
            },
            failure: { operation, error in
                println(error)
                self.showServerErrorMessage("Could Not Fetch Online Resources", operation: operation)
            }
        )
    }
    
    class func showOnline(online: Online, completion: APICompletionBlock? = nil) {
        let manager = AFHTTPRequestOperationManager()
        manager.GET("\(Env.ApiUrl)/api/onlines/\(online.id).json",
            parameters:[
                "authentication_token": authToken
            ],
            success:{operation, responseObject in
                dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0)) {
                    let responseDict = responseObject as! NSDictionary
                    let onlineDict = responseDict["online"] as! NSDictionary
                    
                    let realm = RLMRealm.defaultRealm()
                    realm.beginWriteTransaction()
                    let online = Online.createOrUpdateFromDictionary(onlineDict)
                    realm.commitWriteTransaction()
                    
                    dispatch_async(dispatch_get_main_queue()) {
                        if completion != nil {
                            completion!(response: responseObject, error: nil)
                        }
                    }
                }
            },
            failure: { operation, error in
                println(error)
                self.showServerErrorMessage("Could Not Fetch Online Resource", operation: operation)
            }
        )
    }
    
    // MARK: -
    // MARK: Events

    class func getEvents(completion: APICompletionBlock? = nil) {
        let manager = AFHTTPRequestOperationManager()
        manager.GET("\(Env.ApiUrl)/api/events.json",
            parameters:[
                "authentication_token": authToken
            ],
            success:{operation, responseObject in
                dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0)) {
                    let responseDict = responseObject as! NSDictionary
                    let wantedEventsArray = responseDict["wanted_events"] as! NSArray
                    let unwantedEventsArray = responseDict["unwanted_events"] as! NSArray
                    
                    let realm = RLMRealm.defaultRealm()
                    realm.beginWriteTransaction()
                    realm.deleteObjects(Event.allObjects())
                    var wantedEvents = Event.createOrUpdateFromArray(wantedEventsArray)
                    var unwantedEvents = Event.createOrUpdateFromArray(unwantedEventsArray)
                    realm.commitWriteTransaction()
                    
                    dispatch_async(dispatch_get_main_queue()) {
                        if completion != nil {
                            completion!(response: responseObject, error: nil)
                        }
                    }
                }
            },
            failure: { operation, error in
                println(error)
                self.showServerErrorMessage("Could Not Fetch Events", operation: operation)
            }
        )
    }
    
    class func getEvent(event: Event, completion: APICompletionBlock? = nil) {
        let manager = AFHTTPRequestOperationManager()
        manager.GET("\(Env.ApiUrl)/api/events/\(event.id).json",
            parameters:[
                "authentication_token": authToken
            ],
            success:{operation, responseObject in
                dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0)) {
                    let responseDict = responseObject as! NSDictionary
                    let eventDict = responseDict["event"] as! NSDictionary
                    
                    let realm = RLMRealm.defaultRealm()
                    realm.beginWriteTransaction()
                    var event = Event.createOrUpdateFromDictionary(eventDict)
                    realm.commitWriteTransaction()
                    
                    dispatch_async(dispatch_get_main_queue()) {
                        if completion != nil {
                            completion!(response: responseObject, error: nil)
                        }
                    }
                }
            },
            failure: { operation, error in
                println(error)
                self.showServerErrorMessage("Could Not Fetch Event", operation: operation)
            }
        )
    }
    
    class func getEventsWithInterest(interest: Interest, completion: APICompletionBlock? = nil) {
        let manager = AFHTTPRequestOperationManager()
        manager.GET("\(Env.ApiUrl)/api/interests/\(interest.id)/events.json",
            parameters:[
                "authentication_token": authToken
            ],
            success:{operation, responseObject in
                dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0)) {
                    let responseDict = responseObject as! NSDictionary
                    let eventsArray = responseDict["events"] as! NSArray
                    let interestDict = responseDict["interest"] as! NSDictionary
                    
                    let realm = RLMRealm.defaultRealm()
                    realm.beginWriteTransaction()
                    var events = Event.createOrUpdateFromArray(eventsArray)
                    var interest = Interest.createOrUpdateFromDictionary(interestDict)
                    interest.events.removeAllObjects()
                    interest.events.addObjects(events)
                    realm.commitWriteTransaction()
                    
                    dispatch_async(dispatch_get_main_queue()) {
                        if completion != nil {
                            completion!(response: responseObject, error: nil)
                        }
                    }
                }
            },
            failure: { operation, error in
                println(error)
                self.showServerErrorMessage("Could Not Fetch Events", operation: operation)
            }
        )
    }
    
    // MARK: -
    // MARK: Discussions
    
    class func getDiscussions(completion: APICompletionBlock? = nil) {
        let manager = AFHTTPRequestOperationManager()
        manager.GET("\(Env.ApiUrl)/api/discussions.json",
            parameters:[
                "authentication_token": authToken
            ],
            success:{operation, responseObject in
                dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0)) {
                    let responseDict = responseObject as! NSDictionary
                    let discussionsArray = responseDict["discussions"] as! NSArray
                    
                    let realm = RLMRealm.defaultRealm()
                    realm.beginWriteTransaction()
                    realm.deleteObjects(Discussion.allObjects())
                    var discussions = Discussion.createOrUpdateFromArray(discussionsArray)
                    realm.commitWriteTransaction()
                    
                    dispatch_async(dispatch_get_main_queue()) {
                        if completion != nil {
                            completion!(response: responseObject, error: nil)
                        }
                    }
                }
            },
            failure: { operation, error in
                println(error)
                self.showServerErrorMessage("Could Not Fetch Discussions", operation: operation)
            }
        )
    }

    class func showDiscussion(discussion: Discussion, completion: APICompletionBlock? = nil) {
        let manager = AFHTTPRequestOperationManager()
        manager.GET("\(Env.ApiUrl)/api/discussions/\(discussion.id).json",
            parameters:[
                "authentication_token": authToken
            ],
            success:{operation, responseObject in
                dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0)) {
                    let responseDict = responseObject as! NSDictionary
                    let discussionDict = responseDict["discussion"] as! NSDictionary
                    let discussionResponsesArray = responseDict["discussion_responses"] as! NSArray
                    
                    let realm = RLMRealm.defaultRealm()
                    realm.beginWriteTransaction()
                    let discussion = Discussion.createOrUpdateFromDictionary(discussionDict)
                    let discussionResponses = DiscussionResponse.createOrUpdateFromArray(discussionResponsesArray)
                    discussion.responses.removeAllObjects()
                    discussion.responses.addObjects(discussionResponses)
                    realm.commitWriteTransaction()
                    
                    dispatch_async(dispatch_get_main_queue()) {
                        if completion != nil {
                            completion!(response: responseObject, error: nil)
                        }
                    }
                }
            },
            failure: { operation, error in
                println(error)
                if completion != nil {
                    completion!(response: nil, error: error)
                }
                self.showServerErrorMessage("Could Not Fetch Discussion", operation: operation)
            }
        )
    }

    
    class func postDiscussion(content: String, completion: APICompletionBlock? = nil) {
        let manager = AFHTTPRequestOperationManager()
        manager.POST("\(Env.ApiUrl)/api/discussions.json",
            parameters:[
                "authorization_token" : authToken,
                "content" : content
            ],
            success:{operation, responseObject in
                dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0)) {
                    let responseDict = responseObject as! NSDictionary
                    let discussionsArray = responseDict["discussions"] as! NSArray
                    
                    let realm = RLMRealm.defaultRealm()
                    realm.beginWriteTransaction()
                    var discussions = Discussion.createOrUpdateFromArray(discussionsArray)
                    realm.commitWriteTransaction()
                    
                    dispatch_async(dispatch_get_main_queue()) {
                        if completion != nil {
                            completion!(response: responseObject, error: nil)
                        }
                    }
                }
            },
            failure: { operation, error in
                if completion != nil {
                    completion!(response: nil, error: error)
                }
                println(error)
                self.showServerErrorMessage("Failed to Submit Your Discussion", operation: operation)
            }
        )
    }
    
    class func postResponse(discussion: Discussion, message: String, completion: APICompletionBlock? = nil) {
        let manager = AFHTTPRequestOperationManager()
        manager.POST("\(Env.ApiUrl)/api/discussions/\(discussion.id)/add_response.json",
            parameters:[
                "authorization_token" : authToken,
                "message" : message
            ],
            success:{operation, responseObject in
                dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0)) {
                    let responseDict = responseObject as! NSDictionary
                    let discussionDict = responseDict["discussion"] as! NSDictionary
                    let discussionResponseDict = responseDict["discussion_response"] as! NSDictionary
                    let realm = RLMRealm.defaultRealm()
                    realm.beginWriteTransaction()
                    let discussion = Discussion.createOrUpdateFromDictionary(discussionDict)
                    let response = DiscussionResponse.createOrUpdateFromDictionary(discussionResponseDict)
                    discussion.responses.addObject(response)
                    realm.commitWriteTransaction()
                    
                    dispatch_async(dispatch_get_main_queue()) {
                        if completion != nil {
                            completion!(response: responseObject, error: nil)
                        }
                    }
                }
            },
            failure: { operation, error in
                println(error)
                if completion != nil {
                    completion!(response: nil, error: error)
                }
                self.showServerErrorMessage("Failed to Submit Your Discussion", operation: operation)
            }
        )
    }
    
    // MARK: -
    // MARK: Utility
    
    class func showServerErrorMessage(title: String, operation: AFHTTPRequestOperation) {
        NSOperationQueue.mainQueue().addOperationWithBlock({ () -> Void in
            let responseObject = operation.responseObject as! NSDictionary?
            var message = responseObject?["error"] as! String? //?? "There was a server error."
            UIAlertView(
                title: "Error",
                message: message,
                delegate: nil,
                cancelButtonTitle: "OK"
                ).show()
        })
        
    }
    
    class func openResetPasswordPage() {
        UIApplication.sharedApplication().openURL(NSURL(string:"\(Env.ApiUrl)/users/password/new")!)
    }
    
    class func resetPass(email: String, completion: APICompletionBlock? = nil) {
        let manager = AFHTTPRequestOperationManager()
        manager.POST("\(Env.ApiUrl)/users/password.json",
            parameters:[
                "email": email,
            ],
            success:{operation, responseObject in
                dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0)) {
                   // self.storeCredentials(responseObject as! NSDictionary)
                    let responseDict = responseObject as! NSObject
                    completion!(response:responseDict, error:nil)
                }
            },
            failure: { operation, error in
                println(error)
                self.showServerErrorMessage("Invalid email, or server could not be reached", operation: operation)
                completion!(response:nil, error:error)
            }
        )
    }
    
    class func sendToken(token: String, deviceToken: String, completion: APICompletionBlock? = nil) {
        let manager = AFHTTPRequestOperationManager()
        manager.POST("\(Env.ApiUrl)/api/facebook_authentication",
            parameters:[
                "token": token,
                "device_token":deviceToken
            ],
            success:{operation, responseObject in
                dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0)) {
                     self.storeCredentials(responseObject as! NSDictionary)
//                    let responseDict = responseObject as! NSObject
                    completion!(response:responseObject, error:nil)
                    
                }
            },
            failure: { operation, error in
                println(error)
                self.showServerErrorMessage("server could not be reached", operation: operation)
                completion!(response:nil, error:error)
            }
        )
    }
    
}
