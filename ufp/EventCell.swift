//
//  EventCell.swift
//  Unity for People
//
//  Created by Ron De Benedetti on 8/13/15.
//  Copyright (c) 2015 TwinEngineLabs. All rights reserved.
//

import UIKit


class EventCell: UITableViewCell {
    
    @IBOutlet var myName: UILabel!
    @IBOutlet var myTime: UILabel!
    @IBOutlet var myImage: UIImageView!

    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.myImage.clipsToBounds = true
    }
    
    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
}
