//
//  CityCell.swift
//  Unity for People
//
//  Created by Devclan on 11/12/2015.
//  Copyright (c) 2015 TwinEngineLabs. All rights reserved.
//

import UIKit

class CityCell: UITableViewCell {

    @IBOutlet weak var lbl: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        var vu = UIView()
        vu.backgroundColor = UIColor(red: 231/255.0, green: 154/255.0, blue: 65/255.0, alpha: 1)
        self.selectedBackgroundView=vu
        self.lbl.highlightedTextColor=UIColor.whiteColor()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
