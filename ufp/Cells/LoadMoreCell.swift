//
//  LoadMoreCell.swift
//  Unity for People
//
//  Created by Devclan on 14/12/2015.
//  Copyright (c) 2015 TwinEngineLabs. All rights reserved.
//

import UIKit
public protocol LoadMoreDelegate{
    func loadMore(type:String!,span:String)
}
class LoadMoreCell: UITableViewCell {
    var type:String!
    var span:String!
    var delegate:LoadMoreDelegate!
    
    @IBOutlet weak var loadBtn: UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
//        self.backgroundColor = UIColor(red: 231/255.0, green: 154/255.0, blue: 65/255.0, alpha: 1)
        loadBtn.layer.cornerRadius=3
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    @IBAction func btnPressed(sender:UIButton!){
        delegate.loadMore(self.type, span: self.span)
    }

}
