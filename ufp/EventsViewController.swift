//
//  EventsViewController.swift
//  Unity for People
//
//  Created by Ron De Benedetti on 8/13/15.
//  Copyright (c) 2015 TwinEngineLabs. All rights reserved.
//

class EventsViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    var wantedEvents: RLMCollection!
    var unwantedEvents: RLMCollection!
    
    @IBOutlet var tableView: UITableView!
    @IBOutlet var btnSkills: UIButton!
    @IBOutlet var btnInterests: UIButton!
    
    override func viewDidLoad(){
        super.viewDidLoad()
        
        btnInterests.titleLabel?.numberOfLines = 1
        btnInterests.titleLabel?.adjustsFontSizeToFitWidth = true
        
        RequestManager.getEvents() { (response, error) in
            dispatch_async(dispatch_get_main_queue()){
                self.tableView.reloadData()
            }
        }
        
        tableView?.tableFooterView = UIView(frame: CGRect.zeroRect)
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        if unwantedEvents == nil {
            return 1
        } else {
            return 2
        }
    }
    
    func tableView(tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        if unwantedEvents == nil {
            return nil
        } else if section == 0 {
            return "Suggested Events"
        } else {
            return "Other Events"
        }
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if (section == 0) {
            return Int(wantedEvents.count)
        } else {
            return Int(unwantedEvents.count)
        }
    }

    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell{
        let cell = tableView.dequeueReusableCellWithIdentifier("EventCellIdentifier") as! EventCell
        
        var event: Event!
        
        if indexPath.section == 0 {
            event = wantedEvents[UInt(indexPath.row)] as! Event
        } else {
            event = unwantedEvents[UInt(indexPath.row)] as! Event
        }
        
        cell.myName.text = event.name
        cell.myTime.text = generateDateString(event)
        cell.myImage.sd_setImageWithURL(NSURL(string: event.thumbnailURL))
        
        return cell
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath){
        
        var selectedEvent: Event!
        
        if indexPath.section == 0 {
            selectedEvent = wantedEvents[UInt(indexPath.row)] as! Event
        } else {
            selectedEvent = unwantedEvents[UInt(indexPath.row)] as! Event
        }
        
        if selectedEvent.invalidated { return }
        
        showEvent(selectedEvent)
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return 90
    }
    
    func showEvent(event: Event){
        RequestManager.getEvent(event){ (response, error) in
            dispatch_async(dispatch_get_main_queue()){
                let vc = self.storyboard?.instantiateViewControllerWithIdentifier("EventShow") as! EventShowViewController
                vc.event = event
                self.navigationController?.pushViewController(vc, animated: true)
            }
        }
    }
    
    func isSameDay(date1: NSDate, date2: NSDate) -> Bool{
        let calendar = NSCalendar.currentCalendar()
        var comps1 = calendar.components(.CalendarUnitMonth | .CalendarUnitYear | .CalendarUnitDay, fromDate:date1)
        var comps2 = calendar.components(.CalendarUnitMonth | .CalendarUnitYear | .CalendarUnitDay, fromDate:date2)
        
        return (comps1.day == comps2.day) && (comps1.month == comps2.month) && (comps1.year == comps2.year)
    }
    
    func generateDateString(event: Event) -> String{
        let startsAt: NSDate = event.starts_at
        let endsAt: NSDate = event.ends_at
        
        if isSameDay(startsAt, date2: endsAt){
            let startsAtFormatter = NSDateFormatter()
            startsAtFormatter.dateStyle = NSDateFormatterStyle.MediumStyle
            startsAtFormatter.timeStyle = NSDateFormatterStyle.ShortStyle
            
            let endsAtFormatter = NSDateFormatter()
            endsAtFormatter.timeStyle = NSDateFormatterStyle.ShortStyle
            
            if startsAt == endsAt {
                return "\(startsAtFormatter.stringFromDate(startsAt)), \(event.location_name)"
            } else {
                return "\(startsAtFormatter.stringFromDate(startsAt))-\(endsAtFormatter.stringFromDate(endsAt)), \(event.location_name)"
            }
        }else{
            let formatter = NSDateFormatter()
            formatter.dateStyle = NSDateFormatterStyle.MediumStyle
            formatter.timeStyle = NSDateFormatterStyle.ShortStyle
            
            return "\(formatter.stringFromDate(startsAt))-\(formatter.stringFromDate(endsAt)), \(event.location_name)"
        }
        
    }
    
    @IBAction func didTapInterestsButton(){
        showSkillsetFilter()
    }
    
    func showSkillsetFilter(){
        let vc = self.storyboard?.instantiateViewControllerWithIdentifier("InterestEventFilter") as! FilterByInterestViewController
        self.navigationController?.pushViewController(vc, animated: true)
    }
}
