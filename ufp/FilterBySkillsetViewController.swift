//
//  FilterBySkillsetViewController.swift
//  Unity for People
//
//  Created by Ron De Benedetti on 8/10/15.
//  Copyright (c) 2015 TwinEngineLabs. All rights reserved.
//

class FilterBySkillsetViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    var skillsets = Skill.allObjects().sortedResultsUsingProperty("name", ascending: true)
    
    @IBOutlet var tableView: UITableView?
    
    override func viewDidLoad(){
        super.viewDidLoad()
        
        RequestManager.getSkills() { (response, error) in
            self.tableView?.reloadData()
            return nil
        }
        
        var nib = UINib(nibName: "SkillsetCell", bundle: nil)
        tableView?.registerNib(nib, forCellReuseIdentifier: "SkillsetCellIdentifier")
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return Int(skillsets.count) + 1
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell{
        
        let cell = tableView.dequeueReusableCellWithIdentifier("SkillsetCellIdentifier") as! SkillsetCell
        
        switch indexPath.row{
        case 0:
            cell.myLabel.text = "SORT BY SKILLSETS"
        default:
            let skill = skillsets[UInt(indexPath.row - 1)] as! Skill
            cell.myLabel.text = skill.name
        }
        
        return cell
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath){
        
        switch indexPath.row{
        case 0:
            showAllSkills()
        default:
            let selectedSkill = skillsets[UInt(indexPath.row - 1)] as! Skill
            
            if selectedSkill.invalidated { return }
            
            showUsersWithSkill(selectedSkill)
        }
    }
    
    func showUsersWithSkill(skill: Skill){
        RequestManager.getUsersWithSkill(skill){ (response, error) in
            dispatch_async(dispatch_get_main_queue()){
                let numberOfViewControllers = self.navigationController?.viewControllers.count
                let vc = self.navigationController?.viewControllers[numberOfViewControllers! - 2] as! ConnectionsViewController
                let predicate = NSPredicate(format: "id != \(currentUserID)")
                vc.users = skill.users.objectsWithPredicate(predicate)
                vc.skillsetsButton.titleLabel!.text = skill.name.uppercaseString
                vc.tableView.reloadData()
                if let navController = self.navigationController {
                    navController.popViewControllerAnimated(true)
                }
            }
        }
    }
    
    func showAllSkills(){
        let numberOfViewControllers = self.navigationController?.viewControllers.count
        let vc = self.navigationController?.viewControllers[numberOfViewControllers! - 2] as! ConnectionsViewController
        let predicate = NSPredicate(format: "id != \(currentUserID)")
        vc.users = User.objectsWithPredicate(predicate)
        vc.skillsetsButton.titleLabel!.text = "SORT BY SKILLSETS"
        vc.tableView.reloadData()
        self.navigationController?.popViewControllerAnimated(true)
    }
}
