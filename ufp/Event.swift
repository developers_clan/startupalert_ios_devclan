//
//  Event.swift
//  ufp
//
//  Created by Ron De Benedetti on 7/13/15.
//  Copyright (c) 2015 TwinEngineLabs. All rights reserved.
//

class Event : RLMObject {
    dynamic var id = 0
    dynamic var name = ""
    dynamic var content = ""
    dynamic var location_name = ""
    dynamic var location_address = ""
    dynamic var starts_at = NSDate()
    dynamic var ends_at = NSDate()
    dynamic var thumbnailURL = ""
    dynamic var pictureURL = ""
    dynamic var purview = false
    
    override class func primaryKey() -> String? {
        return "id"
    }
    
    class var remap : Dictionary<String, String> {
        get {
            return [
                "id" : "id",
                "name" : "name",
                "content" : "content",
                "location_name" : "location_name",
                "location_address" : "location_address",
                "starts_at" : "starts_at",
                "ends_at" : "ends_at",
                "thumbnailURL" : "thumbnailURL",
                "pictureURL" : "pictureURL",
                "interests" : "interests",
                "purview" : "purview"
            ]
        }
    }
    
    class func createOrUpdateFromArray(array : NSArray) -> Array<Event> {
        var events = Array<Event>()
        for dict in array {
            events.append(self.createOrUpdateFromDictionary(dict as! NSDictionary) as Event)
        }
        return events
    }
    
    class func createOrUpdateFromDictionary(dict : NSDictionary) -> Event {
        var event = self.createOrUpdateInDefaultRealmWithValue(Formatter.remapJsonKeys(dict, remapDict: remap))
        return event as Event
    }
}
