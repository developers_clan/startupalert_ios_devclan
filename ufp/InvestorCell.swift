//
//  InvestorCell.swift
//  ufp
//
//  Created by Ron De Benedetti on 7/13/15.
//  Copyright (c) 2015 TwinEngineLabs. All rights reserved.
//

import UIKit

class InvestorCell: UITableViewCell {
    
    @IBOutlet var myTitle: UILabel!
    @IBOutlet var myLocation: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
}
