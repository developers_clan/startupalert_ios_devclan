//
//  EventShowViewController.swift
//  Unity for People
//
//  Created by Ron De Benedetti on 8/16/15.
//  Copyright (c) 2015 TwinEngineLabs. All rights reserved.
//

class EventShowViewController: UIViewController{
    
    var event: Event!
    
    @IBOutlet weak var myImage: UIImageView!
    @IBOutlet weak var myTitle: UILabel!
    @IBOutlet weak var myDate: UITextView!
    @IBOutlet weak var myContent: UITextView!
    @IBOutlet weak var myLocationName: UILabel!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var myLocationAddress: UITextView!
    
    @IBOutlet var contentHeight: NSLayoutConstraint!
    @IBOutlet var addressHeight: NSLayoutConstraint!
    @IBOutlet var scrollViewHeight: NSLayoutConstraint!
    
    override func viewDidLoad(){
        super.viewDidLoad()
        
        myImage.sd_setImageWithURL(NSURL(string: event.pictureURL))
        myTitle.text = event.name
        myDate.text = generateDateString(event)
        myContent.text = event.content
        myLocationName.text = event.location_name
        myLocationAddress.text = event.location_address

    }
    
    override func viewWillAppear(animated: Bool) {
        let maxHeight: CGFloat = 10_000
        let maxWidth: CGFloat = myLocationAddress.bounds.size.width
        
        let contentRect = event.content.boundingRectWithSize(CGSize(width: maxWidth, height: maxHeight),
            options: NSStringDrawingOptions.UsesLineFragmentOrigin,
            attributes: [NSFontAttributeName: UIFont.systemFontOfSize(14.0)],
            context: nil)
        let addressRect = event.location_address.boundingRectWithSize(CGSize(width: maxWidth, height: maxHeight),
            options: NSStringDrawingOptions.UsesLineFragmentOrigin,
            attributes: [NSFontAttributeName: UIFont.systemFontOfSize(14.0)],
            context: nil)
        let dateRect = generateDateString(event).boundingRectWithSize(CGSize(width: maxWidth, height: maxHeight),
            options: NSStringDrawingOptions.UsesLineFragmentOrigin,
            attributes: [NSFontAttributeName: UIFont.systemFontOfSize(14.0)],
            context: nil)
        
        self.myImage.clipsToBounds = true
        
        var contentFrame = myContent.frame
        contentFrame.size.height = contentRect.size.height
        myContent.frame = contentFrame
        
        var addressFrame = myLocationAddress.frame
        addressFrame.size.height = addressRect.size.height
        myLocationAddress.frame = addressFrame
        
        var dateFrame = myDate.frame
        dateFrame.size.height = dateRect.size.height
        myDate.frame = dateFrame
        
    }
    
    override func viewDidLayoutSubviews() {
        let myContentHeight = myContent.bounds.size.height
        let myAddressHeight = myLocationAddress.bounds.size.height
        let myImageHeight = myImage.bounds.size.height
        let myDateHeight = myDate.bounds.size.height
        
        var totalHeight = myContentHeight + myAddressHeight + myImageHeight + myDateHeight + 100
        
        self.view.layoutIfNeeded()
        
        scrollView.contentSize.height = totalHeight
    }
    
    func isSameDay(date1: NSDate, date2: NSDate) -> Bool{
        let calendar = NSCalendar.currentCalendar()
        var comps1 = calendar.components(.CalendarUnitMonth | .CalendarUnitYear | .CalendarUnitDay, fromDate:date1)
        var comps2 = calendar.components(.CalendarUnitMonth | .CalendarUnitYear | .CalendarUnitDay, fromDate:date2)
        
        return (comps1.day == comps2.day) && (comps1.month == comps2.month) && (comps1.year == comps2.year)
    }
    
    func generateDateString(event: Event) -> String{
        let startsAt: NSDate = event.starts_at
        let endsAt: NSDate = event.ends_at
        
        
        if isSameDay(startsAt, date2: endsAt){
            let startsAtFormatter = NSDateFormatter()
            startsAtFormatter.dateStyle = NSDateFormatterStyle.MediumStyle
            startsAtFormatter.timeStyle = NSDateFormatterStyle.ShortStyle
            
            let endsAtFormatter = NSDateFormatter()
            endsAtFormatter.timeStyle = NSDateFormatterStyle.ShortStyle
            
            return "\(startsAtFormatter.stringFromDate(startsAt))-\(endsAtFormatter.stringFromDate(endsAt))"
        }else{
            let formatter = NSDateFormatter()
            formatter.dateStyle = NSDateFormatterStyle.MediumStyle
            formatter.timeStyle = NSDateFormatterStyle.ShortStyle
            
            return "\(formatter.stringFromDate(startsAt))-\(formatter.stringFromDate(endsAt))"
        }
        
    }
}
