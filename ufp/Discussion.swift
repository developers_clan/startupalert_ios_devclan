//
//  Discussion.swift
//  Unity for People
//
//  Created by Ron De Benedetti on 7/28/15.
//  Copyright (c) 2015 TwinEngineLabs. All rights reserved.
//

class Discussion : RLMObject {
    dynamic var id = 0
    dynamic var content = ""
    dynamic var user_picture_url = ""
    dynamic var posted_at = ""
    dynamic var user_full_name = ""
    dynamic var responses = RLMArray(objectClassName: DiscussionResponse.className())
    
    override class func primaryKey() -> String? {
        return "id"
    }
    
    class var remap : Dictionary<String, String> {
        get {
            return [
                "id" : "id",
                "content" : "content",
                "user_picture_url" : "user_picture_url",
                "posted_at" : "posted_at",
                "user_full_name" : "user_full_name"
            ]
        }
    }
    
    class func createOrUpdateFromArray(array : NSArray) -> Array<Discussion> {
        var discussions = Array<Discussion>()
        for dict in array {
            discussions.append(self.createOrUpdateFromDictionary(dict as! NSDictionary) as Discussion)
        }
        return discussions
    }
    
    class func createOrUpdateFromDictionary(dict : NSDictionary) -> Discussion {
        var discussion = self.createOrUpdateInDefaultRealmWithValue(Formatter.remapJsonKeys(dict, remapDict: remap))
        return discussion as Discussion
    }
}
