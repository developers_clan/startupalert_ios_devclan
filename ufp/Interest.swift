//
//  Interest.swift
//  ufp
//
//  Created by Ron De Benedetti on 7/10/15.
//  Copyright (c) 2015 TwinEngineLabs. All rights reserved.
//

class Interest : RLMObject {
    dynamic var id = 0
    dynamic var name = ""
    dynamic var events = RLMArray(objectClassName: Event.className())
    
    override class func primaryKey() -> String? {
        return "id"
    }
    
    class var remap : Dictionary<String, String> {
        get {
            return [
                "id" : "id",
                "name" : "name"
            ]
        }
    }
    
    class func createOrUpdateFromArray(array : NSArray) -> Array<Interest> {
        var interests = Array<Interest>()
        for dict in array {
            interests.append(self.createOrUpdateFromDictionary(dict as! NSDictionary) as Interest)
        }
        return interests
    }
    
    class func createOrUpdateFromDictionary(dict : NSDictionary) -> Interest {
        var interest = self.createOrUpdateInDefaultRealmWithValue(Formatter.remapJsonKeys(dict, remapDict: remap))
        return interest as Interest
    }
}
