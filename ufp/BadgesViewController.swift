//
//  BadgesViewController.swift
//  Unity for People
//
//  Created by Ron De Benedetti on 7/15/15.
//  Copyright (c) 2015 TwinEngineLabs. All rights reserved.
//

class BadgesViewController: UIViewController {
    
    @IBOutlet var btnEntrepreneur: UIButton!
    @IBOutlet var btnCommunityLeader: UIButton!
    @IBOutlet var btnMentor: UIButton!
    @IBOutlet var btnAngel: UIButton!
    @IBOutlet var btnVentureCapitalist: UIButton!
    
    var selectedBadges = ["", ""]
    
    @IBAction func didTapEntrepreneur(){
        attemptToAddBadge("Entrepreneur")
    }
    
    @IBAction func didTapCommunityLeader(){
        attemptToAddBadge("Community Leader")
    }
    
    @IBAction func didTapMentor(){
        attemptToAddBadge("Mentor")
    }
    
    @IBAction func didTapAngel(){
        attemptToAddBadge("Angel")
    }
    
    @IBAction func didTapVentureCapitalist(){
        attemptToAddBadge("Venture Capitalist")
    }
    
    func pushBadge(badge: String){
        let first: String = selectedBadges[1] as String
        if (selectedBadges[0] != ""){
            deselectBadge(selectedBadges[0])
        }
        selectedBadges = [first, badge]
    }
    
    func selectBadge(badge: String){
        switch badge{
        case "Entrepreneur":
            btnEntrepreneur.setImage(UIImage(named: "SelectedEntrepreneur"), forState: UIControlState.Normal)
        case "Community Leader":
            btnCommunityLeader.setImage(UIImage(named: "SelectedCommunityLeader"), forState: UIControlState.Normal)
        case "Mentor":
            btnMentor.setImage(UIImage(named: "SelectedMentor"), forState: UIControlState.Normal)
        case "Angel":
            btnAngel.setImage(UIImage(named: "SelectedAngel"), forState: UIControlState.Normal)
        case "Venture Capitalist":
            btnVentureCapitalist.setImage(UIImage(named: "SelectedVentureCapitalist"), forState: UIControlState.Normal)
        default:
            println("excuse?")
        }
    }
    
    func popBadge(index: Int){
        let last: String = selectedBadges[abs(index-1)]
        selectedBadges = ["", last]
    }
    
    func deselectBadge(badge: String){
        switch badge{
        case "Entrepreneur":
            btnEntrepreneur.setImage(UIImage(named: "Entrepreneur"), forState: UIControlState.Normal)
        case "Community Leader":
            btnCommunityLeader.setImage(UIImage(named: "CommunityLeader"), forState: UIControlState.Normal)
        case "Mentor":
            btnMentor.setImage(UIImage(named: "Mentor"), forState: UIControlState.Normal)
        case "Angel":
            btnAngel.setImage(UIImage(named: "Angel"), forState: UIControlState.Normal)
        case "Venture Capitalist":
            btnVentureCapitalist.setImage(UIImage(named: "VentureCapitalist"), forState: UIControlState.Normal)
        default:
            println("excuse?")
        }
    }
    
    func attemptToAddBadge(badge: String){
        if (selectedBadges[0] == badge){
            deselectBadge(badge)
            popBadge(0)
        }
        else if (selectedBadges[1] == badge){
            deselectBadge(badge)
            popBadge(1)
        }
        else{
            selectBadge(badge)
            pushBadge(badge)
        }
    }
    
    func getBadges() -> String{
        return "\(selectedBadges[0]),\(selectedBadges[1])"
    }

}
