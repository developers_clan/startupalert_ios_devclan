//
//  FilterByInterestViewController.swift
//  Unity for People
//
//  Created by Ron De Benedetti on 8/17/15.
//  Copyright (c) 2015 TwinEngineLabs. All rights reserved.
//

class FilterByInterestViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    var interests = Interest.allObjects().sortedResultsUsingProperty("name", ascending: true)
    
    @IBOutlet var tableView: UITableView?
    override func viewDidLoad(){
        super.viewDidLoad()
        
        RequestManager.getInterests() { (response, error) in
            self.tableView?.reloadData()
            return nil
        }
        
        var nib = UINib(nibName: "SkillsetCell", bundle: nil)
        tableView?.registerNib(nib, forCellReuseIdentifier: "SkillsetCellIdentifier")
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return Int(interests.count) + 1
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell{
        
        let cell = tableView.dequeueReusableCellWithIdentifier("SkillsetCellIdentifier") as! SkillsetCell
        
        switch indexPath.row{
        case 0:
            cell.myLabel.text = "SORT BY INTEREST"
        default:
            let interest = interests[UInt(indexPath.row - 1)] as! Interest
            cell.myLabel.text = interest.name
        }
        
        return cell
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath){
        
        switch indexPath.row{
        case 0:
            showMyInterests()
        default:
            let selectedInterest = interests[UInt(indexPath.row - 1)] as! Interest
            
            if selectedInterest.invalidated { return }
            
            showEventsWithInterest(selectedInterest)
        }
    }
    
    func showEventsWithInterest(interest: Interest){
        RequestManager.getEventsWithInterest(interest){ (response, error) in
            dispatch_async(dispatch_get_main_queue()){
                let numberOfViewControllers = self.navigationController?.viewControllers.count
                let vc = self.navigationController?.viewControllers[numberOfViewControllers! - 2] as! EventsViewController
                vc.wantedEvents = interest.events
                vc.unwantedEvents = nil
                vc.btnInterests.titleLabel!.text = interest.name.uppercaseString
                vc.tableView.reloadData()
                if let navController = self.navigationController {
                    navController.popViewControllerAnimated(true)
                }
            }
        }
    }
    
    func showMyInterests(){
        let numberOfViewControllers = self.navigationController?.viewControllers.count
        let vc = self.navigationController?.viewControllers[numberOfViewControllers! - 2] as! EventsViewController
        let wantedEventsPredicate = NSPredicate(format: "purview == true")
        vc.wantedEvents = Event.objectsWithPredicate(wantedEventsPredicate).sortedResultsUsingProperty("starts_at", ascending: true)
        let unwantedEventsPredicate = NSPredicate(format: "purview == false")
        vc.unwantedEvents = Event.objectsWithPredicate(unwantedEventsPredicate).sortedResultsUsingProperty("starts_at", ascending: true)
        vc.btnInterests.titleLabel!.text = "SORT BY INTEREST"
        vc.tableView.reloadData()
        self.navigationController?.popViewControllerAnimated(true)
    }
}
