//
//  EventsSection.swift
//  Unity for People
//
//  Created by Devclan on 09/12/2015.
//  Copyright (c) 2015 TwinEngineLabs. All rights reserved.
//

import UIKit

class EventsSection: NSObject {
    var Title:String!
    var Span:String!
    var Type:String!
    var Events:NSMutableArray!
//    var favEvents:NSMutableArray!
    init(title:String!,type:String!,span:String!,events:NSArray!){
        super.init()
        Title = title
        Span = span
        Type=type
        Events = NSMutableArray()
//        favEvents = NSMutableArray()
        
        for e in events{
            
            var evnt = Event2(dict: e as! NSDictionary,sect:self)
            Events.addObject(evnt)
            
        }
//        self.reloadFavoritesEventData()
    }
//    func reloadFavoritesEventData(){
//        favEvents.removeAllObjects()
//        for e in Events{
//            var evnt = e as! Event2
//            if evnt.isFavorite == true {
//                favEvents.addObject(evnt)
//            }
//        }
//    }
    func append(eventsSection:EventsSection!){
        if eventsSection.Span == self.Span && eventsSection.Type==self.Type{
            for e in eventsSection.Events{
                Events.addObject(e)
            }
//            self.reloadFavoritesEventData()
        }
    }
    func exists(event:Event2!)->Bool{
        for e in Events{
            if(e as! Event2).objId==event.objId{
                return true
            }
        }
        return false
    }
}
