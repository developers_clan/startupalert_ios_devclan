//
//  InterestCollectionCell.swift
//  Unity for People
//
//  Created by Devclan on 12/11/2015.
//  Copyright (c) 2015 TwinEngineLabs. All rights reserved.
//

import UIKit

class InterestCollectionCell: UICollectionViewCell {
    
    @IBOutlet weak var lbl: UILabel!
    override var selected: Bool {
        didSet {
            if self.selected {
                self.lbl.textColor=UIColor.whiteColor()
            }else{
                self.lbl.textColor=UIColor.darkGrayColor()
            }
        }
    }
    override func awakeFromNib() {
        var view=UIView()
        view.backgroundColor=UIColor(red: 231/255.0, green: 154/255.0, blue: 65/255.0, alpha: 1)
        self.selectedBackgroundView=view
    }
}
