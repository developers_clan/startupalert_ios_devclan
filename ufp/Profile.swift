//
//  Profile.swift
//  Unity for People
//
//  Created by Devclan on 01/12/2015.
//  Copyright (c) 2015 TwinEngineLabs. All rights reserved.
//

import UIKit

class Profile: NSObject{
    
    dynamic var id = 0
    dynamic var firstName = ""
    dynamic var lastName = ""
    dynamic var email = ""
    dynamic var avatarURL = ""
    dynamic var currentProject = ""
    dynamic var authToken = ""
    
    var skills: NSMutableArray = []
    var interest: NSMutableArray = []
    var badges: NSMutableArray = []
    
}
