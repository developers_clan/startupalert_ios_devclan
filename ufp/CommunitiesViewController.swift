//
//  CommunitiesViewController.swift
//  ufp
//
//  Created by Ron De Benedetti on 7/13/15.
//  Copyright (c) 2015 TwinEngineLabs. All rights reserved.
//

import UIKit

class CommunitiesViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    var communities = Community.allObjects()
    
    @IBOutlet var tableView: UITableView?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        RequestManager.getCommunities() { (response, error) in
            dispatch_async(dispatch_get_main_queue()){
                self.tableView?.reloadData()
            }
        }
        
        var nib = UINib(nibName: "CoworkingSpacesCell", bundle: nil)
        tableView?.registerNib(nib, forCellReuseIdentifier: "CoworkingSpacesCellIdentifier")
        
        tableView?.tableFooterView = UIView(frame: CGRect.zeroRect)
        
        tableView?.rowHeight = 100
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationItem.title = "Communities"
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return Int(communities.count)
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell{
        let cell = tableView.dequeueReusableCellWithIdentifier("CoworkingSpacesCellIdentifier") as! CoworkingSpacesCell
        let community = communities[UInt(indexPath.row)] as! Community

        cell.myImage.sd_setImageWithURL(NSURL(string: community.picture_url))
        cell.myTitle.text = community.title
        cell.myLocation.text = community.location
        
        return cell
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath){
        let selectedCommunity = UInt(indexPath.row)
        let community = communities[selectedCommunity] as! Community
        
        if community.invalidated { return }
        
        RequestManager.showCommunity(community) { (response, error) in
            dispatch_async(dispatch_get_main_queue()){
                self.showCommunity(community)
            }
        }
    }
    
    func showCommunity(community: Community){
        let vc = self.storyboard?.instantiateViewControllerWithIdentifier("ResourceShow") as! ResourceShowViewController
        vc.setupResourceShow(community.picture_url, content: community.content, phone: community.phone, url: community.url, email: community.email, location: community.location, name: community.title)
        vc.showContactInfo = true
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
}
