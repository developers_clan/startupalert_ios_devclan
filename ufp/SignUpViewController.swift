//
//  SignInController.swift
//  ufp
//
//  Created by Ron De Benedetti on 6/15/15.
//  Copyright (c) 2015 TwinEngineLabs. All rights reserved.
//

import UIKit
import Foundation
import WSProgressHUD
import apptentive_ios
import FBSDKCoreKit
import FBSDKLoginKit

class SignUpViewController: UIViewController, UITextFieldDelegate {
    
    @IBOutlet var emailField :TextField!
    @IBOutlet var passwordField :TextField!
    @IBOutlet var bottomConstraint: NSLayoutConstraint!
    var kbHeight: CGFloat!
    
    @IBOutlet weak var btnCancel: UIBarButtonItem!

    var callbackDelegate:LandingViewConrollerCallbackDelegate!
    override func viewDidLoad(){
        super.viewDidLoad()
        
//        
//        [self.navigationController.navigationBar setBackgroundImage:[UIImage new]
//            
//        self.navigationController.navigationBar.translucent = YES;
//            self.navigationController.view.backgroundColor = [UIColor clearColor];
//            
//            self.navigationController.navigationBar.backgroundColor = UIColor.clearColor
        
            navigationController!.navigationBar.setBackgroundImage(UIImage(), forBarMetrics: UIBarMetrics.Default)
            navigationController!.navigationBar.shadowImage = UIImage()
            navigationController!.navigationBar.translucent = true



        
        NSNotificationCenter.defaultCenter().addObserver(self, selector: Selector("keyboardWillShow:"), name: UIKeyboardWillShowNotification, object: nil)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: Selector("keyboardWillHide:"), name: UIKeyboardWillHideNotification, object: nil)
        
         UIApplication.sharedApplication().statusBarStyle = .LightContent
        
        var color = UIColor(red: 207/255, green: 207/255, blue: 207/255, alpha: 1.0)
        
        self.emailField.layer.borderColor = color.CGColor
        self.emailField.layer.borderWidth = 1.0
        
        self.passwordField.layer.borderColor = color.CGColor
        self.passwordField.layer.borderWidth = 1.0
        
        
        let userDefaults = NSUserDefaults.standardUserDefaults()
//        var userName :String = userDefaults.objectForKey("UserName") as! String
        
        
        
        if userDefaults.objectForKey("UserName") != nil && userDefaults.objectForKey("Password") != nil
        {
            btnCancel.enabled = false
            self.userLogin(userDefaults.objectForKey("UserName") as! String, pass:userDefaults.objectForKey("Password") as! String)
        }
        
        
        
        if Env.name == "development" {
            useTestUser()
        }
    }
    
    @IBAction func fbLoginSignUp(sender: AnyObject) {
        
        if FBSDKAccessToken.currentAccessToken() == nil{
            var login = FBSDKLoginManager()
            login.logInWithReadPermissions(["public_profile","email"], fromViewController: self) {
                (result:FBSDKLoginManagerLoginResult!, error:NSError!) -> Void in
                if error != nil{
                    print("Error: fb login")
                    var alert = UIAlertController(title: "Error!", message: error.localizedDescription , preferredStyle: UIAlertControllerStyle.Alert)
                    alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.Default, handler: nil))
                    self.presentViewController(alert, animated: true, completion: nil)
                }else if result.isCancelled{
                    
                }else{
                    WSProgressHUD.show()
                    var token = result.token.tokenString
                    var ud = NSUserDefaults.standardUserDefaults()
                    ud.setObject(token, forKey: Constants.kFBToken)
                    ud.synchronize()
                    println(token)
                    let delegate = UIApplication.sharedApplication().delegate as! AppDelegate
                    var dToken = delegate.deviceTokenString//"e678643509dc3ff9a6c7e1966a7fe17fbf40d4027eca8c792b192ffb6d277adf"
                    RequestManager.sendToken(token,deviceToken: dToken!){ (response, error) in
                        if error != nil {
                            WSProgressHUD.dismiss()
                            AppManager.sharedInstance.showMessage(self, title: "Error", msg: error?.localizedDescription, buttonTitle: "Cancel")
                        } else {
                            
//                            self.showCollaborationCorner();
//                            self.showNewHome()
                            self.showEventVC();
                        }
                        
                        return nil
                    }
                    
                }
            }
        }else{
            
            var token = FBSDKAccessToken.currentAccessToken().tokenString
            println(token)
            
            WSProgressHUD.show()
            
            let delegate = UIApplication.sharedApplication().delegate as! AppDelegate
            var dToken = delegate.deviceTokenString//"e678643509dc3ff9a6c7e1966a7fe17fbf40d4027eca8c792b192ffb6d277adf"
            
            RequestManager.sendToken(token,deviceToken: dToken!){ (response, error) in
                if error != nil {
                    WSProgressHUD.dismiss()
//                    AppManager.sharedInstance.showMessage(self, title: "Error", msg: error?.localizedDescription, buttonTitle: "Cancel")
                } else {
                    
//                    self.showCollaborationCorner();
                    self.showNewHome()
//                    self.showEventVC();
                    WSProgressHUD.dismiss()
                }
                
                return nil
            }
            
        }
        
    }
    
    func showEventVC(){
        //        let vc = self.storyboard?.instantiateViewControllerWithIdentifier("ProfileSetup") as! ProfileSetupViewController
        //        vc.mode = "signup"
        //        let navController = UINavigationController(rootViewController: vc)
        //        self.presentViewController(navController, animated: true, completion: nil)
        
        let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
        //        appDelegate.refreshCurrentUsersLocation()
        
        NSOperationQueue.mainQueue().addOperationWithBlock { () -> Void in
            let vc = self.storyboard?.instantiateViewControllerWithIdentifier("Interests") as! InterestsViewController
            vc.mode = "signup"
            if vc.lblInterests != nil {
                vc.lblInterests.text="Pick a few types of events you're interested in & we'll notify you about events you care about!"
            }

            vc.callbackDelegate=self.callbackDelegate
            self.presentViewController(vc, animated: true, completion: nil)
        }
    }
    
    
    @IBAction func cancelPressed(sender: AnyObject) {
        if self.callbackDelegate != nil{
            self.callbackDelegate.cancelPressed()
        }
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    func useTestUser() {
        emailField?.text = "test@test.com"
        passwordField?.text = "asdfasdf"
    }
    
    override func touchesBegan(touches: Set<NSObject>, withEvent event: UIEvent) {
        self.view.endEditing(true)
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.navigationBar.hidden = true
        
        
        self.navigationController?.navigationBar.barTintColor = UIColor(red: 25.0/255.0, green: 80.0/255.0, blue: 144.0/255.0, alpha: 1.0)
        self.navigationController?.navigationBar.tintColor = UIColor.whiteColor()
        self.navigationController?.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName : UIColor.whiteColor()]
    }
    
    func keyboardWillShow(notification: NSNotification) {
        var info = notification.userInfo!
        var keyboardFrame: CGRect = (info[UIKeyboardFrameEndUserInfoKey] as! NSValue).CGRectValue()
        
        self.bottomConstraint.constant = keyboardFrame.size.height
        UIView.animateWithDuration(0.1, animations: { () -> Void in
            self.view.layoutIfNeeded()
        })
    }
    
    func keyboardWillHide(notification: NSNotification) {
        self.bottomConstraint.constant = 0
        UIView.animateWithDuration(0.1, animations: { () -> Void in
            self.view.layoutIfNeeded()
        })
    }
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        if (textField == self.emailField){
            self.emailField?.resignFirstResponder()
            self.passwordField?.becomeFirstResponder()
        }else if(textField == self.passwordField){
            self.passwordField?.resignFirstResponder()
            self.loginAndShowCollaborationCorner()
        }
        return true
    }
    
    @IBAction func didTapRegister() {
        let vc = self.storyboard?.instantiateViewControllerWithIdentifier("Registration") as! RegistrationViewController
        vc.mode="signup"
        
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func didTapLogin(){
        self.emailField?.resignFirstResponder()
        self.passwordField?.resignFirstResponder()
        if count(self.emailField.text)>3 && count(self.passwordField.text)>0{
            self.loginAndShowCollaborationCorner()
        }else{
            AppManager.sharedInstance.showMessage(self, title: "Error!", msg: "Please enter credentials", buttonTitle: "Ok")
        }
        
    }
    
    func loginAndShowCollaborationCorner(){
        btnCancel.enabled = false
        self.userLogin(self.emailField.text, pass:self.passwordField.text)
        self.emailField.text = ""
        self.passwordField.text = ""
        
    }
    
    func userLogin(userName: String, pass: String)
    {
        WSProgressHUD.show()
        let delegate = UIApplication.sharedApplication().delegate as! AppDelegate
        var dToken = delegate.deviceTokenString    //"e678643509dc3ff9a6c7e1966a7fe17fbf40d4027eca8c792b192ffb6d277adf"; //
        
        RequestManager.login(userName, password: pass, deviceToken: dToken!) { (response, error) in
            if error != nil {
                
                NSOperationQueue.mainQueue().addOperationWithBlock({ () -> Void in
                    WSProgressHUD.dismiss()
                    self.btnCancel.enabled = true
                })
                
            } else {
                //                self.showCollaborationCorner()
                
                let userDefaults = NSUserDefaults.standardUserDefaults()
                
                userDefaults.removeObjectForKey("UserName")
                userDefaults.removeObjectForKey("Password")
                userDefaults.setBool(true, forKey: Constants.kLoggedIn)
                userDefaults.setObject(userName, forKey: "UserName")
                userDefaults.setObject(pass, forKey: "Password")
                userDefaults.synchronize()
                
                self.btnCancel.enabled = true
                
                
                let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
//                appDelegate.refreshCurrentUsersLocation()
                
                self.showNewHome()
            }
            
            return nil
        }
    }
    
    
    @IBAction func didTapForgotPassword() {
//        RequestManager.openResetPasswordPage()
    }
    
    func showCollaborationCorner(){
        let storyboard = UIStoryboard(name: "CollaborationCorner", bundle: nil)
        let vc = storyboard.instantiateViewControllerWithIdentifier("CollaborationCornerNavigationController") as! UINavigationController
        self.presentViewController(vc, animated: true) { () -> Void in
            WSProgressHUD.dismiss()
            let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
//            appDelegate.refreshCurrentUsersLocation()
            ATConnect.sharedConnection().engage("Login", fromViewController: vc)
        }
    }
    
    func showNewHome(){
        
        
        let mainStoryboard = UIStoryboard(name: "CollaborationCorner2", bundle: nil)
        let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
        
        var centerViewController = mainStoryboard.instantiateViewControllerWithIdentifier("MainViewController") as! MainViewController
        
        var leftViewController = mainStoryboard.instantiateViewControllerWithIdentifier("SideBarViewController") as! SideBarViewController
        
        var leftSideNav = UINavigationController(rootViewController: leftViewController)
        var centerNav = UINavigationController(rootViewController: centerViewController)
        
        appDelegate.centerContainer = MMDrawerController(centerViewController: centerNav, leftDrawerViewController: leftSideNav,rightDrawerViewController:nil)
        appDelegate.centerContainer!.openDrawerGestureModeMask = MMOpenDrawerGestureMode.PanningCenterView;
        appDelegate.centerContainer!.closeDrawerGestureModeMask = MMCloseDrawerGestureMode.PanningCenterView;

        NSOperationQueue.mainQueue().addOperationWithBlock { () -> Void in
            WSProgressHUD.dismiss()
            
            self.view.addSubview(appDelegate.centerContainer!.view)
            UIView.transitionWithView(self.view, duration: 0.4, options: UIViewAnimationOptions.TransitionFlipFromRight, animations: { () -> Void in
                
                }) { (b:Bool) -> Void in
                    appDelegate.centerContainer?.view.removeFromSuperview()
                    appDelegate.window?.rootViewController = appDelegate.centerContainer
            }    
        }
//        self.presentViewController(appDelegate.centerContainer!, animated: true) { () -> Void in
//            
//        
////        appDelegate.window?.rootViewController = appDelegate.centerContainer
////        appDelegate.window?.makeKeyAndVisible()
//
//        
//        }
    }
    
}