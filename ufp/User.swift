//
//  User.swift
//  ufp
//
//  Created by Ron De Benedetti on 6/17/15.
//  Copyright (c) 2015 TwinEngineLabs. All rights reserved.
//

var currentUserID = -1
var authToken = ""

class User : RLMObject {
    dynamic var id = 0
    dynamic var first_name = ""
    dynamic var last_name = ""
    dynamic var email = ""
    dynamic var avatarURL = ""
    dynamic var currentProject = ""
    
    dynamic var skills = RLMArray(objectClassName: Skill.className())
    dynamic var interests = RLMArray(objectClassName: Interest.className())
    dynamic var badges = RLMArray(objectClassName: Badge.className())
    dynamic var vertical = Vertical()
    dynamic var fundingRound = FundingRound()
    
    override class func primaryKey() -> String? {
        return "id"
    }
    
    class var remap : Dictionary<String, String> {
        get {
            return [
                "id" : "id",
                "first_name" : "first_name",
                "last_name" : "last_name",
                "email" : "email",
                "avatar_url" : "avatarURL",
                "current_project" : "currentProject"
            ]
        }
    }
    
    class var current: User? {
        get {
            return User(forPrimaryKey: currentUserID)
        }
    }
    
    class func createOrUpdateFromArray(array : NSArray) -> Array<User> {
        var models = Array<User>()
        for dict in array {
            models.append(self.createOrUpdateFromDictionary(dict as! NSDictionary) as User)
        }
        return models
    }
    
    
    class func createOrUpdateFromDictionary(dict : NSDictionary) -> User {
        var user = self.createOrUpdateInDefaultRealmWithValue(
            Formatter.remapJsonKeys(dict, remapDict: remap)
        )
        
        if let skillsArray = dict["skills"] as? NSArray {
            var skills = Skill.createOrUpdateFromArray(skillsArray)
            user.skills.removeAllObjects()
            user.skills.addObjects(skills)
        }
        
        if let interestsArray = dict["interests"] as? NSArray {
            var interests = Interest.createOrUpdateFromArray(interestsArray)
            user.interests.removeAllObjects()
            user.interests.addObjects(interests)
        }
        
        if let badgesArray = dict["badges"] as? NSArray {
            var badges = Badge.createOrUpdateFromArray(badgesArray)
            user.badges.removeAllObjects()
            user.badges.addObjects(badges)
        }
        
        if let fundingRoundDict = dict["funding_round"] as? NSDictionary {
            var fundingRound = FundingRound.createOrUpdateFromDictionary(fundingRoundDict)
            user.fundingRound = fundingRound
        }
        
        if let verticalDict = dict["vertical"] as? NSDictionary {
            var vertical = Vertical.createOrUpdateFromDictionary(verticalDict)
            user.vertical = vertical
        }
        
        return user as User
    }
    
    func name() -> String {
        return "\(first_name) \(last_name)"
    }

}
