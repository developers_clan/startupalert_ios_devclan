//
//  CoworkingSpacesMarqueeViewController.swift
//  Unity for People
//
//  Created by Ron De Benedetti on 7/27/15.
//  Copyright (c) 2015 TwinEngineLabs. All rights reserved.
//

class CoworkingSpacesMarqueeViewController: UIViewController {
    
    @IBOutlet weak var myName: UILabel!
    @IBOutlet weak var myLocation: UILabel!
}
