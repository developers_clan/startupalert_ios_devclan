//
//  ResourcesViewController.swift
//  ufp
//
//  Created by Ron De Benedetti on 7/1/15.
//  Copyright (c) 2015 TwinEngineLabs. All rights reserved.
//

import UIKit
import apptentive_ios

class ResourcesViewController: UIViewController, UITableViewDelegate {
    
    var resources = ["Coworking Spaces", "Community Resources", "Find Investors", "Online Resources"]
    
    @IBOutlet var tableView: UITableView?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        var nib = UINib(nibName: "ResourceCell", bundle: nil)
        tableView?.registerNib(nib, forCellReuseIdentifier: "ResourceCellIdentifier")
        
        tableView?.rowHeight = 100
        
        tableView?.tableFooterView = UIView(frame: CGRect.zeroRect)
        
        tableView?.delegate = self
        
        if !RequestManager.tokenIsValid() {
            promptForSignIn()
        } else {
            ATConnect.sharedConnection().engage("Resources Tapped", fromViewController: self)
        }
    }
    
    func promptForSignIn() {
        var signUpStoryboard = UIStoryboard(name: "SignUp", bundle: nil)
        let vc = signUpStoryboard.instantiateViewControllerWithIdentifier("SignUp") as! SignUpViewController
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    override func viewWillAppear(animated: Bool) {
        self.navigationItem.title = "Resources"
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .Plain, target: nil, action: nil)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return resources.count
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        let selectedItem = resources[indexPath.row]
        switch selectedItem{
        case "Coworking Spaces":
            let coworkingSpacesViewController = self.storyboard?.instantiateViewControllerWithIdentifier("CoworkingSpaces") as! CoworkingSpacesViewController
            self.navigationController?.pushViewController(coworkingSpacesViewController, animated: true)
            ATConnect.sharedConnection().engage("Coworking Spaces Tapped", fromViewController: coworkingSpacesViewController)
        case "Community Resources":
            let communitesViewController = self.storyboard?.instantiateViewControllerWithIdentifier("Communities") as! CommunitiesViewController
            self.navigationController?.pushViewController(communitesViewController, animated: true)
            ATConnect.sharedConnection().engage("Communities Tapped", fromViewController: communitesViewController)
        case "Find Investors":
            let investorsViewController = self.storyboard?.instantiateViewControllerWithIdentifier("Investors") as! InvestorsViewController
            self.navigationController?.pushViewController(investorsViewController, animated: true)
            ATConnect.sharedConnection().engage("Investors Tapped", fromViewController: investorsViewController)
        default:
            let onlineResourcesViewController = self.storyboard?.instantiateViewControllerWithIdentifier("OnlineResources") as! OnlineResourcesViewController
            self.navigationController?.pushViewController(onlineResourcesViewController, animated: true)
            ATConnect.sharedConnection().engage("Onlines Tapped", fromViewController: onlineResourcesViewController)
        }
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("ResourceCellIdentifier", forIndexPath: indexPath) as! ResourceCell
        
        let selectedItem = resources[indexPath.row]
        switch selectedItem{
        case "Coworking Spaces":
            cell.myImage.image = UIImage(named: "coworking_icon")
        case "Community Resources":
            cell.myImage.image = UIImage(named: "community_icon")
        case "Find Investors":
            cell.myImage.image = UIImage(named: "investors_icon")
        default:
            cell.myImage.image = UIImage(named: "online_icon")
        }
        
        cell.myLabel.text = resources[indexPath.row]
        return cell
    }
}
