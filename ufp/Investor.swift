//
//  Investor.swift
//  ufp
//
//  Created by Ron De Benedetti on 7/13/15.
//  Copyright (c) 2015 TwinEngineLabs. All rights reserved.
//

class Investor : RLMObject {
    dynamic var id = 0
    dynamic var title = ""
    dynamic var content = ""
    dynamic var phone = ""
    dynamic var url = ""
    dynamic var picture_url = ""
    
    dynamic var verticals = RLMArray(objectClassName: Vertical.className())
    dynamic var fundingRounds = RLMArray(objectClassName: FundingRound.className())
    
    override class func primaryKey() -> String? {
        return "id"
    }
    
    class var remap : Dictionary<String, String> {
        get {
            return [
                "id" : "id",
                "title" : "title",
                "content" : "content",
                "phone" : "phone",
                "url" : "url",
                "picture_url" : "picture_url",
                "address": "address"
            ]
        }
    }
    
    class func createOrUpdateFromArray(array : NSArray) -> Array<Investor> {
        var Investors = Array<Investor>()
        for dict in array {
            Investors.append(self.createOrUpdateFromDictionary(dict as! NSDictionary) as Investor)
        }
        return Investors
    }
    
    class func createOrUpdateFromDictionary(dict : NSDictionary) -> Investor {
        let investor = self.createOrUpdateInDefaultRealmWithValue(Formatter.remapJsonKeys(dict, remapDict: remap))
        
        investor.verticals.removeAllObjects()
        investor.verticals.addObjects(Vertical.createOrUpdateFromArray(dict["verticals"] as! NSArray))
        
        investor.fundingRounds.removeAllObjects()
        investor.fundingRounds.addObjects(FundingRound.createOrUpdateFromArray(dict["funding_rounds"] as! NSArray))
        
        return investor
    }
}
