//
//  MainViewController.swift
//  Unity for People
//
//  Created by Devclan on 01/12/2015.
//  Copyright (c) 2015 TwinEngineLabs. All rights reserved.
//

import UIKit
import AFNetworking
import WSProgressHUD
import SDWebImage
import apptentive_ios
import INTULocationManager


class MainViewController: UIViewController, UITableViewDelegate,UITableViewDataSource,FavoriteEventCompletionCallBackDelegate,LoadMoreDelegate {
    var info:String!="Try changing your event interests and we will see if any upcoming events match your interests"
    @IBOutlet weak var segCtrl: UISegmentedControl!
    var dateReader:NSDateFormatter!
    var dateWriter:NSDateFormatter!
    var isRequestInProcess:Bool! = false
    
    var currentMainPage:Int!=Constants.INITIAL_PAGE
    var currentAllWeekPage:Int!=Constants.INITIAL_PAGE+1
    var currentAllMonthPage:Int!=Constants.INITIAL_PAGE+1
    var currentSuggestedWeekPage:Int!=Constants.INITIAL_PAGE+1
    var currentSuggestedMonthPage:Int!=Constants.INITIAL_PAGE+1
    
    
    var currentMainPageFav:Int!=Constants.INITIAL_PAGE
    var currentAllWeekPageFav:Int!=Constants.INITIAL_PAGE+1
    var currentAllMonthPageFav:Int!=Constants.INITIAL_PAGE+1
    var currentSuggestedWeekPageFav:Int!=Constants.INITIAL_PAGE+1
    var currentSuggestedMonthPageFav:Int!=Constants.INITIAL_PAGE+1
    
    
    var counts:TotalCounts!
//    var centerContainer: MMDrawerController?

    @IBOutlet weak var tableVu: UITableView!
    @IBOutlet weak var btnFav: UIButton!
    var suggestedEvents:NSMutableArray!
    var allEvents:NSMutableArray!
    
    var suggestedEventsFav:NSMutableArray!
    var allEventsFav:NSMutableArray!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        counts=TotalCounts()
        dateReader = NSDateFormatter()
        dateWriter = NSDateFormatter()
//        dateReader.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"//"2015-12-19T00:00:00.000-06:00"
        
        let enUSPosixLocale = NSLocale(localeIdentifier: "en_US_POSIX")
        dateReader.locale = enUSPosixLocale
        dateReader.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZZZZZ"
        
        
        dateReader.timeZone=NSTimeZone(abbreviation: "UTC")
//        dateWriter.dateFormat = "MMMM dd"
        dateWriter.dateFormat = "MMM dd | hh:mm a"
        suggestedEvents = NSMutableArray()
        allEvents = NSMutableArray()
        
        suggestedEventsFav = NSMutableArray()
        allEventsFav = NSMutableArray()
        // Do any additional setup after loading the view.
        
        navigationController?.navigationBar.hidden = true
        
        UIApplication.sharedApplication().statusBarStyle = .LightContent
        
        
        //        fetchEvents(0,lat:0,lng:0)
        resetAndLoadEvents()
        resetAndLoadEventsFav()
        
        
        let settings = UIUserNotificationSettings(forTypes: .Sound | .Alert | .Badge , categories: nil)
        UIApplication.sharedApplication().registerUserNotificationSettings(settings)
        UIApplication.sharedApplication().registerForRemoteNotifications()
        
        
    }
    func resetAndLoadEvents(){
        
        if self.btnFav.selected==true{
            return
        }
        self.allEvents.removeAllObjects()
        self.suggestedEvents.removeAllObjects()
        currentMainPage=Constants.INITIAL_PAGE
        currentAllWeekPage=Constants.INITIAL_PAGE+1
        currentAllMonthPage=Constants.INITIAL_PAGE+1
        currentSuggestedWeekPage=Constants.INITIAL_PAGE+1
        currentSuggestedMonthPage=Constants.INITIAL_PAGE+1
        fetchLocationThenEvents(nil,span:nil);
    }
    
    func resetAndLoadEventsFav(){
        if self.btnFav.selected==false{
            return
        }
        
        self.allEventsFav.removeAllObjects()
        self.suggestedEventsFav.removeAllObjects()
        currentMainPageFav=Constants.INITIAL_PAGE
        currentAllWeekPageFav=Constants.INITIAL_PAGE+1
        currentAllMonthPageFav=Constants.INITIAL_PAGE+1
        currentSuggestedWeekPageFav=Constants.INITIAL_PAGE+1
        currentSuggestedMonthPageFav=Constants.INITIAL_PAGE+1
        fetchLocationThenEvents(nil,span:nil);
    }
    @IBAction func eventFilterChanged(){
        if self.segCtrl.selectedSegmentIndex == 1{
            ATConnect.sharedConnection().engage("viewed_all_events", fromViewController: self)
        }else{
            ATConnect.sharedConnection().engage("viewed_suggested_events", fromViewController: self)
        }
        self.tableVu.reloadData()
    }

    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        RequestManager.trySendingDeviceTokenIfNotSent()
        if AppManager.sharedInstance.needsReloadEvents == true {
           resetAndLoadEvents()
        }
        if AppManager.sharedInstance.needsReloadEventsFav == true {
            resetAndLoadEventsFav()
        }
        
        
        self.tableVu.reloadData()
    }
    func fetchLocationThenEvents(type:String?,span:String?){
        if(isRequestInProcess==true){
            return
        }
        btnFav.enabled=false
        isRequestInProcess=true
        var ud = NSUserDefaults.standardUserDefaults()
        let cityId:Int = ud.integerForKey(Constants.kSelectedCityId)
        if cityId>0{
            var dic = ud.objectForKey(Constants.kCitiesResponse) as? NSDictionary
            if dic != nil {
                var arr2 = NSMutableArray()
                var arr = dic!.objectForKey("all_cities") as! NSArray
                for a in arr{
                    arr2.addObject(City(dict: a as! NSDictionary))
                }
                let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
                appDelegate.cities = arr2
                
                
                var i:Int
                var cityFound = false
                for i=0;i<appDelegate.cities.count;i++ {
                    var c:City! = appDelegate.cities.objectAtIndex(i) as! City
                    if c.objId == cityId{
                        cityFound=true
                        fetchEvents(c.lat, lng: c.lng,type:type, sp:span)
                        break
                    }
                }
                if cityFound == false{
                    btnFav.enabled=true
                    isRequestInProcess=false
                }
                
                
            }else{
                btnFav.enabled=true
                isRequestInProcess=false
                let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
                appDelegate.centerContainer!.toggleDrawerSide(MMDrawerSide.Left, animated: true, completion: nil)
                var vc = self.storyboard?.instantiateViewControllerWithIdentifier("ChangeCityViewController") as! ChangeCityViewController
                //            vc.mode = "settings"
                //            vc.registerButtonTitle = "UPDATE"
                self.presentViewController(vc, animated: true, completion: nil)
            }
        }else{
            var locMgr = INTULocationManager.sharedInstance()
            var p = WSProgressHUD(view: self.navigationController?.view)
            self.view.addSubview(p)
            locMgr.requestLocationWithDesiredAccuracy(INTULocationAccuracy.City, timeout: 30.0, delayUntilAuthorized: true) { (location:CLLocation!, accuracy:INTULocationAccuracy, status:INTULocationStatus) -> Void in
                p.dismiss()
                if status == INTULocationStatus.Success{
                    self.fetchEvents(location.coordinate.latitude, lng: location.coordinate.longitude,type:type,sp:span)
                }else{
                    //show error
                    self.btnFav.enabled=true
                    self.isRequestInProcess=false
                    let alert = UIAlertController(title: "Error!", message:"Could not fetch your location, please set location manually from menu", preferredStyle: .Alert)
                    alert.addAction(UIAlertAction(title: "OK", style: .Default) { _ in })
                    self.presentViewController(alert, animated: true){}
                }
            }
        }
    }
    func loadMore(type: String!, span: String) {
        self.fetchLocationThenEvents(type, span: span)
    }
    func fetchEvents(lat:Double!,lng:Double!,type:String?,sp:String?){
        
        if authToken != "" {
            RequestManager.updateCurrentUserLocation(lat, longitude: lng)
        }
        
        
        var url:String!
        var pageCount:Int = Constants.INITIAL_PAGE
        var params = NSMutableDictionary()
        if type==nil || sp==nil{
            url = "\(Env.ApiUrl)/api/events.json"
            pageCount = self.btnFav.selected==false ? currentMainPage+1 : currentMainPageFav+1
        }else{
            url = "\(Env.ApiUrl)/api/get_events"
            params.setObject(type!, forKey: "type")
            params.setObject(sp!, forKey: "span")
            if  sp=="week"{
                if type=="all"{
                    pageCount=btnFav.selected == false ? currentAllWeekPage+1 : currentAllWeekPageFav+1
                }else{
                    pageCount=btnFav.selected == false ? currentSuggestedWeekPage+1 : currentSuggestedWeekPageFav+1
                }
            }
            else{
                if type=="all"{
                    pageCount=btnFav.selected == false ? currentAllMonthPage+1 : currentAllMonthPageFav+1
                }else{
                    pageCount=btnFav.selected == false ? currentSuggestedMonthPage+1 : currentSuggestedMonthPageFav+1
                }
            }
        }

        params.setObject(NSNumber(bool: self.btnFav.selected), forKey: "favourite_events")
        params.setObject("5wUK9sZ9erzYQoRstwFQ", forKey: "authentication_token")
        params.setObject(NSNumber(double: lat), forKey: "lat")
        params.setObject(NSNumber(double: lng), forKey: "lng")
        params.setObject(NSNumber(integer: Constants.PAGE_SIZE), forKey: "per_page")
        params.setObject(NSNumber(integer: Constants.DISTANCE), forKey: "distance")
        params.setObject(NSNumber(integer: pageCount), forKey: "page_count")
        
        var p = WSProgressHUD(view: self.navigationController?.view)
        self.view.addSubview(p)
        p.show()
        let manager = AFHTTPRequestOperationManager()
        manager.GET(url,
            parameters:params,
            success:{operation, responseObject in
                p.dismiss()
                self.btnFav.enabled=true
                self.isRequestInProcess=false
                if self.btnFav.selected==false{
                    AppManager.sharedInstance.needsReloadEvents = false
                }else{
                    AppManager.sharedInstance.needsReloadEventsFav = false
                }
                
                if type==nil || sp==nil {
                    var noEventFound=true
                    if self.btnFav.selected==false{
                        self.currentMainPage=self.currentMainPage+1
                        self.suggestedEvents.removeAllObjects()
                        self.allEvents.removeAllObjects()
                    }else{
                        self.currentMainPageFav=self.currentMainPageFav+1
                        self.suggestedEventsFav.removeAllObjects()
                        self.allEventsFav.removeAllObjects()
                    }
                    let responseDict = responseObject as! NSDictionary
                    
                    var sugEvnts = responseDict.valueForKey("suggested_events") as! NSDictionary
                    self.counts = TotalCounts(dict: responseDict.valueForKey("counts") as! NSDictionary)
                    var arr = sugEvnts.valueForKey("this_Week") as! NSArray
                    var sec:EventsSection! = EventsSection(title: "Next 7 Days",type:"suggested",span:"week", events: arr)
                    if sec.Events.count>0{
                        if self.btnFav.selected==false{
                            self.suggestedEvents.addObject(sec)
                        }else{
                            self.suggestedEventsFav.addObject(sec)
                        }
                        noEventFound=false
                    }
                    
                    
                    sec = EventsSection(title: "Next 30 Days", type:"suggested",span:"month",events: sugEvnts.valueForKey("this_month") as! NSArray)
                    if sec.Events.count>0{
                        if self.btnFav.selected == false{
                            self.suggestedEvents.addObject(sec)
                        }else{
                            self.suggestedEventsFav.addObject(sec)
                        }
                        
                        noEventFound=false
                    }
                    
                    
                    var allEvnts = responseDict.valueForKey("all_events") as! NSDictionary
                    sec = EventsSection(title: "Next 7 Days", type:"all",span:"week",events: allEvnts.valueForKey("this_Week") as! NSArray)
                    if sec.Events.count>0{
                        
                        if self.btnFav.selected == false{
                            self.allEvents.addObject(sec)
                        }else{
                            self.allEventsFav.addObject(sec)
                        }
                        noEventFound=false
                    }
                    
                    sec = EventsSection(title: "Next 30 Days", type:"all",span:"month",events: allEvnts.valueForKey("this_month") as! NSArray)
                    if sec.Events.count>0{
                        if self.btnFav.selected == false{
                            self.allEvents.addObject(sec)
                        }else{
                            self.allEventsFav.addObject(sec)
                        }
                        noEventFound=false
                    }
//                    if noEventFound==true{
//                        let alert = UIAlertController(title: "Warning!", message:"No event found", preferredStyle: .Alert)
//                        alert.addAction(UIAlertAction(title: "OK", style: .Default) { _ in })
//                        self.presentViewController(alert, animated: true){}
//                    }
                   
                }else{
                    let arr = responseObject as! NSArray
                    var sec:EventsSection!
                    var currentSec:EventsSection?
                    if  sp=="week"{
                        if type=="all"{
                            if(self.btnFav.selected==true){
                                self.currentAllWeekPageFav=self.currentAllWeekPageFav+1
                            }else{
                                self.currentAllWeekPage=self.currentAllWeekPage+1
                            }
                        }else{
                            if(self.btnFav.selected == true){
                                self.currentSuggestedWeekPageFav=self.currentSuggestedWeekPageFav+1
                            }else{
                                self.currentSuggestedWeekPage=self.currentSuggestedWeekPage+1
                            }
                        }
                        
                        sec = EventsSection(title: "Next 7 Days",type:type,span:sp, events: arr)
                        currentSec = self.getEventsSection(sec.Span, type: sec.Type)
                        if currentSec != nil{
                            currentSec?.append(sec)
                        }else{
                            if type=="all" && self.btnFav.selected==false{
                                self.allEvents.addObject(sec)
                            }else if type=="suggested" && self.btnFav.selected==false{
                                self.suggestedEvents.addObject(sec)
                            }else if type=="all" && self.btnFav.selected==true{
                                self.allEventsFav.addObject(sec)
                            }else if type=="suggested" && self.btnFav.selected==true{
                                self.suggestedEventsFav.addObject(sec)
                            }
                        }
                    }else{
                        
                        if type=="all"{
                            if self.btnFav.selected==false{
                                self.currentAllMonthPage=self.currentAllMonthPage+1
                            }else{
                                self.currentAllMonthPageFav=self.currentAllMonthPageFav+1
                            }
                            
                        }else{
                            if self.btnFav.selected==false{
                                self.self.currentSuggestedMonthPage=self.self.currentSuggestedMonthPage+1
                            }else{
                                self.self.currentSuggestedMonthPageFav=self.self.currentSuggestedMonthPageFav+1
                            }
                        }
                        
                        sec = EventsSection(title: "Next 30 Days",type:type,span:sp, events: arr)
                        currentSec = self.getEventsSection(sec.Span, type: sec.Type)
                        if currentSec != nil{
                            currentSec?.append(sec)
                        }else{
                            if type=="all" && self.btnFav.selected==false{
                                self.allEvents.addObject(sec)
                            }else if type=="suggested" && self.btnFav.selected==false{
                                self.suggestedEvents.addObject(sec)
                            }else if type=="all" && self.btnFav.selected==true{
                                self.allEventsFav.addObject(sec)
                            }else if type=="suggested" && self.btnFav.selected==true{
                                self.suggestedEventsFav.addObject(sec)
                            }
                        }
                    }
                    
                }
                self.tableVu.reloadData()
            },
            failure: { operation, error in
                p.dismiss()
                self.isRequestInProcess=false
                self.btnFav.enabled=true
                AppManager.sharedInstance.showMessage(self, title: "Error", msg: "Server isn't reachable", buttonTitle: "Cancel")
        })
    }
    
    
    
    
    func getEventsSection(span:String!,type:String!)->EventsSection?{
        var sec:EventsSection? = nil
        var secs:NSMutableArray!
        if type == "all" && btnFav.selected==false{
            secs = allEvents
        }else if type == "suggested" && btnFav.selected==false{
            secs = suggestedEvents
        }else if type == "all" && btnFav.selected==true{
            secs = allEventsFav
        }else if type == "suggested" && btnFav.selected==true{
            secs = suggestedEventsFav
        }
        for s in secs{
            if (s as! EventsSection).Span == span{
                sec = s as? EventsSection
                break
            }
        }
        return sec
    }
    @IBAction func btnMenu(sender: AnyObject) {
         let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
         appDelegate.centerContainer!.toggleDrawerSide(MMDrawerSide.Left, animated: true, completion: nil)
        
    }
    
    @IBAction func btnFav(sender: AnyObject) {
        
        self.btnFav.selected = !self.btnFav.selected;
        
        if self.btnFav.selected == true {
            ATConnect.sharedConnection().engage("only_favorite_events_turned_on", fromViewController: self)
        } else {
            ATConnect.sharedConnection().engage("only_favorite_events_turned_off", fromViewController: self)
        }
        AppManager.sharedInstance.needsReloadEventsFav = true
        AppManager.sharedInstance.needsReloadEvents = true
        
        if self.btnFav.selected == false {
            resetAndLoadEvents()
        }
        else if AppManager.sharedInstance.needsReloadEventsFav == true {
            resetAndLoadEventsFav()
        }
        
        
//        self.tableVu.reloadData()
    }
    
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        var count = 1
        switch segCtrl.selectedSegmentIndex{
        case 0:
            count = self.btnFav.selected==false ? suggestedEvents.count : suggestedEventsFav.count
        case 1:
            count = self.btnFav.selected==false ? allEvents.count : allEventsFav.count
        default:
            return count
        }
        return count==0 ? 1 : count
    }
    
    func tableView(tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        var title = ""
        switch segCtrl.selectedSegmentIndex{
        case 0:
            var secArr = self.btnFav.selected==false ? suggestedEvents : suggestedEventsFav
            if secArr.count==0{
                return nil
            }
            title = self.btnFav.selected==false ? (suggestedEvents.objectAtIndex(section) as! EventsSection).Title : (suggestedEventsFav.objectAtIndex(section) as! EventsSection).Title
        case 1:
            var secArr = self.btnFav.selected==false ? allEvents : allEventsFav
            if secArr.count==0{
                return nil
            }
            title = self.btnFav.selected==false ? (allEvents.objectAtIndex(section) as! EventsSection).Title : (allEventsFav.objectAtIndex(section) as! EventsSection).Title
        default:
            return ""
        }
        return title
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        var count = 0
        var hasMore = false
        switch segCtrl.selectedSegmentIndex{
        case 0:
            if btnFav.selected == true{
                if suggestedEventsFav.count==0{
                    return 1
                }
                count =  (suggestedEventsFav.objectAtIndex(section) as! EventsSection).Events.count
                hasMore = self.hasMore(suggestedEventsFav.objectAtIndex(section) as! EventsSection)
            }else{
                if suggestedEvents.count==0{
                    return 1
                }
                count =  (suggestedEvents.objectAtIndex(section) as! EventsSection).Events.count
                hasMore = self.hasMore(suggestedEvents.objectAtIndex(section) as! EventsSection)
            }
            

        case 1:
            if btnFav.selected == true{
                if allEventsFav.count==0{
                    return 1
                }
                count = (allEventsFav.objectAtIndex(section) as! EventsSection).Events.count
                hasMore = self.hasMore(allEventsFav.objectAtIndex(section) as! EventsSection)
            }else{
                if allEvents.count==0{
                    return 1
                }
                count = (allEvents.objectAtIndex(section) as! EventsSection).Events.count
                hasMore = self.hasMore(allEvents.objectAtIndex(section) as! EventsSection)
            }
        default:
            return 0
        }
        return count > 0 && hasMore==true ? count+1 : count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        var cell:NewEventCell = tableView.dequeueReusableCellWithIdentifier("NewEventCell") as! NewEventCell
        var evnt:Event2!
        switch segCtrl.selectedSegmentIndex{
        case 0:
            
            if btnFav.selected == true{
                if(suggestedEventsFav.count==0){
                    var noDataCell = tableView.dequeueReusableCellWithIdentifier("NoDataFound") as! NoDataCell
                    noDataCell.info.text = info
                    return noDataCell
                }
                if (suggestedEventsFav.objectAtIndex(indexPath.section) as! EventsSection).Events.count <= indexPath.row{
                    var cell:LoadMoreCell = tableView.dequeueReusableCellWithIdentifier("LoadMoreCell") as! LoadMoreCell
                    cell.type = (suggestedEvents.objectAtIndex(indexPath.section) as! EventsSection).Type
                    cell.delegate = self
                    cell.span = (suggestedEvents.objectAtIndex(indexPath.section) as! EventsSection).Span
                    return cell
                }
                
                
                evnt = (suggestedEventsFav.objectAtIndex(indexPath.section) as! EventsSection).Events.objectAtIndex(indexPath.row) as! Event2
            }else{
                if(suggestedEvents.count==0){
                    var noDataCell = tableView.dequeueReusableCellWithIdentifier("NoDataFound") as! NoDataCell
                    noDataCell.info.text = info
                    return noDataCell
                }
                if (suggestedEvents.objectAtIndex(indexPath.section) as! EventsSection).Events.count <= indexPath.row{
                    var cell:LoadMoreCell = tableView.dequeueReusableCellWithIdentifier("LoadMoreCell") as! LoadMoreCell
                    cell.type = (suggestedEvents.objectAtIndex(indexPath.section) as! EventsSection).Type
                    cell.delegate = self
                    cell.span = (suggestedEvents.objectAtIndex(indexPath.section) as! EventsSection).Span
                    return cell
                }
                
                evnt = (suggestedEvents.objectAtIndex(indexPath.section) as! EventsSection).Events.objectAtIndex(indexPath.row) as! Event2
            }
            
            
        case 1:
            if btnFav.selected == true{
                if(allEventsFav.count==0){
                    var noDataCell = tableView.dequeueReusableCellWithIdentifier("NoDataFound") as! NoDataCell
                    noDataCell.info.text = info
                    return noDataCell
                }
                if (allEventsFav.objectAtIndex(indexPath.section) as! EventsSection).Events.count <= indexPath.row{
                    var cell:LoadMoreCell = tableView.dequeueReusableCellWithIdentifier("LoadMoreCell") as! LoadMoreCell
                    cell.delegate = self
                    cell.type = (allEvents.objectAtIndex(indexPath.section) as! EventsSection).Type
                    cell.span = (allEvents.objectAtIndex(indexPath.section) as! EventsSection).Span
                    return cell
                }
                
                evnt = (allEventsFav.objectAtIndex(indexPath.section) as! EventsSection).Events.objectAtIndex(indexPath.row) as! Event2
            }else{
                if(allEvents.count==0){
                    var noDataCell = tableView.dequeueReusableCellWithIdentifier("NoDataFound") as! NoDataCell
                    noDataCell.info.text = info
                    return noDataCell
                }
                if (allEvents.objectAtIndex(indexPath.section) as! EventsSection).Events.count <= indexPath.row{
                    var cell:LoadMoreCell = tableView.dequeueReusableCellWithIdentifier("LoadMoreCell") as! LoadMoreCell
                    cell.delegate = self
                    cell.type = (allEvents.objectAtIndex(indexPath.section) as! EventsSection).Type
                    cell.span = (allEvents.objectAtIndex(indexPath.section) as! EventsSection).Span
                    return cell
                }
                
                evnt = (allEvents.objectAtIndex(indexPath.section) as! EventsSection).Events.objectAtIndex(indexPath.row) as! Event2
            }
        default:
            return cell
        }
        cell.event = evnt
        cell.delegate=self
        cell.name.text = evnt.name
        var date = dateReader.dateFromString(evnt.startsAt)
        var startDateStr = dateWriter.stringFromDate(date!)
        
//        date = dateReader.dateFromString(evnt.endsAt)
//        var endDateStr = dateWriter.stringFromDate(date!)
        
//        cell.from.text = startDateStr+" - "+endDateStr
        cell.from.text = startDateStr
        cell.img.sd_setImageWithURL(NSURL(string: evnt.thumbURL), placeholderImage: UIImage(named: "AppIcon"))
        
        if evnt.intrests.count>0{
            var inte = evnt.intrests.objectAtIndex(0) as! Interest2
            cell.intrst1.text = inte.name
            cell.intrst1.hidden=false
        }else{
            cell.intrst1.hidden=true
        }
        
        if evnt.intrests.count>1{
            var inte = evnt.intrests.objectAtIndex(1) as! Interest2
            cell.intrst2.text = inte.name
            cell.intrst2.hidden=false
        }else{
            cell.intrst2.hidden=true
        }
        
        cell.favBtn.selected=evnt.isFavorite

        cell.contentView.backgroundColor = cell.event.isHighlighted==true ? UIColor(red: 240/255.0, green: 247/255.0, blue: 1, alpha: 1) : UIColor.whiteColor()
        return cell
        
    }
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        switch segCtrl.selectedSegmentIndex{
        case 0:
            
            if btnFav.selected == true{
                if suggestedEventsFav.count==0{
                    return self.tableVu.frame.size.height
                }
                if (suggestedEventsFav.objectAtIndex(indexPath.section) as! EventsSection).Events.count <= indexPath.row{
                    return 33
                }
            }else{
                if suggestedEvents.count==0{
                    return self.tableVu.frame.size.height
                }
                if (suggestedEvents.objectAtIndex(indexPath.section) as! EventsSection).Events.count <= indexPath.row{
                    return 33
                }
            }
            
            
        case 1:
            if btnFav.selected == true{
                if allEventsFav.count==0{
                    return self.tableVu.frame.size.height
                }
                if (allEventsFav.objectAtIndex(indexPath.section) as! EventsSection).Events.count <= indexPath.row{
                    return 33
                }
                
            }else{
                if allEvents.count==0{
                    return self.tableVu.frame.size.height
                }
                if (allEvents.objectAtIndex(indexPath.section) as! EventsSection).Events.count <= indexPath.row{
                    return 33
                }
            }
        default:
            return 111
        }
        return 111
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func tableView(tableView: UITableView, shouldHighlightRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        switch segCtrl.selectedSegmentIndex{
        case 0:
            
            if btnFav.selected == true{
                if suggestedEventsFav.count==0 || (suggestedEventsFav.objectAtIndex(indexPath.section) as! EventsSection).Events.count <= indexPath.row{
                    return false
                }
            }else{
                if suggestedEvents.count==0 || (suggestedEvents.objectAtIndex(indexPath.section) as! EventsSection).Events.count <= indexPath.row{
                    return false
                }
            }
            
            
        case 1:
            if btnFav.selected == true{
                if (allEventsFav.count==0 || (allEventsFav.objectAtIndex(indexPath.section) as! EventsSection).Events.count <= indexPath.row){
                    return false
                }
                
            }else{
                if (allEvents.count==0 || (allEvents.objectAtIndex(indexPath.section) as! EventsSection).Events.count <= indexPath.row){
                    return false
                }
            }
        default:
            return true
        }
        return true
    }
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        var vc:EventDetailViewController! = self.storyboard?.instantiateViewControllerWithIdentifier("EventDetailViewController") as! EventDetailViewController
        var evnt:Event2!
        switch segCtrl.selectedSegmentIndex{
        case 0:
            if btnFav.selected == true{
                evnt = (suggestedEventsFav.objectAtIndex(indexPath.section) as! EventsSection).Events.objectAtIndex(indexPath.row) as! Event2
            }else{
                evnt = (suggestedEvents.objectAtIndex(indexPath.section) as! EventsSection).Events.objectAtIndex(indexPath.row) as! Event2
            }
        case 1:
            if btnFav.selected == true{
                evnt = (allEventsFav.objectAtIndex(indexPath.section) as! EventsSection).Events.objectAtIndex(indexPath.row) as! Event2
            }else{
                evnt = (allEvents.objectAtIndex(indexPath.section) as! EventsSection).Events.objectAtIndex(indexPath.row) as! Event2
            }
        default:
            return
        }
        
        vc.event = evnt
        self.navigationController?.pushViewController(vc, animated: true)
    }
    func completed(event: Event2!) {
        
        if event.isFavorite==true{
            ATConnect.sharedConnection().engage("event_marked_favorite", fromViewController: self)
            
        }else{
            ATConnect.sharedConnection().engage("event_unmarked_favorite", fromViewController: self)
        }
        
        
//        event.section.reloadFavoritesEventData()
        AppManager.sharedInstance.needsReloadEventsFav=true
        AppManager.sharedInstance.needsReloadEvents=true
        self.tableVu.reloadData()
    }
    func hasMore(section:EventsSection!)->Bool{
        var has = false
        
        if  section.Span=="week"{
            if section.Type=="all"{
                has = btnFav.selected==false ? (currentAllWeekPage+1) * Constants.PAGE_SIZE < counts.allEventsForWeek : (currentAllWeekPageFav+1) * Constants.PAGE_SIZE < counts.allEventsForWeekFav
            }else{
                has = btnFav.selected==false ? (currentSuggestedWeekPage+1) * Constants.PAGE_SIZE < counts.suggestedEventsForWeek :  (currentSuggestedWeekPageFav+1) * Constants.PAGE_SIZE < counts.suggestedEventsForWeekFav
            }
        }
        else{
            if section.Type=="all"{
                has = btnFav.selected==false ? (currentAllMonthPage+1) * Constants.PAGE_SIZE < counts.allEventsForMonth : (currentAllMonthPageFav+1) * Constants.PAGE_SIZE < counts.allEventsForMonthFav
            }else{
                has = btnFav.selected==false ? (currentSuggestedMonthPage+1) * Constants.PAGE_SIZE < counts.suggestedEventsForMonth : (currentSuggestedMonthPageFav+1) * Constants.PAGE_SIZE < counts.suggestedEventsForMonthFav
            }
        }
        
        return has
    }
}
