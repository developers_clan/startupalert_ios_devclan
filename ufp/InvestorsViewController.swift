//
//  InvestorsViewController.swift
//  ufp
//
//  Created by Ron De Benedetti on 7/13/15.
//  Copyright (c) 2015 TwinEngineLabs. All rights reserved.
//

import WSProgressHUD
import RateLimit

class InvestorsViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {

    var investors : RLMCollection!
    
    @IBOutlet var tableView: UITableView?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        investors = Investor.allObjects()
        
        PersistentRateLimit.executeBlock({ () -> Void in
            self.investors = Investor.objectsWithPredicate(NSPredicate(value: false))
            WSProgressHUD.show()
            RequestManager.getInvestors() { (response, error) in
                dispatch_async(dispatch_get_main_queue()){
                    self.investors = Investor.allObjects()
                    self.tableView?.reloadData()
                    WSProgressHUD.dismiss()
                }
            }
        }, name: "getInvestors", limit: 24 * 60 * 60)
        
        var nib = UINib(nibName: "CoworkingSpacesCell", bundle: nil)
        tableView?.registerNib(nib, forCellReuseIdentifier: "CoworkingSpacesCellIdentifier")
        
        tableView?.tableFooterView = UIView(frame: CGRect.zeroRect)
        
        tableView?.rowHeight = 100
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationItem.title = "Investors In the Area"
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return Int(investors.count)
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell{
        let cell = tableView.dequeueReusableCellWithIdentifier("CoworkingSpacesCellIdentifier") as! CoworkingSpacesCell
        let investor = investors[UInt(indexPath.row)] as! Investor
        
        cell.myImage.sd_setImageWithURL(NSURL(string: investor.picture_url))
        cell.myTitle.text = investor.title
        cell.myLocation.text = investor.url
        
        return cell
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath){
        let selectedInvestor = UInt(indexPath.row)
        let investor = investors[selectedInvestor] as! Investor
        
        if investor.invalidated { return }
        
        RequestManager.showInvestor(investor) { (response, error) in
            dispatch_async(dispatch_get_main_queue()){
                self.showInvestor(investor)
            }
        }
    }
    
    func showInvestor(investor: Investor) {
        let vc = self.storyboard?.instantiateViewControllerWithIdentifier("ResourceShow") as! ResourceShowViewController
        vc.setupResourceShow(investor.picture_url, content: investor.content, phone: investor.phone, url: investor.url, email: "", location: investor.url, name: investor.title)
        vc.setupInvestorInfo(", ".join(investor.verticals.toArray(Vertical.self).map { $0.name }), fundingRounds: ", ".join(investor.fundingRounds.toArray(FundingRound.self).map { $0.name }), address: "")
        vc.showContactInfo = true
        self.navigationController?.pushViewController(vc, animated: true)
    }

}
