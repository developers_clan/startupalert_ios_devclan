//
//  AppManager.swift
//  Unity for People
//
//  Created by Devclan on 14/12/2015.
//  Copyright (c) 2015 TwinEngineLabs. All rights reserved.
//

import UIKit

class AppManager: NSObject {
    var needsReloadEvents=true;
    var needsReloadEventsFav=true;
    var deviceTokenSent=false;
    func showMessage(viewController:UIViewController!,title:String!,msg:String!,buttonTitle:String!){
        NSOperationQueue.mainQueue().addOperationWithBlock { () -> Void in
            let alert = UIAlertController(title: title, message:msg, preferredStyle: .Alert)
            alert.addAction(UIAlertAction(title: buttonTitle, style: .Default) { _ in })
            viewController.presentViewController(alert, animated: true){}
        }
    }
    class var sharedInstance: AppManager {
        struct Static {
            static let instance: AppManager = AppManager()
        }
        return Static.instance
    }
}
