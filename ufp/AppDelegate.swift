//
//  AppDelegate.swift
//  ufp
//
//  Created by Ron De Benedetti on 6/15/15.
//  Copyright (c) 2015 TwinEngineLabs. All rights reserved.
//

import UIKit
import Fabric
import Crashlytics
//import CoreLocation
import KVNProgress
import INTULocationManager
import apptentive_ios
import FBSDKCoreKit
import WSProgressHUD


@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    var deviceTokenString: String! = ""
    var centerContainer: MMDrawerController?
    var cities:NSMutableArray!
    var launchOpt: [NSObject: AnyObject]?

    func application(application: UIApplication, openURL url: NSURL, sourceApplication: String?, annotation: AnyObject?) -> Bool {
        return FBSDKApplicationDelegate.sharedInstance().application(application, openURL: url, sourceApplication: sourceApplication, annotation: annotation)
    }
    func applicationDidBecomeActive(application: UIApplication) {
        FBSDKAppEvents.activateApp()
        if window != nil && window?.rootViewController != nil{
            ATConnect.sharedConnection().engage("app_became_active", fromViewController:window?.rootViewController )
        }
        
    }
    func application(application: UIApplication, didFinishLaunchingWithOptions launchOptions: [NSObject: AnyObject]?) -> Bool {
        launchOpt=launchOptions
        Fabric.with([Crashlytics()])
        Heap.setAppId(Env.HeapAppId)
        ATConnect.sharedConnection().apiKey = Env.ApptentiveApiKey
        cities=NSMutableArray()
        if Env.name == "development" {
            Heap.enableVisualizer()
        }
        
        self.window = UIWindow(frame: UIScreen.mainScreen().bounds)
        self.window?.makeKeyAndVisible()
        
        printRealmPath()
        performMigration()
        initializeCurrentUser()
//        refreshCurrentUsersLocation()
        configureAppearance()
        showMainView()
        
        FBSDKApplicationDelegate.sharedInstance().application(application, didFinishLaunchingWithOptions: launchOptions)

        //notifications
        
        
        let userDefaults = NSUserDefaults.standardUserDefaults()
        if userDefaults.objectForKey("UserName") != nil && userDefaults.objectForKey("Password") != nil
        {
            self.userLogin(userDefaults.objectForKey("UserName") as! String, pass:userDefaults.objectForKey("Password") as! String,launchOptions:launchOpt)
        }else if FBSDKAccessToken.currentAccessToken() != nil {
            self.fbLogin()
        }else{
            if launchOpt != nil{
                var dic = (launchOpt as! NSDictionary).objectForKey(UIApplicationLaunchOptionsRemoteNotificationKey) as! NSDictionary
                handlePush(dic)
            }
        }
        return true
    }
    
    
    func fbLogin(){
            var token = FBSDKAccessToken.currentAccessToken().tokenString
            println(token)
            
            WSProgressHUD.show()
            
            let delegate = UIApplication.sharedApplication().delegate as! AppDelegate
            var dToken = delegate.deviceTokenString//"e678643509dc3ff9a6c7e1966a7fe17fbf40d4027eca8c792b192ffb6d277adf"
            
            RequestManager.sendToken(token,deviceToken: dToken!){ (response, error) in
                if error != nil {
                    WSProgressHUD.dismiss()
                    //                    AppManager.sharedInstance.showMessage(self, title: "Error", msg: error?.localizedDescription, buttonTitle: "Cancel")
                } else {
                    
                    //                    self.showCollaborationCorner();
                    self.showHome(self.launchOpt)
                }
                
                return nil
            }

    }
    func application(application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: NSError) {
//        AppManager.sharedInstance.showMessage(self.window?.rootViewController, title: "Error!", msg: error.localizedDescription, buttonTitle:"Cancel")
    }
    
    func application( application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: NSData ) {
        var characterSet: NSCharacterSet = NSCharacterSet( charactersInString: "<>" )
        
        deviceTokenString = ( deviceToken.description as NSString )
            .stringByTrimmingCharactersInSet( characterSet )
            .stringByReplacingOccurrencesOfString( " ", withString: "" ) as String
        RequestManager.trySendingDeviceTokenIfNotSent()

        //
        //
        //        UIAlertView(
        //            title: "Device Token",
        //            message: deviceTokenString,
        //            delegate: nil,
        //            cancelButtonTitle: "OK"
        //            ).show()
        
    }

    func userLogin(userName: String, pass: String,launchOptions:[NSObject:AnyObject]?)
    {
        WSProgressHUD.show()
        var vc = self.window?.rootViewController as? LandingViewController
        
        if vc != nil{
            vc?.signupBtn.enabled=false
            vc?.loginBtn.enabled=false
        }
        let delegate = UIApplication.sharedApplication().delegate as! AppDelegate
        var dToken = delegate.deviceTokenString    //"e678643509dc3ff9a6c7e1966a7fe17fbf40d4027eca8c792b192ffb6d277adf"; //
        
        RequestManager.login(userName, password: pass, deviceToken: dToken!) { (response, error) in
            if error != nil {
                WSProgressHUD.dismiss()
                AppManager.sharedInstance.showMessage(self.window?.rootViewController, title: "Error!", msg: error!.localizedDescription, buttonTitle:"Cancel")
            } else {
                //                self.showCollaborationCorner()
                
                let userDefaults = NSUserDefaults.standardUserDefaults()
                
                userDefaults.removeObjectForKey("UserName")
                userDefaults.removeObjectForKey("Password")
                userDefaults.setBool(true, forKey: Constants.kLoggedIn)
                userDefaults.setObject(userName, forKey: "UserName")
                userDefaults.setObject(pass, forKey: "Password")
                userDefaults.synchronize()
                
                
                self.showHome(launchOptions)
                
                
            }
            
            return nil
        }
    }
    func showHome(launchOptions:[NSObject:AnyObject]?){
        
        NSOperationQueue.mainQueue().addOperationWithBlock { () -> Void in
            
            
            let mainStoryboard = UIStoryboard(name: "CollaborationCorner2", bundle: nil)
            let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
            
            var centerViewController = mainStoryboard.instantiateViewControllerWithIdentifier("MainViewController") as! MainViewController
            
            var leftViewController = mainStoryboard.instantiateViewControllerWithIdentifier("SideBarViewController") as! SideBarViewController
            
            var leftSideNav = UINavigationController(rootViewController: leftViewController)
            var centerNav = UINavigationController(rootViewController: centerViewController)
            
            appDelegate.centerContainer = MMDrawerController(centerViewController: centerNav, leftDrawerViewController: leftSideNav,rightDrawerViewController:nil)
            appDelegate.centerContainer!.openDrawerGestureModeMask = MMOpenDrawerGestureMode.PanningCenterView;
            appDelegate.centerContainer!.closeDrawerGestureModeMask = MMCloseDrawerGestureMode.PanningCenterView;

            appDelegate.window?.rootViewController = appDelegate.centerContainer
            if launchOptions != nil{
                var dic = (launchOptions as! NSDictionary).objectForKey(UIApplicationLaunchOptionsRemoteNotificationKey) as! NSDictionary
                self.handlePush(dic)
            }
        }
        
    }
    func application(application: UIApplication, didReceiveRemoteNotification userInfo: [NSObject : AnyObject]) {
        var dic = userInfo as NSDictionary
        println(dic)
//
        handlePush(dic)
//        UIAlertView(
//            title: "push recieved",
//            message: dic.description,
//            delegate: nil,
//            cancelButtonTitle: "OK"
//            ).show()
        
    }
    func handlePush(dic:NSDictionary!){
        let userDefaults = NSUserDefaults.standardUserDefaults()
        if (userDefaults.objectForKey("UserName") != nil && userDefaults.objectForKey("Password") != nil) || FBSDKAccessToken.currentAccessToken() != nil
        {
            var message:String! = ""
            if let aps = dic["aps"] as? NSDictionary {
                if let alert = aps["alert"] as? String {
                    message = alert
                }
            }
            if let msg = dic["message"] as? String{
                message = msg
            }
            if let eventId = dic["id"] as? Int{
                tellUserThatEventReceived(eventId, message: message)
            }
        }
    }
    func tellUserThatEventReceived(eventId:Int!,message:String!){
        let alert = UIAlertController(title: "New event", message: message, preferredStyle: UIAlertControllerStyle.Alert)
        alert.addAction(UIAlertAction(title: "View", style: .Default) { _ in
            var vc = UIStoryboard(name: "CollaborationCorner2", bundle: nil).instantiateViewControllerWithIdentifier("EventDetailViewController") as? EventDetailViewController
            vc?.eventId = eventId
            self.window?.rootViewController?.presentViewController(vc!, animated: true, completion: nil)

            
            })
        alert.addAction(UIAlertAction(title: "Cancel", style: .Default) { _ in
            alert.dismissViewControllerAnimated(true, completion: { () -> Void in
                
            })
            })
        

        self.window?.rootViewController!.presentViewController(alert, animated: true){}

    }
    
    func applicationWillResignActive(application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(application: UIApplication) {
        // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
    }

    func applicationWillTerminate(application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }
    
    func printRealmPath() {
        let paths:NSArray = NSSearchPathForDirectoriesInDomains(NSSearchPathDirectory.DocumentationDirectory, NSSearchPathDomainMask.UserDomainMask, false)
        let documentsDirectory:String = paths.firstObject as! String
        println("\(RLMRealmConfiguration.defaultConfiguration().path)")
    }
    
    func performMigration() {
        let configuration = RLMRealmConfiguration.defaultConfiguration()
        
        configuration.schemaVersion = 2
        configuration.migrationBlock = { migration, oldSchemaVersion in
            
        }
        
        RLMRealmConfiguration.setDefaultConfiguration(configuration)
    }
    
    func initializeCurrentUser() {
        let userDefaults = NSUserDefaults.standardUserDefaults()
        
        if let userID = userDefaults.integerForKey("currentUserID") as Int? {
            currentUserID = userID
        }
        
        if let token = userDefaults.objectForKey("authenticationToken") as! String? {
            authToken = token
        }
        
        if let user = User(forPrimaryKey: currentUserID) {
            Heap.identify([
                "email" : user.email,
                "name" : user.name()
            ])
            
            ATConnect.sharedConnection().personEmailAddress = user.email
            ATConnect.sharedConnection().personName = user.name()
        }
    }
    
    func configureAppearance() {
        
        
        
        let configuration = KVNProgressConfiguration.new()
        
        configuration.statusColor = UIColor.whiteColor()
        configuration.circleStrokeForegroundColor = UIColor(red: 0.173, green:0.263, blue:0.856, alpha:1.0)
        configuration.circleStrokeBackgroundColor = UIColor(white: 1.0, alpha: 0.3)
        configuration.circleFillBackgroundColor = UIColor(white: 1.0, alpha:1)
        configuration.backgroundFillColor = UIColor(white: 1.0, alpha:0.7)
        configuration.backgroundTintColor = UIColor(red: 0.173, green:0.263, blue:0.856, alpha:0.7)
        configuration.backgroundType = KVNProgressBackgroundType.Solid
        configuration.fullScreen = true;
        
        KVNProgress.setConfiguration(configuration)
    }
    
    func showMainView() {
//        if authToken != "" {
//            showCollaborationCorner()
//        } else {
            showSignUpView()
//        }
    }
    
    func showSignUpView(){
        let storyboard = UIStoryboard(name:"SignUp", bundle:nil)
        let viewController = storyboard.instantiateInitialViewController() as! LandingViewController
        

        self.window!.rootViewController = viewController
    }
    
    func showCollaborationCorner(){
        let storyboard = UIStoryboard(name: "CollaborationCorner", bundle:nil)
        let viewController = storyboard.instantiateInitialViewController() as! UIViewController
        
        self.window!.rootViewController = viewController
    }
    
//    func refreshCurrentUsersLocation() {
//        if authToken != "" {
//            let locationManager = INTULocationManager.sharedInstance()
//            locationManager.requestLocationWithDesiredAccuracy(INTULocationAccuracy.City, timeout: 10.0, block: { (currentLocation, achievedAccuracy, status) -> Void in
//                if (status == INTULocationStatus.Success) {
//                    RequestManager.updateCurrentUserLocation(Double(currentLocation.coordinate.latitude), longitude: Double(currentLocation.coordinate.longitude))
//                }
//            })
//        }
//    }
}

