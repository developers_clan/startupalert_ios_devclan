//
//  CoworkingSpacesViewController.swift
//  ufp
//
//  Created by Ron De Benedetti on 7/13/15.
//  Copyright (c) 2015 TwinEngineLabs. All rights reserved.
//

class CoworkingSpacesViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {

    var spaces = CoworkingSpace.allObjects()
    
    @IBOutlet var tableView: UITableView?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        RequestManager.getCoworkingSpaces() { (response, error) in
            dispatch_async(dispatch_get_main_queue()){
                self.tableView?.reloadData()
            }
        }
        
        var nib = UINib(nibName: "CoworkingSpacesCell", bundle: nil)
        tableView?.registerNib(nib, forCellReuseIdentifier: "CoworkingSpacesCellIdentifier")
        
        tableView?.tableFooterView = UIView(frame: CGRect.zeroRect)
        
        tableView?.rowHeight = 100
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationItem.title = "Coworking Spaces"
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return Int(spaces.count)
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell{
        let cell = tableView.dequeueReusableCellWithIdentifier("CoworkingSpacesCellIdentifier") as! CoworkingSpacesCell
        let space = spaces[UInt(indexPath.row)] as! CoworkingSpace
        
        cell.myTitle.text = space.title
        cell.myLocation.text = space.location
        cell.myImage.sd_setImageWithURL(NSURL(string: space.picture_url))
        
        return cell
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath){
        let selectedSpace = UInt(indexPath.row)
        let space = spaces[selectedSpace] as! CoworkingSpace
        
        if space.invalidated { return }
        
        RequestManager.showCoworkingSpace(space) { (response, error) in
            dispatch_async(dispatch_get_main_queue()){
                self.showCoworkingSpace(space)
            }
        }
    }
    
    func showCoworkingSpace(coworking_space: CoworkingSpace){
        let vc = self.storyboard?.instantiateViewControllerWithIdentifier("ResourceShow") as! ResourceShowViewController
        vc.setupResourceShow(coworking_space.picture_url, content: coworking_space.content, phone: coworking_space.phone, url: coworking_space.url, email: coworking_space.email, location: coworking_space.location, name: coworking_space.title)
        vc.showContactInfo = true
        self.navigationController?.pushViewController(vc, animated: true)
    }
}

