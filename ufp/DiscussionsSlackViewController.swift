//
//  MessageViewController.swift
//  Messenger-Swift
//
//  Created by Ignacio Romero Zurbuchen on 10/16/14.
//  Modified by Ron De Benedetti on 8/7/15
//  Copyright (c) 2014 Slack Technologies, Inc. All rights reserved.
//

import SlackTextViewController

class DiscussionsSlackViewController: SLKTextViewController {
    
    var discussions = Discussion.allObjects()
    
    override class func tableViewStyleForCoder(decoder: NSCoder) -> UITableViewStyle {
        return UITableViewStyle.Plain;
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        RequestManager.getDiscussions(){ (response, error) in
            dispatch_async(dispatch_get_main_queue()){
                self.tableView.reloadData()
            }
        }
        
        self.bounces = true
        self.shakeToClearEnabled = true
        self.keyboardPanningEnabled = true
        self.inverted = false
        
        self.textView.placeholder = "Start a Discussion"
        self.textView.placeholderColor = UIColor.lightGrayColor()
        
        self.rightButton.setTitle("Send", forState: UIControlState.Normal)
        
        self.textInputbar.autoHideRightButton = true
        self.textInputbar.counterStyle = SLKCounterStyle.Split
        
        self.typingIndicatorView.canResignByTouch = true
        
        self.tableView.tableFooterView = UIView(frame: CGRect.zeroRect)
        
        var nib = UINib(nibName: "DiscussionCell", bundle: nil)
        self.tableView.registerNib(nib, forCellReuseIdentifier: "DiscussionCellIdentifier")
        
    }
    
    
    override func didPressRightButton(sender: AnyObject!) {
        
        self.textView.refreshFirstResponder()
        
        let message = self.textView.text.copy() as! String
        
        
        RequestManager.postDiscussion(message){ (response, error) in
            dispatch_async(dispatch_get_main_queue()){
                self.tableView.reloadData()
                self.tableViewScrollToTop(true)
                super.didPressRightButton(sender)
            }
        }
        
    }
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return Int(self.discussions.count)
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell{
        
        let cell = tableView.dequeueReusableCellWithIdentifier("DiscussionCellIdentifier") as! DiscussionCell
        let discussion = discussions[UInt(Int(discussions.count) - Int(indexPath.row) - 1)] as! Discussion
        
        cell.myDiscussion.text = discussion.content
        cell.myName.text = discussion.user_full_name
        cell.myDate.text = discussion.posted_at
        cell.myImage.sd_setImageWithURL(NSURL(string: discussion.user_picture_url))
        
        return cell
        
    }
    
    
    override func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat{
        
        let maxHeight : CGFloat = 10000
        let maxWidth = UIScreen.mainScreen().bounds.size.width - 20
        let discussion = discussions[UInt(Int(discussions.count) - Int(indexPath.row) - 1)] as! Discussion
        let text = discussion.content
        let rect = text.boundingRectWithSize(CGSize(width: maxWidth, height: maxHeight),
            options: NSStringDrawingOptions.UsesLineFragmentOrigin,
            attributes: [NSFontAttributeName: UIFont.systemFontOfSize(15.0)],
            context: nil)
        var discussionHeight = rect.size.height
        return 70 + discussionHeight
        
    }
    
    func tableViewScrollToTop(animated: Bool) {
        
        let delay = 0.1 * Double(NSEC_PER_SEC)
        let time = dispatch_time(DISPATCH_TIME_NOW, Int64(delay))
        
        dispatch_after(time, dispatch_get_main_queue(), {
            
            let numberOfSections = self.tableView.numberOfSections()
            let numberOfRows = self.tableView.numberOfRowsInSection(numberOfSections-1)
            
            if numberOfRows > 0 {
                let indexPath = NSIndexPath(forRow: 0, inSection: (numberOfSections-1))
                self.tableView.scrollToRowAtIndexPath(indexPath, atScrollPosition: UITableViewScrollPosition.Bottom, animated: animated)
            }
            
        })
    }
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath){
        let selectedDiscussion = UInt(Int(discussions.count) - Int(indexPath.row) - 1)
        let discussion = discussions[selectedDiscussion] as! Discussion
        
        if discussion.invalidated { return }
        
        RequestManager.showDiscussion(discussion) { (response, error) in
            dispatch_async(dispatch_get_main_queue()){
                self.showDiscussion(discussion)
            }
        }
    }
    
    func showDiscussion(discussion: Discussion){
        let vc = self.storyboard?.instantiateViewControllerWithIdentifier("DiscussionShow") as! DiscussionShowTableViewController
        vc.discussion = discussion
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}
