 //
//  SkillsetSeekViewController.swift
//  ufp
//
//  Created by Ron De Benedetti on 6/29/15.
//  Copyright (c) 2015 TwinEngineLabs. All rights reserved.
//

import UIKit

class SkillsetsHaveViewController: UIViewController,UICollectionViewDataSource,UICollectionViewDelegate {
    
    
    var skillsets = Skill.allObjects().sortedResultsUsingProperty("name", ascending: true)
    
    var mode: String!
    
    var titleString: String!
    var profileSetupString: String!
    var skillsetsString: String!
    var buttonTitle: String!
    
//    @IBOutlet var tableView: UITableView?
    @IBOutlet weak var collectionVu: UICollectionView!
    @IBOutlet var lblTitle: UILabel!
    @IBOutlet var lblProfileSetup: UILabel!
    @IBOutlet var lblSkillsets: UILabel!
    @IBOutlet var btnNextStep: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if mode == "signup" {
            RequestManager.getSkills() { (response, error) in
                dispatch_async(dispatch_get_main_queue()){
                    self.collectionVu?.reloadData()
                }
            }
        }
        
        if (mode == "settings"){
            self.lblTitle.text = titleString
            self.lblProfileSetup.text = profileSetupString
            self.lblSkillsets.text = skillsetsString
            self.btnNextStep.setTitle(buttonTitle, forState: .Normal)
        }
        
        self.collectionVu.dataSource = self;
        self.collectionVu.delegate = self;
        self.collectionVu?.allowsMultipleSelection = true;
        
        
//        collectionVu.registerClass(SkillsetCollectionCell.self, forCellWithReuseIdentifier: "SkillsetColIdentifier")
        
//        self.collectionView registerNib:[UINib nibWithNibName:@"MyCell" bundle:nil] forCellWithReuseIdentifier:@"CELL";
        
//        var nib = UINib(nibName: "SkillsetCollectionCell", bundle: nil)
//        collectionVu?.registerNib(nib, forCellWithReuseIdentifier: "SkillsetColIdentifier")
//
//        collectionVu?.estimatedRowHeight
//        collectionVu?.rowHeight = UITableViewAutomaticDimension
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
        if mode == "settings" {
            for indexPath in preselectedIndexPaths() {
                self.collectionVu?.selectItemAtIndexPath(indexPath as? NSIndexPath, animated: false, scrollPosition: UICollectionViewScrollPosition.None)
//                self.tableView?.selectRowAtIndexPath(indexPath as? NSIndexPath, animated: false, scrollPosition: .None)
            }
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func preselectedIndexPaths() -> NSArray {
        let user = User(forPrimaryKey: currentUserID)
        let preselectedItems = NSMutableSet.new()
        
        var row = 0
        for skillset in skillsets.toArray(Skill.self) {
            if contains(user!.skills.toArray(), skillset) {
                preselectedItems.addObject(NSIndexPath(forRow: row, inSection: 0))
            }
            row++
        }
        
        return preselectedItems.allObjects
    }
    
//    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
//        return Int(skillsets.count)
//    }
//    
//    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell{
//        let cell = tableView.dequeueReusableCellWithIdentifier("SkillsetCellIdentifier") as! SkillsetCell
//        let skill = skillsets[UInt(indexPath.row)] as! Skill
//        cell.myLabel.text = skill.name
//        return cell
//    }

    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        let cell = collectionVu?.dequeueReusableCellWithReuseIdentifier("SkillsetColIdentifier", forIndexPath: indexPath) as! SkillsetCollectionCell
        let skill = skillsets[UInt(indexPath.row)] as! Skill
        cell.label.text = skill.name
        
        if cell.selectedBackgroundView==nil {
            var view=UIView()
            view.backgroundColor=UIColor(red: 231/255.0, green: 154/255.0, blue: 65/255.0, alpha: 1)
            cell.selectedBackgroundView=view
        }
        
        if preselectedIndexPaths().containsObject(indexPath){
            cell.label.textColor=UIColor.whiteColor()
        }else{
            cell.label.textColor=UIColor.darkGrayColor()
        }
        
        return cell
    }
    func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath) {
        let cell = collectionVu?.cellForItemAtIndexPath(indexPath) as! SkillsetCollectionCell
        cell.label.textColor=UIColor.whiteColor()
    }
    func collectionView(collectionView: UICollectionView, didDeselectItemAtIndexPath indexPath: NSIndexPath) {
        let cell = collectionVu?.cellForItemAtIndexPath(indexPath) as! SkillsetCollectionCell
        cell.label.textColor=UIColor.darkGrayColor()
    }
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return Int(skillsets.count)
    }
    
    @IBAction func didTapNextStep(){
//        var selectedIndexPaths = self.tableView?.indexPathsForSelectedRows()
        var selectedIndexPaths = self.collectionVu?.indexPathsForSelectedItems()
        var skills = ""
        for (var i = 0; i < selectedIndexPaths?.count; i++) {
            let selected_skill = UInt(selectedIndexPaths![i].row)
            let skill = skillsets[selected_skill] as! Skill
            skills += "\(skill.id),"
        }
        RequestManager.step2(skills){ (response, error) in
            dispatch_async(dispatch_get_main_queue()){
                self.showNextStep()
            }
        }
    }
    
    func showNextStep(){
        if (mode == "signup"){
            let vc = self.storyboard?.instantiateViewControllerWithIdentifier("Interests") as! InterestsViewController
            vc.mode = "signup"
            self.navigationController?.pushViewController(vc, animated: true)
        } else {
            self.navigationController?.popViewControllerAnimated(true)
        }
    }
}
