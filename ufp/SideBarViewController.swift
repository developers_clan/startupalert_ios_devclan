//
//  SideBarViewController.swift
//  Unity for People
//
//  Created by Devclan on 02/12/2015.
//  Copyright (c) 2015 TwinEngineLabs. All rights reserved.
//

import UIKit
import apptentive_ios
import WSProgressHUD

class SideBarViewController: UIViewController, UITableViewDelegate,UITableViewDataSource {

//    var items: [String] = ["My Profile", "My Event Interests", "Notifications Settings", "Change City", "Give Us feedback","Logout"]
//    
//     var imageName: [String] = ["profile_pic", "event_icon", "notification_setting_icon", "city_icon", "feedback_icon","logout"]

    var items: [String] = ["My Profile", "My Event Interests", "Change City", "Give Us feedback","Logout"]
    
    var imageName: [String] = ["profile_pic", "event_icon", "city_icon", "feedback_icon","logout"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        navigationController?.navigationBar.hidden = true
        
        
        
    }
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
//        UIApplication.sharedApplication().setStatusBarHidden(true, withAnimation: UIStatusBarAnimation.Slide)
    }
    override func viewWillDisappear(animated: Bool) {
        super.viewWillDisappear(animated)
//        UIApplication.sharedApplication().setStatusBarHidden(false, withAnimation: UIStatusBarAnimation.Slide)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.items.count;
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        var cell:SideMenuCell = tableView.dequeueReusableCellWithIdentifier("SideMenuCell") as! SideMenuCell
        
        var bgColorView = UIView()
        bgColorView.backgroundColor = UIColor(red: 45/255, green: 45/255, blue: 45/255, alpha: 1.0) //UIColor.redColor()
        cell.selectedBackgroundView = bgColorView
        
        cell.lblText.text = self.items[indexPath.row]
        cell.img.image = UIImage(named: self.imageName[indexPath.row])
        
        return cell
        
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        if indexPath.row == 0
        {
            let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
            appDelegate.centerContainer!.toggleDrawerSide(MMDrawerSide.Left, animated: true, completion: nil)
            
            let storyboard = UIStoryboard(name: "SignUp", bundle: nil)
            var vc = storyboard.instantiateViewControllerWithIdentifier("Registration") as! RegistrationViewController
            vc.mode = "settings"
            vc.registerButtonTitle = "UPDATE"
            self.presentViewController(vc, animated: true, completion: nil)
            
            return;
        }
        if indexPath.row == 1 //Interests
        {
            ATConnect.sharedConnection().engage("my_event_interests_from_menu_pressed", fromViewController: self)
            
            var user = User(forPrimaryKey: currentUserID)!
            
            let storyboard = UIStoryboard(name: "SignUp", bundle: nil)
            var vc = storyboard.instantiateViewControllerWithIdentifier("Interests") as! InterestsViewController
            vc.mode = "settings"
            vc.titleString = "Unity for People"
//            vc.profileSetupString = "SETTINGS"
            //            vc.interestsString = "Edit Your Event Interests"
            vc.buttonTitle = "SAVE"
            WSProgressHUD.show()
            RequestManager.getInterests(completion: { (response, error) -> Void? in
                dispatch_async(dispatch_get_main_queue()){
                    if error == nil{
                        return RequestManager.getUser(user){ (response, error) in
                            dispatch_async(dispatch_get_main_queue()){
                                WSProgressHUD.dismiss()
                                if error == nil{
                                    let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
                                    appDelegate.centerContainer!.toggleDrawerSide(MMDrawerSide.Left, animated: true, completion: nil)
                                    self.presentViewController(vc, animated: true, completion: nil)
                                }
                            }
                        }
                    }else{
                        WSProgressHUD.dismiss()
                    }
                }

            })
            
//            RequestManager.getInterests() { (response, error) in
//                RequestManager.getUser(user){ (response, error) in
//                    dispatch_async(dispatch_get_main_queue()){
//                        p.dismiss()
//                        let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
//                        appDelegate.centerContainer!.toggleDrawerSide(MMDrawerSide.Left, animated: true, completion: nil)
//                        
//                        self.presentViewController(vc, animated: true, completion: nil)
//                    }
//                }
//            }
        }
        if indexPath.row == 2
        {
            ATConnect.sharedConnection().engage("change_city_from_menu_pressed", fromViewController: self)
            let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
            appDelegate.centerContainer!.toggleDrawerSide(MMDrawerSide.Left, animated: true, completion: nil)
            
            let storyboard = UIStoryboard(name: "CollaborationCorner2", bundle: nil)
            var vc = storyboard.instantiateViewControllerWithIdentifier("ChangeCityViewController") as! ChangeCityViewController
//            vc.mode = "settings"
//            vc.registerButtonTitle = "UPDATE"
            self.presentViewController(vc, animated: true, completion: nil)
            
            return;
        }
        if indexPath.row == 3
        {
            let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
            appDelegate.centerContainer!.toggleDrawerSide(MMDrawerSide.Left, animated: true, completion: nil)
            
            ATConnect.sharedConnection().presentMessageCenterFromViewController(self)
            
            return;
        }
        if indexPath.row == 4
        {
            
            let userDefaults = NSUserDefaults.standardUserDefaults()
            userDefaults.removeObjectForKey("currentUserID")
            userDefaults.removeObjectForKey("authenticationToken")
            userDefaults.removeObjectForKey("UserName")
            userDefaults.removeObjectForKey("Password")
            userDefaults.removeObjectForKey("user")
            userDefaults.synchronize()
            
//            appDelegate.centerContainer!.navigationController?.popViewControllerAnimated(true)
            
            let storyboard = UIStoryboard(name: "SignUp", bundle: nil)
            var vc = storyboard.instantiateViewControllerWithIdentifier("LandingViewController") as! LandingViewController
            
             let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
              appDelegate.window?.rootViewController = vc
            appDelegate.window?.makeKeyAndVisible()
            
//            self.dismissViewControllerAnimated(true, completion: nil)
            
            return;
        }
    }


    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
