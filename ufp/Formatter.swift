//
//  Formatter.swift
//  ufp
//
//  Created by Ron De Benedetti on 6/17/15.
//  Copyright (c) 2015 TwinEngineLabs. All rights reserved.
//

import Foundation

let dateKeyStrings = ["created_at", "updated_at", "starts_at", "ends_at"]

class Formatter {
    
    class func dateFromString(string: String) -> NSDate {
        return dateFromString(string, withFormat: "yyyy-MM-dd'T'HH:mm:ss.SSSZZZZZ")
    }
    
    class func dateFromString(string: String, withFormat format: String) -> NSDate {
        let dateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = format
        
        return dateFormatter.dateFromString(string) ?? NSDate()
    }
    
    class func stringFromDate(date: NSDate, withFormat format: String) -> String {
        let dateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = format
        
        return dateFormatter.stringFromDate(date)
    }
    
    class func remapJsonKeys(jsonDict: NSDictionary, remapDict: NSDictionary) -> NSDictionary {
        var newDict = NSMutableDictionary()
        for (key, value) in jsonDict {
            if (remapDict[key as! String] == nil) {
                continue
            }
            
            let newKey = remapDict[key as! String] as! String
            
            if (contains(dateKeyStrings, key as! String)) {
                newDict[newKey] = Formatter.dateFromString(value as! String)
            }
            else {
                newDict[newKey] = value
            }
        }
        return newDict as NSDictionary
    }
    
    class func combineDictionaries (dictionary: NSDictionary, otherDictionary: NSDictionary) -> NSDictionary {
        var newDict = NSMutableDictionary(dictionary: dictionary)
        for (key, value) in otherDictionary {
            newDict.setValue(value, forKey: key as! String)
        }
        return newDict
    }
}