//
//  Skill.swift
//  ufp
//
//  Created by Ron De Benedetti on 6/30/15.
//  Copyright (c) 2015 TwinEngineLabs. All rights reserved.
//

class Skill : RLMObject {
    dynamic var id = 0
    dynamic var name = ""
    dynamic var users = RLMArray(objectClassName: User.className())
    
    override class func primaryKey() -> String? {
        return "id"
    }
    
    class var remap : Dictionary<String, String> {
        get {
            return [
                "id" : "id",
                "name" : "name"
            ]
        }
    }
    
    class func createOrUpdateFromArray(array : NSArray) -> Array<Skill> {
        var skills = Array<Skill>()
        for dict in array {
            skills.append(self.createOrUpdateFromDictionary(dict as! NSDictionary) as Skill)
        }
        return skills
    }
    
    class func createOrUpdateFromDictionary(dict : NSDictionary) -> Skill {
        var skill = self.createOrUpdateInDefaultRealmWithValue(Formatter.remapJsonKeys(dict, remapDict: remap))
        return skill as Skill
    }
}
