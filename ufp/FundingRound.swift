//
//  FundingRound.swift
//  Unity for People
//
//  Created by Ron De Benedetti on 7/25/15.
//  Copyright (c) 2015 TwinEngineLabs. All rights reserved.
//

class FundingRound : RLMObject {
    dynamic var id = 0
    dynamic var name = ""
    
    override class func primaryKey() -> String? {
        return "id"
    }
    
    class var remap : Dictionary<String, String> {
        get {
            return [
                "id" : "id",
                "name" : "name"
            ]
        }
    }
    
    class func createOrUpdateFromArray(array : NSArray) -> Array<FundingRound> {
        var fundingRounds = Array<FundingRound>()
        for dict in array {
            fundingRounds.append(self.createOrUpdateFromDictionary(dict as! NSDictionary) as FundingRound)
        }
        return fundingRounds
    }
    
    class func createOrUpdateFromDictionary(dict : NSDictionary) -> FundingRound {
        var fundingRound = self.createOrUpdateInDefaultRealmWithValue(Formatter.remapJsonKeys(dict, remapDict: remap))
        return fundingRound as FundingRound
    }
}
