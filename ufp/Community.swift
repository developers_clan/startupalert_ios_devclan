//
//  Community.swift
//  ufp
//
//  Created by Ron De Benedetti on 7/13/15.
//  Copyright (c) 2015 TwinEngineLabs. All rights reserved.
//

class Community : RLMObject {
    dynamic var id = 0
    dynamic var title = ""
    dynamic var location = ""
    dynamic var content = ""
    dynamic var phone = ""
    dynamic var email = ""
    dynamic var url = ""
    dynamic var picture_url = ""
    
    override class func primaryKey() -> String? {
        return "id"
    }
    
    class var remap : Dictionary<String, String> {
        get {
            return [
                "id" : "id",
                "title": "title",
                "location" : "location",
                "content" : "content",
                "phone" : "phone",
                "email" : "email",
                "url" : "url",
                "picture_url" : "picture_url"
            ]
        }
    }
    
    class func createOrUpdateFromArray(array : NSArray) -> Array<Community> {
        var communities = Array<Community>()
        for dict in array {
            communities.append(self.createOrUpdateFromDictionary(dict as! NSDictionary) as Community)
        }
        return communities
    }
    
    class func createOrUpdateFromDictionary(dict : NSDictionary) -> Community {
        var community = self.createOrUpdateInDefaultRealmWithValue(Formatter.remapJsonKeys(dict, remapDict: remap))
        return community as Community
    }
}
