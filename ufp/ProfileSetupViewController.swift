//
//  ProfileSetupViewController.swift
//  ufp
//
//  Created by Ron De Benedetti on 7/1/15.
//  Copyright (c) 2015 TwinEngineLabs. All rights reserved.
//

import UIKit

import GMImagePicker
import KVNProgress

class ProfileSetupViewController: UIViewController, GMImagePickerControllerDelegate {
    
    var mode: String!
    
    var titleString: String!
    var profileSetupString: String!
    var buttonTitle: String!
    
    @IBOutlet var btnUploadProfileImage: UIButton!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblProfileSetup: UILabel!
    @IBOutlet weak var btnNextStep: UIButton!
    
    var badgesViewController: BadgesViewController!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if mode == "settings" {
            self.lblTitle.text = titleString
            self.lblProfileSetup.text = profileSetupString
            self.btnNextStep.setTitle(buttonTitle, forState: .Normal)
            
            let currentUser = User(forPrimaryKey: currentUserID)
            
            self.btnUploadProfileImage.sd_setImageWithURL(NSURL(string: currentUser!.avatarURL), forState: .Normal)
            currentUser?.badges.toArray(Badge.self).map { self.badgesViewController.attemptToAddBadge($0.name) }
        }
        
        self.btnUploadProfileImage.imageView!.clipsToBounds = true
        self.btnUploadProfileImage.imageView!.contentMode = .ScaleAspectFill
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
        if(mode == "signup"){
            self.navigationController?.navigationBar.barTintColor = UIColor(red: 5/255, green: 60/255, blue: 115/255, alpha: 1.0)
            self.navigationController?.navigationBar.tintColor = UIColor.whiteColor()
            self.navigationController?.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName : UIColor.whiteColor()]
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func didTapAvatar() {
        PHPhotoLibrary.requestAuthorization { (status) -> Void in
            dispatch_async(dispatch_get_main_queue(), { () -> Void in
                if status == PHAuthorizationStatus.Authorized {
                    let picker = GMImagePickerController()
                    picker.delegate = self
                    picker.title = "AVATAR"
                    self.presentViewController(picker, animated: true, completion: nil)
                } else {
                    UIAlertView(
                        title: "No Photo Access",
                        message: "This app does not have access to your photos. You can enable access in your Privacy Settings.",
                        delegate: nil,
                        cancelButtonTitle: "OK"
                        ).show()
                }
            })
        }
        
    }
    
    // MARK: -
    // MARK: GMImagePickerControllerDelegate
    
    func assetsPickerController(picker: GMImagePickerController!, didSelectAsset asset: PHAsset!) {
        PHImageManager.defaultManager().requestImageForAsset(asset as PHAsset, targetSize: CGSize(width: 400, height: 400), contentMode: .AspectFill, options: nil, resultHandler: { (image, info) -> Void in
            if image == nil {
                UIAlertView(
                    title: "Error",
                    message: "An unknown error occurred while trying to load that image. Please try again with a different one.",
                    delegate: nil,
                    cancelButtonTitle: "OK"
                    ).show()
                
                return
            }
            
            if info["PHImageResultIsDegradedKey"] as! NSNumber == 1 {
                return
            }
            
            KVNProgress.showWithStatus(nil, onView: self.btnUploadProfileImage)
            
            RequestManager.updateCurrentUserImage(image, completion: { (response, error) in
                if error == nil {
                    var user = User(forPrimaryKey: currentUserID)!
                    RequestManager.getUser(user){ (response, error) in
                        dispatch_async(dispatch_get_main_queue()){
                            var user = User(forPrimaryKey: currentUserID)!
                            self.btnUploadProfileImage.sd_setImageWithURL(NSURL(string: user.avatarURL), forState: .Normal, completed: { (image, error, cacheType, url) -> Void in
                                KVNProgress.dismiss()
                            })
                        }
                    }
                } else  {
                    KVNProgress.dismiss()
                    // show error
                }
                
                return nil
            })
        })
        
        dismissViewControllerAnimated(true, completion: nil)
    }
    
    func assetsPickerController(picker: GMImagePickerController!, didFinishPickingAssets assets: [AnyObject]!) {
        // No op
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if let vc = segue.destinationViewController as? BadgesViewController
            where segue.identifier == "BadgesSegue" {
                self.badgesViewController = vc
        }
    }
    
    @IBAction func didTapNextStep(){
        
        var badges = badgesViewController.getBadges()
        
        RequestManager.step1(badges) { (response, error) in
            dispatch_async(dispatch_get_main_queue()){
                self.showNextStep()
            }
        }
    }
    
    func showNextStep(){
        if (mode == "signup"){
            let vc = self.storyboard?.instantiateViewControllerWithIdentifier("SkillsetsHave") as! SkillsetsHaveViewController
            vc.mode = "signup"
            self.navigationController?.pushViewController(vc, animated: true)
        } else {
            self.navigationController?.popViewControllerAnimated(true)
        }
    }
}
