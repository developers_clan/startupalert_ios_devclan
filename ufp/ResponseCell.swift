//
//  ResponseCell.swift
//  Unity for People
//
//  Created by Ron De Benedetti on 8/9/15.
//  Copyright (c) 2015 TwinEngineLabs. All rights reserved.
//

class ResponseCell: UICollectionViewCell {
    
    @IBOutlet var myDiscussion: UITextView!
    @IBOutlet var myImage: UIImageView!
    @IBOutlet var myName: UILabel!
    @IBOutlet var myDate: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
}
