//
//  DescribeCurrentProfileViewController.swift
//  ufp
//
//  Created by Ron De Benedetti on 6/30/15.
//  Copyright (c) 2015 TwinEngineLabs. All rights reserved.
//

import UIKit

class DescribeCurrentProjectViewController: UIViewController, UIPickerViewDelegate {
    
    var mode: String!
    
    var verticals = Vertical.allObjects().sortedResultsUsingProperty("name", ascending: true)
    var rounds = FundingRound.allObjects()

    @IBOutlet var textView: UITextView?
    @IBOutlet var pickerView: UIPickerView?
    
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblProfileSetup: UILabel!
    @IBOutlet weak var lblCurrentProject: UILabel!
    @IBOutlet weak var btnCompleteProfile: UIButton!
    @IBOutlet weak var textViewTopConstraint: NSLayoutConstraint!
    
    var titleString: String!
    var profileSetupString: String!
    var currentProjectString: String!
    var currentProjectText: String!
    var buttonTitle: String!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if mode == "signup" {
            RequestManager.getVerticals() { (response, error) in
                dispatch_async(dispatch_get_main_queue()){
                    self.pickerView?.reloadAllComponents()
                }
            }
            
            RequestManager.getFundingRounds(){ (response, error) in
                dispatch_async(dispatch_get_main_queue()){
                    self.pickerView?.reloadAllComponents()
                }
            }
        }
        
        if (mode == "settings"){
            self.lblTitle.text = titleString
            self.lblProfileSetup.text = profileSetupString
            self.lblCurrentProject.text = currentProjectString
            self.textView!.text = currentProjectText
            self.btnCompleteProfile.setTitle(buttonTitle, forState: .Normal)
            
            let user = User(forPrimaryKey: currentUserID)
            
            if let vertical = user?.vertical {
                if let defaultRowIndex = find(verticals.toArray(Vertical.self), vertical) {
                    pickerView?.selectRow(defaultRowIndex, inComponent: 0, animated: false)
                }
            }
            
            if let fundingRound = user?.fundingRound {
                if let defaultRowIndex = find(rounds.toArray(FundingRound.self), fundingRound) {
                    pickerView?.selectRow(defaultRowIndex, inComponent: 1, animated: false)
                }
            }
        }
        
        textView!.layer.borderWidth = 1
        textView!.layer.borderColor = UIColor.grayColor().CGColor
        
        let keyboardToolbar = UIToolbar(frame: CGRectMake(0, 0, UIScreen.mainScreen().bounds.size.width, 44))
        let flexibleSpace = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.FlexibleSpace, target: nil, action: nil)
        let nextButton = UIBarButtonItem(title: "Next", style: UIBarButtonItemStyle.Plain, target: self, action: "nextButtonTapped")
        nextButton.tintColor = UIColor(red: 0.49, green: 0.60, blue: 0.71, alpha: 1)
        keyboardToolbar.setItems([flexibleSpace, nextButton, flexibleSpace], animated: false)
        textView?.inputAccessoryView = keyboardToolbar
        
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "keyboardWillShow:", name: UIKeyboardWillShowNotification, object: nil)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "keyboardWillHide:", name: UIKeyboardWillHideNotification, object: nil)
    }
    
    func keyboardWillShow(notification: NSNotification) {
        if let userInfo = notification.userInfo {
            let duration = userInfo[UIKeyboardAnimationDurationUserInfoKey]!.doubleValue
            let curve = userInfo[UIKeyboardAnimationCurveUserInfoKey] as! NSNumber
            
            self.textViewTopConstraint.constant = 10;
            
            UIView.animateWithDuration(duration, delay: 0, options: UIViewAnimationOptions.BeginFromCurrentState, animations: { () -> Void in
                UIView.setAnimationCurve(UIViewAnimationCurve(rawValue: curve.unsignedIntegerValue)!)
                self.view.layoutIfNeeded()
            }, completion: nil)
        }
    }
    
    func keyboardWillHide(notification: NSNotification) {
        if let userInfo = notification.userInfo {
            let duration = userInfo[UIKeyboardAnimationDurationUserInfoKey]!.doubleValue
            let curve = userInfo[UIKeyboardAnimationCurveUserInfoKey] as! NSNumber
            
            self.textViewTopConstraint.constant = 97;
            
            UIView.animateWithDuration(duration, delay: 0, options: UIViewAnimationOptions.BeginFromCurrentState, animations: { () -> Void in
                UIView.setAnimationCurve(UIViewAnimationCurve(rawValue: curve.unsignedIntegerValue)!)
                self.view.layoutIfNeeded()
                }, completion: nil)
        }
    }
    
    override func touchesBegan(touches: Set<NSObject>, withEvent event: UIEvent) {
        self.view.endEditing(true)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
    }
    
    func nextButtonTapped() {
        textView?.resignFirstResponder()
    }
    
    // returns the number of 'columns' to display.
    func numberOfComponentsInPickerView(pickerView: UIPickerView) -> Int{
        return 2
    }
    
    // returns the # of rows in each component..
    func pickerView(pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int{
        switch(component){
            case 0:
                return Int(verticals.count)
            case 1:
                return Int(rounds.count)
            default:
                return 0
        }
    }
    
    func pickerView(pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String!{
        switch (component){
            case 0:
                return verticals[UInt(row)].name
            case 1:
                return rounds[UInt(row)].name
            default:
                return ""
        }
    }
    
     func didTapNextStep(){
        
        let selected_vertical = UInt(pickerView!.selectedRowInComponent(0))
        let vertical = verticals[selected_vertical] as! Vertical
        
        let selected_round = UInt(pickerView!.selectedRowInComponent(1))
        let round = rounds[selected_round] as! FundingRound
        
        var current_project = textView!.text
        RequestManager.step4(authToken, current_project: current_project, vertical: String(vertical.name), funding_round: String(round.name)){ (response, error) in
            dispatch_async(dispatch_get_main_queue()){
                self.showCollaborationCorner()
            }
        }
    }
    
    func showCollaborationCorner(){
        if (mode == "signup"){
            let storyboard = UIStoryboard(name: "CollaborationCorner", bundle: nil)
            let vc = storyboard.instantiateViewControllerWithIdentifier("CollaborationCornerNavigationController") as! UINavigationController
            self.presentViewController(vc, animated: true) { () -> Void in
                let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
//                appDelegate.refreshCurrentUsersLocation()
            }
        } else {
            self.navigationController?.popViewControllerAnimated(true)
        }
    }
}
