//
//  SettingsViewController.swift
//  Unity for People
//
//  Created by Ron De Benedetti on 8/17/15.
//  Copyright (c) 2015 TwinEngineLabs. All rights reserved.
//

import apptentive_ios

class SettingsViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet var tableView: UITableView!
    
    var settings = ["Edit Profile", "Edit Avatar and Badges", "Edit Skillsets", "Edit Event Interests", "Edit Project Details", "Give Feedback", "Logout"]
    
    override func viewDidLoad(){
        super.viewDidLoad()
        
        var nib = UINib(nibName: "SkillsetCell", bundle: nil)
        tableView?.registerNib(nib, forCellReuseIdentifier: "SkillsetCellIdentifier")
        
        tableView?.tableFooterView = UIView(frame: CGRect.zeroRect)

    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        
        let selectedItem = settings[indexPath.row]
        switch selectedItem{
        case "Edit Profile":
            var user = User(forPrimaryKey: currentUserID)!
            let storyboard = UIStoryboard(name: "SignUp", bundle: nil)
            var vc = storyboard.instantiateViewControllerWithIdentifier("Registration") as! RegistrationViewController
            vc.mode = "settings"
            vc.user = user
            vc.firstNameFieldText = user.first_name
            vc.lastNameFieldText = user.last_name
            vc.emailFieldText = user.email
            vc.registerButtonTitle = "UPDATE"
            
            RequestManager.getUser(user) { (response, error) in
                dispatch_async(dispatch_get_main_queue()) {
                    self.navigationController?.pushViewController(vc, animated: true)
                }
            }
        case "Edit Avatar and Badges":
            var user = User(forPrimaryKey: currentUserID)!
            let storyboard = UIStoryboard(name: "SignUp", bundle: nil)
            var vc = storyboard.instantiateViewControllerWithIdentifier("ProfileSetup") as! ProfileSetupViewController
            vc.mode = "settings"
            vc.titleString = "Unity for People"
            vc.profileSetupString = "SETTINGS"
            vc.buttonTitle = "SAVE PROFILE"
            
            RequestManager.getUser(user){ (response, error) in
                dispatch_async(dispatch_get_main_queue()){
                    self.navigationController?.pushViewController(vc, animated: true)
                }
            }
        case "Edit Skillsets":
            var user = User(forPrimaryKey: currentUserID)!
            let storyboard = UIStoryboard(name: "SignUp", bundle: nil)
            var vc = storyboard.instantiateViewControllerWithIdentifier("SkillsetsHave") as! SkillsetsHaveViewController
            vc.mode = "settings"
            vc.titleString = "Unity for People"
            vc.profileSetupString = "SETTINGS"
            vc.skillsetsString = "Edit Your Skillsets"
            vc.buttonTitle = "SAVE SKILLSETS"
            
            RequestManager.getSkills() { (response, error) in
                RequestManager.getUser(user){ (response, error) in
                    dispatch_async(dispatch_get_main_queue()){
                        self.navigationController?.pushViewController(vc, animated: true)
                    }
                }
            }
        case "Edit Event Interests":
            var user = User(forPrimaryKey: currentUserID)!
            let storyboard = UIStoryboard(name: "SignUp", bundle: nil)
            var vc = storyboard.instantiateViewControllerWithIdentifier("Interests") as! InterestsViewController
            vc.mode = "settings"
            vc.titleString = "Unity for People"
            vc.profileSetupString = "SETTINGS"
//            vc.interestsString = "Edit Your Event Interests"
            vc.buttonTitle = "SAVE"
            
            RequestManager.getInterests() { (response, error) in
                RequestManager.getUser(user){ (response, error) in
                    dispatch_async(dispatch_get_main_queue()){
                        self.navigationController?.pushViewController(vc, animated: true)
                    }
                }
            }
        case "Edit Project Details":
            var user = User(forPrimaryKey: currentUserID)!
            let storyboard = UIStoryboard(name: "SignUp", bundle: nil)
            var vc = storyboard.instantiateViewControllerWithIdentifier("CurrentProject") as! DescribeCurrentProjectViewController
            vc.mode = "settings"
            vc.titleString = "Unity for People"
            vc.profileSetupString = "SETTINGS"
            vc.currentProjectString = "Edit Your Current Project"
            vc.buttonTitle = "SAVE CURRENT PROJECT"
            
            RequestManager.getVerticals() { (response, error) in
                RequestManager.getFundingRounds(){ (response, error) in
                    RequestManager.getUser(user){ (response, error) in
                        dispatch_async(dispatch_get_main_queue()){
                            vc.currentProjectText = user.currentProject
                            self.navigationController?.pushViewController(vc, animated: true)
                        }
                    }
                }
            }
        case "Give Feedback":
            ATConnect.sharedConnection().presentMessageCenterFromViewController(self)
        case "Logout":
            authToken = ""
            currentUserID = -1
            
            let userDefaults = NSUserDefaults.standardUserDefaults()
            userDefaults.removeObjectForKey("currentUserID")
            userDefaults.removeObjectForKey("authenticationToken")
            userDefaults.synchronize()
            
            let storyboard = UIStoryboard(name: "SignUp", bundle: nil)
            var vc = storyboard.instantiateViewControllerWithIdentifier("LandingViewController") as! LandingViewController
            let navController = UINavigationController(rootViewController: vc)
            self.presentViewController(navController, animated: true, completion: nil)
        default:
            println("No op")
        }
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationItem.title = "Settings"
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return settings.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("SkillsetCellIdentifier", forIndexPath: indexPath) as! SkillsetCell
        cell.myLabel.text = settings[indexPath.row]
        return cell
    }
    
    
}
