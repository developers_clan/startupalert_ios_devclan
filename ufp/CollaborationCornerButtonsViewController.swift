//
//  CollaborationCornerButtonsViewController.swift
//  Unity for People
//
//  Created by Ron De Benedetti on 7/29/15.
//  Copyright (c) 2015 TwinEngineLabs. All rights reserved.
//

import apptentive_ios

class CollaborationCornerButtonsViewController: UIViewController {

    @IBAction func didTapDiscussions(sender: AnyObject) {
        if !RequestManager.tokenIsValid() {
            promptForSignIn()
        } else {
            let vc = self.storyboard?.instantiateViewControllerWithIdentifier("Discussions") as! DiscussionsSlackViewController
            self.navigationController?.pushViewController(vc, animated: true)
            ATConnect.sharedConnection().engage("Discussions Tapped", fromViewController: vc)
        }
    }
    
    @IBAction func didTapEvents() {
        if !RequestManager.tokenIsValid() {
            promptForSignIn()
        } else {
            let vc = self.storyboard?.instantiateViewControllerWithIdentifier("Events") as! EventsViewController
            let wantedEventsPredicate = NSPredicate(format: "purview == true")
            vc.wantedEvents = Event.objectsWithPredicate(wantedEventsPredicate).sortedResultsUsingProperty("starts_at", ascending: true)
            let unwantedEventsPredicate = NSPredicate(format: "purview == false")
            vc.unwantedEvents = Event.objectsWithPredicate(unwantedEventsPredicate).sortedResultsUsingProperty("starts_at", ascending: true)
            self.navigationController?.pushViewController(vc, animated: true)
            ATConnect.sharedConnection().engage("Events Tapped", fromViewController: vc)
        }
    }
    
    func promptForSignIn() {
        var signUpStoryboard = UIStoryboard(name: "SignUp", bundle: nil)
        let vc = signUpStoryboard.instantiateViewControllerWithIdentifier("SignUp") as! SignUpViewController
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
}
