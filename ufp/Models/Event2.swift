//
//  Event2.swift
//  Unity for People
//
//  Created by Devclan on 09/12/2015.
//  Copyright (c) 2015 TwinEngineLabs. All rights reserved.
//

import UIKit

public class Event2: NSObject {
    var objId:Int!
    var name:String!
    var content:String!
    var startsAt:String!
    var endsAt:String!
    var picUrl:String!
    var locationName:String!
    var locationAddress:String!
    var thumbURL:String!

    var lat:String!
    var lng:String!
    
    var isFavorite:Bool!
    var isHighlighted:Bool!
    var section:EventsSection!
    var intrests:NSMutableArray!
    init(dict:NSDictionary!,sect:EventsSection!){
        section=sect
        objId = dict.valueForKey("id") as! Int
        name = dict.valueForKey("name") as! String
        content = dict.valueForKey("content") as! String
        startsAt = dict.valueForKey("starts_at") as! String
        endsAt = dict.valueForKey("ends_at") as! String
        picUrl = dict.valueForKey("pictureURL") as! String
        locationName = dict.valueForKey("location_name") as! String
        locationAddress = dict.valueForKey("location_address") as! String
        thumbURL = dict.valueForKey("thumbnailURL") as! String
        
        lat = dict.valueForKey("lat") as! String
        lng = dict.valueForKey("lng") as! String
        
        isFavorite = dict.valueForKey("is_favourite") as! Bool
        isHighlighted = dict.valueForKey("is_highlighted") as! Bool
        intrests = NSMutableArray()
        
        
        var arr = dict.objectForKey("interests") as! NSArray
        for i in arr{
            intrests.addObject(Interest2(dict: i as! NSDictionary))
        }
        
    }
    func intrestsString()->NSString{
        var str:NSString! = ""
        for i in intrests{
            str = "\(str)\((i as! Interest2).name) "
        }
        return str
    }
}
