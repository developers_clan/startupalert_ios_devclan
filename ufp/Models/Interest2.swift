//
//  Interest2.swift
//  Unity for People
//
//  Created by Devclan on 11/12/2015.
//  Copyright (c) 2015 TwinEngineLabs. All rights reserved.
//

import UIKit

class Interest2: NSObject {
/*
    {
    "id": 5,
    "name": "Startup Week/End events",
    "created_at": "2015-08-18T18:06:39.835-05:00",
    "updated_at": "2015-08-18T18:06:39.835-05:00"
    }
    
    */
    var objId:Int!
    var name:String!
    var updatedAt:String!
    var createdAt:String!
    
    init(dict:NSDictionary!){
        objId = dict.valueForKey("id") as! Int
        name = dict.valueForKey("name") as! String
        updatedAt = dict.valueForKey("created_at") as! String
        createdAt = dict.valueForKey("updated_at") as! String
    }
}
