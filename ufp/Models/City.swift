//
//  City.swift
//  Unity for People
//
//  Created by Devclan on 11/12/2015.
//  Copyright (c) 2015 TwinEngineLabs. All rights reserved.
//

import UIKit

class City: NSObject {
    var objId:Int!
    var name:String!
    var state:String!
    var country:String!
    var lat:Double!
    var lng:Double!
    
    init(dict:NSDictionary!){
        objId = dict.valueForKey("id") as! Int
        name = dict.valueForKey("name") as! String
        state = dict.valueForKey("state") as! String
        country = dict.valueForKey("country") as! String
        lat = dict.valueForKey("latitude") as! Double
        lng = dict.valueForKey("longitude") as! Double
    }
}
