//
//  TotalCounts.swift
//  Unity for People
//
//  Created by Devclan on 15/12/2015.
//  Copyright (c) 2015 TwinEngineLabs. All rights reserved.
//

import UIKit

class TotalCounts: NSObject {
    var suggestedEventsForWeek:Int!
    var suggestedEventsForMonth:Int!
    var allEventsForWeek:Int!
    var allEventsForMonth:Int!
    
    var suggestedEventsForWeekFav:Int!
    var suggestedEventsForMonthFav:Int!
    var allEventsForWeekFav:Int!
    var allEventsForMonthFav:Int!
    
    init(dict:NSDictionary!){
        suggestedEventsForWeek = dict.valueForKey("suggested_events_for_week") as! Int
        suggestedEventsForMonth = dict.valueForKey("suggested_events_for_month") as! Int
        allEventsForWeek = dict.valueForKey("all_events_for_week") as! Int
        allEventsForMonth = dict.valueForKey("all_events_for_month") as! Int
        
        suggestedEventsForWeekFav = dict.valueForKey("favorite_suggested_week_count") as! Int
        suggestedEventsForMonthFav = dict.valueForKey("favorite_suggested_month_count") as! Int
        allEventsForWeekFav = dict.valueForKey("favorite_all_week_count") as! Int
        allEventsForMonthFav = dict.valueForKey("favorite_all_month_count") as! Int
    }
    override init(){
        suggestedEventsForWeek = 0
        suggestedEventsForMonth = 0
        allEventsForWeek = 0
        allEventsForMonth = 0
        
        suggestedEventsForWeekFav = 0
        suggestedEventsForMonthFav = 0
        allEventsForWeekFav = 0
        allEventsForMonthFav = 0
    }
}
