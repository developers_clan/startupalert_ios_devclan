//
//  ConnectionsViewController.swift
//  Unity for People
//
//  Created by Ron De Benedetti on 8/7/15.
//  Copyright (c) 2015 TwinEngineLabs. All rights reserved.
//

import UIKit

class ConnectionsViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet var tableView: UITableView!
    @IBOutlet var skillsetsButton: UIButton!
    
    var users: RLMResults!

    override func viewDidLoad() {
        super.viewDidLoad()
        
        skillsetsButton.titleLabel?.numberOfLines = 1
        skillsetsButton.titleLabel?.adjustsFontSizeToFitWidth = true
        
        RequestManager.getUsers() { (response, error) in
            dispatch_async(dispatch_get_main_queue()){
                let predicate = NSPredicate(format: "id != \(currentUserID)")
                self.users = User.objectsWithPredicate(predicate)
                self.tableView.reloadData()
            }
        }
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationItem.title = "Connections"
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        return Int(users.count)
    }

    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell{
        let cell = tableView.dequeueReusableCellWithIdentifier("ConnectionCellIdentifier") as! ConnectionsCell
        let user = users[UInt(indexPath.row)] as! User
        
        cell.myName.text = user.name()
        cell.myImage.sd_setImageWithURL(NSURL(string: user.avatarURL))
        
        return cell
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath){
        let selectedUser = users[UInt(indexPath.row)] as! User
        
        if selectedUser.invalidated { return }
        
        showUser(selectedUser)
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return 90
    }
    
    @IBAction func didTapSkillsetButton(){
        showSkillsetFilter()
    }
    
    func showSkillsetFilter(){
        let vc = self.storyboard?.instantiateViewControllerWithIdentifier("SkillsetFilter") as! FilterBySkillsetViewController
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func showUser(user: User){
        RequestManager.getUser(user){ (response, error) in
            dispatch_async(dispatch_get_main_queue()){
                let vc = self.storyboard?.instantiateViewControllerWithIdentifier("UserShow") as! UserShowViewController
                vc.user = user
                self.navigationController?.pushViewController(vc, animated: true)
            }
        }
    }
    
}
